package io.powroseba.page.structs

import spock.lang.Specification

class WebReferenceTest extends Specification {

    def 'should clean web address with necessary data'() {
        when:
            def webAddress = new WebReference(input)

        then:
            webAddress.webLink == output
            noExceptionThrown()

        where:
            input                     || output
            "http://www.address.com"  || "address.com"
            "https://www.address.com" || "address.com"
            "https://address.com"     || "address.com"
            "www.address.com"         || "address.com"
            "addresshttp.com"         || "addresshttp.com"
            "addresshttps.com"        || "addresshttps.com"

    }
}
