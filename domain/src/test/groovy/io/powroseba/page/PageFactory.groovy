package io.powroseba.page

import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId

final class PageFactory {

    private PageFactory() {}

    static Page withContainers(int n, ContainerFactory.FixedContainer fixedContainer) {
        return new Page(
                new PageId(UUID.randomUUID()),
                new DisplayText("page"),
                0,
                ContainersFactory.nContainersSomeFixed(n, fixedContainer)
        )
    }
}
