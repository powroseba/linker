package io.powroseba.page


import io.powroseba.page.container.ContainerFactory.FixedContainer
import io.powroseba.page.container.Containers
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId

import java.util.stream.Collectors

final class PagesFactory {

    static final String DISPLAY_TEXT = "page"

    private PagesFactory() {}

    static Pages nPagesOneFixed(int n, FixedPage fixedPage) {
        final def order = PageOrderFactory.nEntriesOneFixed(n, fixedPage)
        final def pages = order.stream()
                .map({ entry -> new Page(entry.id, new DisplayText(DISPLAY_TEXT), entry.order, prepareContainers(fixedPage)) })
                .collect(Collectors.toSet())
        return new Pages(order, pages)
    }

    static Pages nPages(int n) {
        final def order = PageOrderFactory.nEntries(n)
        final def pages = order.stream()
                .map({ entry -> new Page(entry.id, new DisplayText(DISPLAY_TEXT), entry.order) })
                .collect(Collectors.toSet())
        return new Pages(order, pages)
    }

    private static Containers prepareContainers(FixedPage fixedPage) {
        if (fixedPage.container == null && fixedPage.containers == null) {
            return Containers.empty(fixedPage.pageId)
        }
        if (fixedPage.container != null) {
            return ContainersFactory.nContainersSomeFixed(1, fixedPage.container)
        }
        return fixedPage.containers
    }

    static class FixedPage {
        final PageId pageId
        final int order
        String displayText
        final FixedContainer container
        final Containers containers

        FixedPage(PageId pageId, int order, String displayText = "page", FixedContainer container = null, Containers containers = null) {
            this.pageId = pageId
            this.order = order
            this.displayText = displayText
            this.container = container
            this.containers = containers
        }

        FixedPage(PageId pageId, int order, Containers containers) {
            this.pageId = pageId
            this.order = order
            this.displayText = "page"
            this.container = null
            this.containers = containers
        }
    }
}
