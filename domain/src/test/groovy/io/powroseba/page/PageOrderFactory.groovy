package io.powroseba.page

import io.powroseba.page.structs.PageId

final class PageOrderFactory {

    private PageOrderFactory() {}

    static PageOrder nEntriesOneFixed(int n, PagesFactory.FixedPage page) {
        final def set = new HashSet()
        for (int i = 0; i < n; i++) {
            if (i == page.order) {
                set.add(new PageOrder.PageOrderEntry(i, page.pageId))
            } else {
                set.add(new PageOrder.PageOrderEntry(i, new PageId(UUID.randomUUID())))
            }
        }
        return new PageOrder(set)
    }

    static PageOrder nEntries(int n) {
        return nEntriesOneFixed(n, new PagesFactory.FixedPage(new PageId(UUID.randomUUID()), 0))
    }
}
