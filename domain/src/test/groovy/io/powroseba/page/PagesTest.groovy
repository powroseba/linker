package io.powroseba.page


import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import spock.lang.Specification

import java.util.stream.Collectors

class PagesTest extends Specification {

    def 'should create empty pages'() {
        when:
            def empty = Pages.empty()

        then:
            empty.getOrder().stream().count() == 0
            empty.stream().count() == 0
            noExceptionThrown()
    }

    def 'should add new page'() {
        given:
            final def pages = Pages.empty()
            final def pageDisplayText = "displayTextOfPage"

        when:
            def result = pages.add(new DisplayText(pageDisplayText))

        then:
            result.getOrder().stream().count() == 1
            noExceptionThrown()
    }

    def 'should throw exception when trying to reorder page which does not exist'() {
        given:
            final def notExistingPageId = new PageId(UUID.randomUUID())
            final def pages = PagesFactory.nPages(10)

        when:
            pages.reorder(new PageReorder(notExistingPageId, 5))

        then:
            def exception = thrown(PageException.PageNotFound)
            exception.getMessage() == "Page " + notExistingPageId.value + " not found"
    }

    def 'should reorder pages'() {
        given:
            final def initialPageOrder = 4
            final def destinationPageOrder = 7
            final def pageIdToMove = new PageId(UUID.randomUUID())
            final def pages = PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(pageIdToMove, initialPageOrder))

        when:
            def result = pages.reorder(new PageReorder(pageIdToMove, destinationPageOrder))

        then:
            pages.getOrder().getOrderOfPage(pageIdToMove) == initialPageOrder
            with(result) {
                getOrder().stream().count() == pages.getOrder().stream().count()
                result.stream().count() == pages.stream().count()
                getOrder().getOrderOfPage(pageIdToMove) == destinationPageOrder
            }
            noExceptionThrown()
    }

    def 'should not remove page due to fact page to remove does not exist'() {
        given:
            final def pageToRemoveId = new PageId(UUID.randomUUID())
            final def pages = PagesFactory.nPages(10)

        when:
            pages.remove(pageToRemoveId)

        then:
            def exception = thrown(PageException.PageNotFound)
            exception.getMessage() == "Page " + pageToRemoveId.value + " not found"
    }

    def 'should remove page'() {
        given:
            final def pageToRemoveOrder = 2
            final def pageToRemoveId = new PageId(UUID.randomUUID())
            final def pages = PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(pageToRemoveId, pageToRemoveOrder))

        when:
            def result = pages.remove(pageToRemoveId)

        then:
            pages.stream().anyMatch({ page -> (page.pageId == pageToRemoveId) })
            result.stream().noneMatch({ page -> page.pageId == pageToRemoveId })
            pages.order.getOrderOfPage(pageToRemoveId) == pageToRemoveOrder
            result.order.getOrderOfPage(pageToRemoveId) == null
            noExceptionThrown()
    }

    def 'should load pages from pages export model'() {
        given:
            final def exportModel = PagesExportModelFactory.with2PagesIncluding4ContainersIncluding10Links()

        when:
            Pages pages = Pages.load(exportModel)

        then:
            pages.stream().count() == 2
            final def sortedPages = sortPages(pages)
            for (int i = 0; i < sortedPages.size(); i++) {
                final def page = sortedPages.get(i)
                assert page.displayOrder == pages.getOrder().getOrderOfPage(page.pageId)
                assert page.getDisplayText() == "page_" + i
                assert page.containers().count() == 4
                assert page.containers().flatMap({ it.links() }).count() == 40
            }
            noExceptionThrown()
    }

    private static List<Page> sortPages(Pages pages) {
        return pages
                .stream()
                .sorted({ p1, p2 -> Integer.compare(p1.displayOrder, p2.displayOrder) })
                .collect(Collectors.toList())
    }
}
