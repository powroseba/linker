package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.export.PagesExportModel
import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.ImageAddress
import io.powroseba.page.structs.ModifyLink
import io.powroseba.page.structs.PageId
import io.powroseba.page.structs.WebReference
import spock.lang.Specification

class LinkTest extends Specification {

    def 'should modify link display text and web reference'() {
        given:
            final def newDisplayText = new DisplayText("newDisplayText")
            final def newWebReference = new WebReference("www.web.reference")
            final def newImageAddress = new ImageAddress("www.newImage.jpg")
            final def link = LinkFactory.create(
                    new LinkFactory.FixedLink(
                            displayText: "linkText",
                            webReference: "www.initial.address",
                            imageAddress: "initial.jpg"
                    )
            )

        when:
            def result = link.modify(new ModifyLink(link.linkId, newDisplayText, newWebReference, newImageAddress))

        then:
            with(result) {
                linkId == link.linkId
                displayText == newDisplayText
                webReference == newWebReference
                image == newImageAddress
                displayOrder == link.displayOrder
            }
            noExceptionThrown()
    }

    def 'should create valid export model of link'() {
        given:
            def containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            final def link = new Link(
                    containerId,
                    new LinkId(containerId, UUID.randomUUID()),
                    new DisplayText("text"),
                    new WebReference("www.web.ref"),
                    new ImageAddress("www.image.jpg"),
                    0
            )

        when:
            def exportModel = link.createExportModel()

        then:
            with(exportModel) {
                linkId == link.linkId.value
                displayText == link.displayText.content
                displayOrder == link.displayOrder
                webReference == link.webReference.webLink
                imageAddress == link.image.address
            }
            noExceptionThrown()
    }

    def 'should load link from export model link'() {
        given:
            final def containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            final def exportModelLink = PagesExportModelFactory.link(0)

        when:
            Link link = Link.load(containerId, exportModelLink)

        then:
            link != null
            with(link) {
                displayOrder == 0
                displayText == new DisplayText("link_" + 0)
                webReference == new WebReference("webReference_" + 0 + ".com")
                image == new ImageAddress("image_" + 0 + ".jpg")
                linkId != null
                linkId.containerId == containerId
            }
            noExceptionThrown()
    }
}
