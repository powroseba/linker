package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.structs.PageId
import spock.lang.Specification
import spock.lang.Unroll

class LinkOrderTest extends Specification {

    private static final PageId randomPageId = new PageId(UUID.randomUUID())
    private static final ContainerId randomContainerId = new ContainerId(randomPageId, UUID.randomUUID())

    def 'should add new link to link order'() {
        given:
            final def linkId = new LinkId(randomContainerId, UUID.randomUUID())
            final def order = new LinkOrder(new HashSet<>())

        when:
            def newOrder = order.add(linkId)

        then:
            newOrder.stream().count() == 1
            newOrder.stream().anyMatch({ entry -> entry.getId() == linkId })
            noExceptionThrown()
    }

    @Unroll
    def 'should reorder links - should move link from #initialOrder to #destinationOrder'() {
        given:
            final def linkReorder = new LinkReorder(
                    new LinkId(randomContainerId, UUID.randomUUID()),
                    destinationOrder
            )
            final def order = LinkOrderFactory.nEntriesOneFixed(
                    10,
                    new LinkFactory.FixedLink(linkId : linkReorder.id, order: initialOrder)
            )

        when:
            def result = order.swapOrder(linkReorder)

        then:
            final var resultOrderSize = result.stream().count()
            resultOrderSize == order.stream().count()
            for (int i = 0; i < resultOrderSize; i++) {
                final var oldOrder = findByOrder(order, i)
                final var newOrder = findByOrder(result, i)

                if (initialOrder > destinationOrder) {
                    if (i <= initialOrder && i >= destinationOrder) {
                        assert oldOrder.id != newOrder.id
                    } else if (i == destinationOrder) {
                        assert oldOrder.id != linkReorder.id
                        assert newOrder.id == linkReorder.id
                    } else {
                        assert oldOrder.id == newOrder.id
                    }
                } else if (initialOrder == destinationOrder) {
                    assert oldOrder.id == newOrder.id
                } else {
                    if (i <= destinationOrder && i >= initialOrder) {
                        assert oldOrder.id != newOrder.id
                    } else if (i == destinationOrder) {
                        assert oldOrder.id != linkReorder.id
                        assert newOrder.id == linkReorder.id
                    } else {
                        assert oldOrder.id == newOrder.id
                    }
                }
            }
            noExceptionThrown()

        where:
            initialOrder || destinationOrder
            0            || 2
            2            || 4
            2            || 2
            4            || 2
            8            || 3
            8            || 8

    }

    @Unroll
    def 'should reorder links order to lower from link on index #initialOrder'() {
        given:
            final def linkId = new LinkId(randomContainerId, UUID.randomUUID())
            final def order = LinkOrderFactory.nEntriesOneFixed(
                    10,
                    new LinkFactory.FixedLink(linkId:  linkId, order: initialOrder)
            )

        when:
            def result = order.removeOrder(linkId)

        then:
            final def resultCount = result.stream().count()
            resultCount == order.stream().count() - 1
            assert findByLinkId(result, linkId) == null
            for (int i = 0; i < resultCount; i++) {
                if (i >= initialOrder) {
                    assert findByOrder(result, i).id == findByOrder(order, i + 1).id
                } else {
                    assert findByOrder(order, i) == findByOrder(result, i)
                }
            }
            noExceptionThrown()

        where:
            initialOrder << [0, 4, 6, 9]

    }

    def 'should return order of link when link exist in link order'() {
        given:
            final order = 2
            final def linkId = new LinkId(randomContainerId, UUID.randomUUID())
            final var linkOrder = LinkOrderFactory.nEntriesOneFixed(
                    10,
                    new LinkFactory.FixedLink(linkId: linkId, order: order)
            )

        when:
            def result = linkOrder.getOrderOfLink(linkId)

        then:
            result == order
            noExceptionThrown()
    }

    private static LinkOrder.LinkOrderEntry findByOrder(LinkOrder order, int index) {
        return order.stream()
                .filter({ it.order == index })
                .findFirst()
                .orElse(null)
    }

    private static LinkOrder.LinkOrderEntry findByLinkId(LinkOrder linkOrder, LinkId linkId) {
        linkOrder.stream()
                .filter({ entry -> entry.id == linkId })
                .findFirst()
                .orElse(null)
    }
}
