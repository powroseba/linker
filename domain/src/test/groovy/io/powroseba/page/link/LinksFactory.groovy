package io.powroseba.page.link

import io.powroseba.page.link.LinkFactory.FixedLink

import java.util.stream.Collectors

final class LinksFactory {

    private LinksFactory() {}

    static Links nLinksOneFixed(int n, LinkFactory.FixedLink fixedLink) {
        final def order = LinkOrderFactory.nEntriesOneFixed(n, fixedLink)
        final def links = order.stream()
                .map({ prepareLinks(it, fixedLink) })
                .collect(Collectors.toSet())
        return new Links(fixedLink.containerId, order, links)
    }

    private static Link prepareLinks(LinkOrder.LinkOrderEntry it, FixedLink link) {
        if (link != null && it.order == link.order) {
            return LinkFactory.create(link)
        } else {
            return LinkFactory.create(it.getId())
        }
    }

    static Links nLinks(int n) {
        return nLinksOneFixed(n, new LinkFactory.FixedLink())
    }
}
