package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.ImageAddress
import io.powroseba.page.structs.PageId
import io.powroseba.page.structs.WebReference

final class LinkFactory {

    private LinkFactory() {}

    static Link create(FixedLink fixedLink) {
        return new Link(
                fixedLink.containerId,
                fixedLink.linkId,
                new DisplayText(fixedLink.displayText),
                new WebReference(fixedLink.webReference),
                new ImageAddress(fixedLink.imageAddress),
                fixedLink.order
        )
    }

    static Link create(LinkId linkId) {
        return new Link(
                new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                linkId,
                new DisplayText("link"),
                new WebReference("www.test.pl"),
                new ImageAddress("image.jpg"),
                0
        )
    }

    static class FixedLink {
        ContainerId containerId
        LinkId linkId
        int order
        String displayText
        String webReference
        String imageAddress

        FixedLink(ContainerId containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                  LinkId linkId,
                  int order = 0,
                  String displayText = "link",
                  String webReference = "www.test.com",
                  String imageAddress = "image.jpg") {
            this.containerId = containerId
            this.linkId = linkId
            this.order = order
            this.displayText = displayText
            this.webReference = webReference
            this.imageAddress = imageAddress
        }

        FixedLink(ContainerId containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                  int order = 0,
                  String displayText = "link",
                  String webReference = "www.test.com",
                  String imageAddress = "image.jpg") {
            this.containerId = containerId
            this.linkId = new LinkId(containerId, UUID.randomUUID())
            this.order = order
            this.displayText = displayText
            this.webReference = webReference
            this.imageAddress = imageAddress
        }
    }
}
