package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.structs.PageId
import spock.lang.Specification

class LinkOrderEntryTest extends Specification {

    public static final LinkOrder.LinkOrderEntry exampleLinkOrder = new LinkOrder.LinkOrderEntry(
            2, new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())
    )

    def 'should increase order'() {
        when:
            def result = exampleLinkOrder.increase()

        then:
            result == fromExampleLinkOrder(exampleLinkOrder.getOrder() + 1)
            noExceptionThrown()
    }

    def 'should decrease order'() {
        when:
            def result = exampleLinkOrder.decrease()

        then:
            result == fromExampleLinkOrder(exampleLinkOrder.getOrder() - 1)
            noExceptionThrown()
    }

    def 'should not decrease order with order = 0'() {
        given:
            final def pageOrder = fromExampleLinkOrder(0)

        when:
            def result = pageOrder.decrease()

        then:
            result == pageOrder
            noExceptionThrown()
    }

    def 'should change order'() {
        when:
            def result = exampleLinkOrder.changeOrder(3)

        then:
            result == fromExampleLinkOrder(3)
            noExceptionThrown()

    }

    def 'should change order at least to 0 when input order is less than 0'() {
        when:
            def result = exampleLinkOrder.changeOrder(-1)

        then:
            result == fromExampleLinkOrder(0)
            noExceptionThrown()

    }

    private static LinkOrder.LinkOrderEntry fromExampleLinkOrder(int order) {
        new LinkOrder.LinkOrderEntry(order, exampleLinkOrder.getId())
    }



}
