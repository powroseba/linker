package io.powroseba.page.link

final class LinkOrderFactory {

    private LinkOrderFactory() {}

    static LinkOrder nEntriesOneFixed(int n, LinkFactory.FixedLink link) {
        final def set = new HashSet()
        for (int i = 0; i < n; i++) {
            if (i == link.order) {
                set.add(new LinkOrder.LinkOrderEntry(i, link.linkId))
            } else {
                set.add(new LinkOrder.LinkOrderEntry(i, new LinkId(link.containerId, UUID.randomUUID())))
            }
        }
        return new LinkOrder(set)
    }
}
