package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.structs.*
import spock.lang.Specification

import java.util.stream.Collectors

class LinksTest extends Specification {

    private static final PageId randomPageId = new PageId(UUID.randomUUID())
    private static final ContainerId randomContainerId = new ContainerId(randomPageId, UUID.randomUUID())

    def 'should create empty links'() {
        when:
            def result = Links.empty(randomContainerId)

        then:
            result.getOrder().stream().count() == 0
            result.stream().count() == 0
            noExceptionThrown()

    }

    def 'should add empty link to links object'() {
        given:
            final def displayText = "link"
            final def links = Links.empty(randomContainerId)

        when:
            def result = links.add(new DisplayText(displayText), new WebReference("www.test.pl"))

        then:
            result.getOrder().stream().count() == 1
            result.stream().count() == 1
            noExceptionThrown()
    }

    def 'should add fixed link on the same order as fixed link have'() {
        given:
            final def destinationOrder = 5
            final def links = LinksFactory.nLinks(10)
            final def linkToAdd = LinkFactory.create(new LinkFactory.FixedLink(order : destinationOrder))

        when:
            def result = links.add(linkToAdd)

        then:
            result.getOrder().stream().count() == 11
            result.stream().count() == 11
            result.getOrder().stream().anyMatch({ it.order == destinationOrder && it.id == linkToAdd.linkId })
            noExceptionThrown()
    }

    def 'should reorder links'() {
        given:
            final def initialLinkOrder = 4
            final def linkReorder = new LinkReorder(
                    new LinkId(randomContainerId, UUID.randomUUID()),
                    7
            )
            final def links = LinksFactory.nLinksOneFixed(
                    10,
                    new LinkFactory.FixedLink(linkId : linkReorder.id, order : initialLinkOrder)
            )

        when:
            def result = links.reorder(linkReorder)

        then:
            links.getOrder().getOrderOfLink(linkReorder.id) == initialLinkOrder
            with(result) {
                getOrder().stream().count() == links.getOrder().stream().count()
                result.stream().count() == links.stream().count()
                getOrder().getOrderOfLink(linkReorder.id) == linkReorder.destinationOrder
            }
            noExceptionThrown()
    }

    def 'should not remove link due to fact link to remove does not exist'() {
        given:
            final def linkToRemoveId = new LinkId(randomContainerId, UUID.randomUUID())
            final def links = LinksFactory.nLinks(10)

        when:
            links.remove(linkToRemoveId)

        then:
            def exception = thrown(LinkException.LinkNotFound)
            exception.getMessage() == "Link " + linkToRemoveId.value + " not found"
    }

    def 'should remove link'() {
        given:
            final def linkToRemoveOrder = 2
            final def linkToRemoveId = new LinkId(randomContainerId, UUID.randomUUID())
            final def links = LinksFactory.nLinksOneFixed(
                    10,
                    new LinkFactory.FixedLink(linkId: linkToRemoveId, order: linkToRemoveOrder)
            )

        when:
            Links result = links.remove(linkToRemoveId)

        then:
            links.stream().anyMatch({ link -> link.linkId == linkToRemoveId })
            result.stream().noneMatch({ link -> link.linkId == linkToRemoveId })
            links.order.getOrderOfLink(linkToRemoveId) == linkToRemoveOrder
            result.order.getOrderOfLink(linkToRemoveId) == null
            noExceptionThrown()
    }

    def 'should modify one link in links'() {
        given:
            final def linkToModifyId = new LinkId(randomContainerId, UUID.randomUUID())
            final def newDisplayText = new DisplayText("modifiedDisplayText")
            final def newWebAddress = new WebReference("www.modifiedWebAddress.com")
            final def newImageAddress = new ImageAddress("www.modifiedWebAddress.jpg")
            final def links = LinksFactory.nLinksOneFixed(
                    10, new LinkFactory.FixedLink(
                        linkId: linkToModifyId
                    )
            )

        when:
            def result = links.modifyLink(new ModifyLink(linkToModifyId, newDisplayText, newWebAddress, newImageAddress))

        then:
            final def modifiedLink = getLinkById(result, linkToModifyId)
            with(modifiedLink) {
                isPresent()
                get().displayText == newDisplayText
                get().webReference == newWebAddress
                get().image == newImageAddress
            }
            noExceptionThrown()

    }

    def 'should load links from export model link set'() {
        given:
            final def containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            final def exportModelLinks = PagesExportModelFactory.nLinks(10)

        when:
            Links links = Links.load(containerId, exportModelLinks)

        then:
            links.containerId == containerId
            links.stream().count() == 10
            final def sortedLinks = sortLinks(links)
            for (int i = 0; i < sortedLinks.size(); i++) {
                final def link = sortedLinks.get(i)
                assert link.displayOrder == links.getOrder().getOrderOfLink(link.linkId)
                assert link.displayText == new DisplayText("link_" + i)
            }
            noExceptionThrown()
    }

    private static Optional<Link> getLinkById(Links links, LinkId linkId) {
        return links.stream().filter({ it.linkId == linkId }).findFirst()
    }

    private static List<Link> sortLinks(Links links) {
        return links.stream()
                .sorted({ l1, l2 -> Integer.compare(l1.displayOrder, l2.displayOrder) })
                .collect(Collectors.toList())
    }

}
