package io.powroseba.page

import io.powroseba.page.structs.PageId
import spock.lang.Specification

class PageOrderEntryTest extends Specification {


    public static final PageOrder.PageOrderEntry examplePageOrder = new PageOrder.PageOrderEntry(2, new PageId(UUID.randomUUID()))

    def 'should increase order'() {
        when:
            def result = examplePageOrder.increase()

        then:
            result == fromExamplePage(examplePageOrder.getOrder() + 1)
            noExceptionThrown()
    }

    def 'should decrease order'() {
        when:
            def result = examplePageOrder.decrease()

        then:
            result == fromExamplePage(examplePageOrder.getOrder() - 1)
            noExceptionThrown()
    }

    def 'should not decrease order with order = 0'() {
        given:
            final def pageOrder = fromExamplePage(0)

        when:
            def result = pageOrder.decrease()

        then:
            result == pageOrder
            noExceptionThrown()
    }

    def 'should change order'() {
        when:
            def result = examplePageOrder.changeOrder(3)

        then:
            result == fromExamplePage(3)
            noExceptionThrown()

    }

    def 'should change order at least to 0 when input order is less than 0'() {
        when:
            def result = examplePageOrder.changeOrder(-1)

        then:
            result == fromExamplePage(0)
            noExceptionThrown()

    }

    private static PageOrder.PageOrderEntry fromExamplePage(int order) {
        new PageOrder.PageOrderEntry(order, examplePageOrder.getId())
    }
}
