package io.powroseba.page.container

import io.powroseba.page.link.LinkFactory.FixedLink
import io.powroseba.page.link.Links
import io.powroseba.page.link.LinksFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId

import java.util.stream.Stream

final class ContainerFactory {

    static Container create(ContainerId containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                            int displayOrder = 0) {
        return new Container(containerId, new DisplayText("container"), displayOrder)
    }

    static Container withLinks(int n, FixedContainer fixedContainer) {
        return new Container(
                fixedContainer.containerId,
                new DisplayText(fixedContainer.displayText),
                fixedContainer.order,
                prepareLinks(n, fixedContainer)
        )
    }

    static Container withLinks(int n, FixedLink fixedLink) {
        return new Container(
                new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                new DisplayText("container"),
                0,
                LinksFactory.nLinksOneFixed(n, fixedLink)
        )
    }

    static Container withText(String displayText) {
        return new Container(
                new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                new DisplayText(displayText),
                0)
    }

    private static Links prepareLinks(int n, FixedContainer fixedContainer) {
        return fixedContainer.links == null ? LinksFactory.nLinksOneFixed(n, fixedContainer.fixedLink) : fixedContainer.links
    }

    static class FixedContainer {
        PageId pageId
        ContainerId containerId
        int order
        String displayText
        FixedLink fixedLink
        Links links

        FixedContainer(PageId pageId = new PageId(UUID.randomUUID()),
                       ContainerId containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                       int order = 0, displayText = "container", FixedLink fixedLink = null, Links links = null) {
            this.pageId = pageId
            this.containerId = containerId
            this.order = order
            this.displayText = displayText
            this.fixedLink = fixedLink
            this.links = links
        }

        FixedContainer(ContainerId containerId, int order, Links links) {
            this.pageId = containerId.pageId
            this.containerId = containerId
            this.order = order
            this.displayText = "container"
            this.fixedLink = null
            this.links = links
        }

        static Optional<FixedContainer> getByOrder(FixedContainer[] containers, int order) {
            return Stream.of(containers)
                    .filter({ container -> container.order == order })
                    .findFirst()
        }

        static PageId randomOfFixedPageId(FixedContainer... containers) {
            return containers[0] == null ? new PageId(UUID.randomUUID()) : containers[0].pageId
        }
    }
}
