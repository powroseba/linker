package io.powroseba.page.container

import io.powroseba.page.structs.PageId

import java.util.stream.Collectors

import static io.powroseba.page.container.ContainerFactory.*

final class ContainersFactory {

    private ContainersFactory() {}

    static Containers nContainersSomeFixed(int n, FixedContainer... fixedContainers) {
        final def order = ContainerOrderFactory.nEntriesSomeFixed(n, fixedContainers)
        final def containers = order.stream()
                .map({ it -> buildContainers(fixedContainerOrRandom(it, fixedContainers), it, n) })
                .collect(Collectors.toSet())
        return new Containers(randomOfFixedPageId(fixedContainers), order, containers)
    }

    static Containers nContainers(int n) {
        return nContainersSomeFixed(n, new FixedContainer())
    }

    private static Container buildContainers(FixedContainer fixedContainer,
                                             ContainerOrder.ContainerOrderEntry it, int n) {
        return fixedContainer.fixedLink == null && fixedContainer.links == null ?
                create(it.getId()) :
                withLinks(n, fixedContainer)
    }

    private static FixedContainer fixedContainerOrRandom(ContainerOrder.ContainerOrderEntry it, FixedContainer[] fixedContainers) {
        FixedContainer.getByOrder(fixedContainers, it.order).orElse(new FixedContainer())
    }

    private static PageId randomOfFixedPageId(FixedContainer... containers) {
        return containers[0] == null ? new PageId(UUID.randomUUID()) : containers[0].pageId
    }
}
