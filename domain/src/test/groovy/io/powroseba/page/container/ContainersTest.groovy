package io.powroseba.page.container

import io.powroseba.commons.DomainException
import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.link.LinkId
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.LinkMove
import io.powroseba.page.structs.ModifyContainer
import io.powroseba.page.structs.PageId
import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.IntStream

import static io.powroseba.page.container.ContainerFactory.FixedContainer
import static io.powroseba.page.container.ContainersFactory.nContainersSomeFixed
import static io.powroseba.page.link.LinkFactory.FixedLink
import static io.powroseba.page.link.LinksFactory.nLinks
import static io.powroseba.page.link.LinksFactory.nLinksOneFixed

class ContainersTest extends Specification {

    private static final PageId randomPageId = new PageId(UUID.randomUUID())

    def 'should create empty containers'() {
        when:
            def result = Containers.empty(randomPageId)

        then:
            result.getOrder().stream().count() == 0
            result.stream().count() == 0
            noExceptionThrown()

    }

    def 'should add empty container to containers object'() {
        given:
            final def displayText = "container"
            final def containers = Containers.empty(randomPageId)

        when:
            def result = containers.add(new DisplayText(displayText))

        then:
            result.getOrder().stream().count() == 1
            result.stream().count() == 1
            noExceptionThrown()
    }

    def 'should reorder containers'() {
        given:
            final def initialContainerOrder = 4
            final def containerReorder = new ContainerReorder(
                    new ContainerId(randomPageId, UUID.randomUUID()),
                    7
            )
            final def containers = nContainersSomeFixed(
                    10, new FixedContainer(containerId : containerReorder.id, order : initialContainerOrder)
            )

        when:
            def result = containers.reorder(containerReorder)

        then:
            containers.getOrder().getOrderOfContainer(containerReorder.id) == initialContainerOrder
            with(result) {
                getOrder().stream().count() == containers.getOrder().stream().count()
                result.stream().count() == containers.stream().count()
                getOrder().getOrderOfContainer(containerReorder.id) == containerReorder.destinationOrder
            }
            noExceptionThrown()
    }

    def 'should not remove container due to fact container to remove does not exist'() {
        given:
            final def containerToRemoveId = new ContainerId(randomPageId, UUID.randomUUID())
            final def containers = ContainersFactory.nContainers(10)

        when:
            containers.remove(containerToRemoveId)

        then:
            def exception = thrown(ContainerException.ContainerNotFound)
            exception.getMessage() == "Container " + containerToRemoveId.value + " not found"
    }

    def 'should remove container'() {
        given:
            final def containerToRemoveOrder = 2
            final def containerToRemoveId = new ContainerId(randomPageId, UUID.randomUUID())
            final def containers = nContainersSomeFixed(
                    10, new FixedContainer(containerId:  containerToRemoveId, order:  containerToRemoveOrder)
            )

        when:
            Containers result = containers.remove(containerToRemoveId)

        then:
            containers.stream().anyMatch({ container -> container.containerId == containerToRemoveId })
            result.stream().noneMatch({ container -> container.containerId == containerToRemoveId })
            containers.order.getOrderOfContainer(containerToRemoveId) == containerToRemoveOrder
            result.order.getOrderOfContainer(containerToRemoveId) == null
            noExceptionThrown()
    }

    def 'should modify one container display text in containers'() {
        given:
            final def containerToModifyId = new ContainerId(randomPageId, UUID.randomUUID())
            final def containers = nContainersSomeFixed(
                    10, new FixedContainer(containerId:  containerToModifyId)
            )
            final def newDisplayText = new DisplayText("modifiedDisplayText")

        when:
            def result = containers.modifyContainer(new ModifyContainer(containerToModifyId, newDisplayText))

        then:
            final def modifiedContainer = getContainerById(result, containerToModifyId)
            with(modifiedContainer) {
                isPresent()
                get().displayText == newDisplayText
            }
            noExceptionThrown()
    }

    def 'should throw exception when source container does not exist in containers'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            final def containers = twoContainersWith10LinksOneFixedInFirstContainer(randomContainerId(), destinationContainerId, linkId)

        when:
            containers.moveLinkBetweenContainers(
                    new LinkMove(linkId, destinationOrder, destinationContainerId)
            )

        then:
            def exception = thrown(DomainException)
            exception.getMessage() == "Container ${sourceContainerId.value} not found"
    }

    def 'should throw exception when destination container does not exist in containers'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            final def containers = twoContainersWith10LinksOneFixedInFirstContainer(sourceContainerId, randomContainerId(), linkId)

        when:
            containers.moveLinkBetweenContainers(
                    new LinkMove(linkId, destinationOrder, destinationContainerId)
            )

        then:
            def exception = thrown(DomainException)
            exception.getMessage() == "Container ${destinationContainerId.value} not found"
    }

    def 'should throw exception when list does not exist in source container'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            final def containers = twoContainersWith10LinksOneFixedInFirstContainer(
                    sourceContainerId, destinationContainerId, new LinkId(sourceContainerId, UUID.randomUUID())
            )

        when:
            containers.moveLinkBetweenContainers(
                    new LinkMove(linkId, destinationOrder, destinationContainerId)
            )

        then:
            def exception = thrown(DomainException)
            exception.getMessage() == "Link ${linkId.value} not found"
    }

    def 'should move link from one container to another'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            final def containers = twoContainersWith10LinksOneFixedInFirstContainer(
                    sourceContainerId, destinationContainerId, linkId
            )

        when:
            Containers result = containers.moveLinkBetweenContainers(
                    new LinkMove(linkId, destinationOrder, destinationContainerId)
            )

        then:
            final def firstContainer = getContainer(result, sourceContainerId)
            final def secondContainer = getContainer(result, destinationContainerId)
            assert firstContainer.links().count() == 9
            assert secondContainer.links().count() == 11
            assert firstContainer.links().noneMatch({ it.linkId == linkId})
            assert secondContainer.links().anyMatch({ it.linkId == linkId})
            assert getLinksOrderAsSet(firstContainer).containsAll(intRange(0, 8))
            assert getLinksOrderAsSet(secondContainer).containsAll(intRange(0, 10))
            assert secondContainer.getLinksOrder()
                    .stream()
                    .anyMatch({ it -> it.order == destinationOrder && it.id == linkId })
            noExceptionThrown()

    }

    def 'should load containers from export model container set'() {
        given:
            final def pageId = new PageId(UUID.randomUUID())
            final def exportModelContainers = PagesExportModelFactory.nContainersWithNLinks(4, 10)

        when:
            Containers containers = Containers.load(pageId, exportModelContainers)

        then:
            containers.pageId == pageId
            containers.stream().count() == 4
            final def sortedContainers = sortContainers(containers)
            for (int i = 0; i < sortedContainers.size(); i++) {
                final def container = sortedContainers.get(i)
                assert container.displayOrder == containers.getOrder().getOrderOfContainer(container.containerId)
                assert container.displayText == new DisplayText("container_" + i)
                assert container.links().count() == 10
            }
            noExceptionThrown()
    }

    private static Optional<Container> getContainerById(Containers containers, containerToModifyId) {
        return containers.stream().filter({ it.containerId == containerToModifyId }).findFirst()
    }

    private static Containers twoContainersWith10LinksOneFixedInFirstContainer(ContainerId firstContainerId,
                                                                               ContainerId secondContainerId,
                                                                               LinkId linkId) {
        return nContainersSomeFixed(2,
                new FixedContainer(
                        firstContainerId,
                        0,
                        nLinksOneFixed(10,
                                new FixedLink(containerId:  firstContainerId, linkId : linkId)
                        )
                ),
                new FixedContainer(secondContainerId, 1, nLinks(10)),
        )
    }

    private static Container getContainer(Containers containers, ContainerId containerId) {
        return containers.stream().filter({ it.containerId == containerId }).findFirst().get()
    }

    private static Set<Integer> getLinksOrderAsSet(Container firstContainer) {
        return firstContainer.getLinksOrder().stream().map({ it.order }).collect(Collectors.toSet())
    }

    private static Set<Integer> intRange(int start, int end) {
        return IntStream.range(start, end).mapToObj(Integer::valueOf).collect(Collectors.toSet())
    }

    private static ContainerId randomContainerId() {
        return new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
    }

    private static List<Container> sortContainers(Containers containers) {
        return containers
                .stream()
                .sorted({ c1, c2 -> Integer.compare(c1.displayOrder, c2.displayOrder) })
                .collect(Collectors.toList())
    }
}
