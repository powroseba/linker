package io.powroseba.page.container


import io.powroseba.page.structs.PageId
import spock.lang.Specification
import spock.lang.Unroll

class ContainerOrderTest extends Specification {

    private static final PageId RANDOM_PAGE_ID = new PageId(UUID.randomUUID())

    def 'should add new container to container order'() {
        given:
            final def containerId = new ContainerId(RANDOM_PAGE_ID, UUID.randomUUID())
            final def order = new ContainerOrder(new HashSet<>())

        when:
            def newOrder = order.add(containerId)

        then:
            newOrder.stream().count() == 1
            newOrder.stream().anyMatch({ entry -> entry.getId() == containerId })
            noExceptionThrown()
    }

    @Unroll
    def 'should reorder containers - should move container from #initialOrder to #destinationOrder'() {
        given:
            final ContainerReorder reorderModel = new ContainerReorder(
                    new ContainerId(RANDOM_PAGE_ID, UUID.randomUUID()),
                    destinationOrder
            )
            final def order = ContainerOrderFactory.nEntriesSomeFixed(
                    10, new ContainerFactory.FixedContainer(containerId : reorderModel.id, order : initialOrder)
            )


        when:
            def result = order.swapOrder(reorderModel)

        then:
            final var resultOrderSize = result.stream().count()
            resultOrderSize == order.stream().count()
            for (int i = 0; i < resultOrderSize; i++) {
                final var oldOrder = findByOrder(order, i)
                final var newOrder = findByOrder(result, i)

                if (initialOrder > destinationOrder) {
                    if (i <= initialOrder && i >= destinationOrder) {
                        assert oldOrder.id != newOrder.id
                    } else if (i == destinationOrder) {
                        assert oldOrder.id != reorderModel.id
                        assert newOrder.id == reorderModel.id
                    } else {
                        assert oldOrder.id == newOrder.id
                    }
                } else if (initialOrder == destinationOrder) {
                    assert oldOrder.id == newOrder.id
                } else {
                    if (i <= destinationOrder && i >= initialOrder) {
                        assert oldOrder.id != newOrder.id
                    } else if (i == destinationOrder) {
                        assert oldOrder.id != reorderModel.id
                        assert newOrder.id == reorderModel.id
                    } else {
                        assert oldOrder.id == newOrder.id
                    }
                }
            }
            noExceptionThrown()

        where:
            initialOrder || destinationOrder
            0            || 2
            2            || 4
            2            || 2
            4            || 2
            8            || 3
            8            || 8

    }

    @Unroll
    def 'should reorder containers order to lower from container on index #initialOrder'() {
        given:
            final def containerId = new ContainerId(RANDOM_PAGE_ID, UUID.randomUUID())
            final def order = ContainerOrderFactory.nEntriesSomeFixed(
                    10, new ContainerFactory.FixedContainer(containerId:  containerId, order : initialOrder)
            )

        when:
            def result = order.removeOrder(containerId)

        then:
            final def resultCount = result.stream().count()
            resultCount == order.stream().count() - 1
            assert findByContainerId(result, containerId) == null
            for (int i = 0; i < resultCount; i++) {
                if (i >= initialOrder) {
                    assert findByOrder(result, i).id == findByOrder(order, i + 1).id
                } else {
                    assert findByOrder(order, i) == findByOrder(result, i)
                }
            }
            noExceptionThrown()

        where:
            initialOrder << [0, 4, 6, 9]

    }


    def 'should return order of container when container exist in container order'() {
        given:
            final order = 2
            final def containerId = new ContainerId(RANDOM_PAGE_ID, UUID.randomUUID())
            final var containerOrder = ContainerOrderFactory.nEntriesSomeFixed(
                    10, new ContainerFactory.FixedContainer(containerId:  containerId, order:  order)
            )

        when:
            def result = containerOrder.getOrderOfContainer(containerId)

        then:
            result == order
            noExceptionThrown()
    }

    private static ContainerOrder.ContainerOrderEntry findByOrder(ContainerOrder order, int index) {
        return order.stream()
                .filter({ it.order == index })
                .findFirst()
                .orElse(null)
    }

    private static ContainerOrder.ContainerOrderEntry findByContainerId(ContainerOrder containerOrder, ContainerId containerId) {
        containerOrder.stream()
                .filter({ entry -> entry.id == containerId })
                .findFirst()
                .orElse(null)
    }
}
