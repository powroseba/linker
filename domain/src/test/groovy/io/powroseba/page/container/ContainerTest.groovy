package io.powroseba.page.container

import io.powroseba.page.export.PagesExportModel
import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.link.Link
import io.powroseba.page.link.LinkFactory
import io.powroseba.page.link.LinkId
import io.powroseba.page.link.LinksFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import io.powroseba.page.structs.WebReference
import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.Stream

class ContainerTest extends Specification {

    private static final PageId randomPageId = new PageId(UUID.randomUUID())
    private static final ContainerId randomContainerId = new ContainerId(randomPageId, UUID.randomUUID())

    def 'should add new link to container'() {
        given:
            final def containerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def container = new Container(containerId, new DisplayText("container"), 0)
            final def displayText = "link"
            final def webAddress = "www.test.pl"

        when:
            def result = container.addLink(new DisplayText(displayText), new WebReference(webAddress))

        then:
            result.links().count() == 1
            noExceptionThrown()
    }

    def 'should add fixed link to container on the same order as fixed link'() {
        given:
            final def destinationOrder = 5
            final def container = ContainerFactory.withLinks(10, new LinkFactory.FixedLink())
            final def linkToAdd = LinkFactory.create(new LinkFactory.FixedLink(order: destinationOrder))

        when:
            def result = container.addLink(linkToAdd)

        then:
            result.links().count() == 11
            result.getLinksOrder().stream().anyMatch({ it.order == 5 && it.id == linkToAdd.linkId })
            noExceptionThrown()
    }

    def 'should remove link'() {
        given:
            final def initialLinkOrder = 2
            final def linkId = new LinkId(randomContainerId, UUID.randomUUID())
            final def container = ContainerFactory.withLinks(10,
                    new LinkFactory.FixedLink(
                            linkId: linkId,
                            order: initialLinkOrder
                    )
            )

        when:
            def result = container.removeLink(linkId)

        then:
            container.links().count() - 1 == result.links().count()
            container.getLinksOrder().getOrderOfEntry(linkId) == initialLinkOrder
            result.getLinksOrder().getOrderOfEntry(linkId) == null
            noExceptionThrown()
    }

    def 'should change container display text'() {
        given:
            final def newDisplayText = new DisplayText("newDisplayText")
            final def container = ContainerFactory.withText("initialDisplayText")

        when:
            def result = container.modifyDisplayText(newDisplayText)

        then:
            with(result) {
                containerId == container.containerId
                displayText == newDisplayText
                displayOrder == container.displayOrder
            }
            noExceptionThrown()
    }

    def 'should create valid export model of container'() {
        given:
            final def inputContainerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            final def link = new LinkFactory.FixedLink(
                    inputContainerId,
                    new LinkId(inputContainerId, UUID.randomUUID())
            )
            final def container = new Container(
                    inputContainerId,
                    new DisplayText("text"),
                    0,
                    LinksFactory.nLinksOneFixed(1, link)
            )

        when:
            PagesExportModel.Container exportModel = container.createExportModel()

        then:
            with(exportModel) {
                containerId == container.containerId.value
                displayText == container.displayText.content
                displayOrder == container.displayOrder
                links == container.links().map({ it.createExportModel() }).collect(Collectors.toSet())
            }
            noExceptionThrown()
    }

    def 'should load container from export model container'() {
        given:
            final def pageId = new PageId(UUID.randomUUID())
            final def exportModelContainer = PagesExportModelFactory.containerWithNLinks(0, 10)

        when:
            Container container = Container.load(pageId, exportModelContainer)

        then:
            container != null
            with(container) {
                displayOrder == 0
                displayText == new DisplayText("container_" + 0)
                containerId != null
                containerId.pageId == pageId
                links().count() == 10
            }
            noExceptionThrown()
    }
}
