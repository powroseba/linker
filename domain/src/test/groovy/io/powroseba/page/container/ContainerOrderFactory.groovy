package io.powroseba.page.container

import io.powroseba.page.container.ContainerFactory.FixedContainer

final class ContainerOrderFactory {

    private ContainerOrderFactory() {}

    static ContainerOrder nEntriesSomeFixed(int n, FixedContainer... containers) {
        final def set = new HashSet()
        final def pageId = FixedContainer.randomOfFixedPageId(containers)
        for (int i = 0; i < n; i++) {
            final def fixedContainer = FixedContainer.getByOrder(containers, i)
            if (fixedContainer.isPresent()) {
                set.add(new ContainerOrder.ContainerOrderEntry(i, fixedContainer.get().containerId))
            } else {
                set.add(new ContainerOrder.ContainerOrderEntry(i, new ContainerId(pageId, UUID.randomUUID())))
            }
        }
        return new ContainerOrder(set)
    }

}
