package io.powroseba.page.container

import io.powroseba.page.structs.PageId
import spock.lang.Specification

class ContainerOrderEntryTest extends Specification {

    public static final ContainerOrder.ContainerOrderEntry exampleContainerOrder = new ContainerOrder.ContainerOrderEntry(
            2, new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
    )

    def 'should increase order'() {
        when:
            def result = exampleContainerOrder.increase()

        then:
            result == fromExampleContainerOrder(exampleContainerOrder.getOrder() + 1)
            noExceptionThrown()
    }

    def 'should decrease order'() {
        when:
            def result = exampleContainerOrder.decrease()

        then:
            result == fromExampleContainerOrder(exampleContainerOrder.getOrder() - 1)
            noExceptionThrown()
    }

    def 'should not decrease order with order = 0'() {
        given:
            final def pageOrder = fromExampleContainerOrder(0)

        when:
            def result = pageOrder.decrease()

        then:
            result == pageOrder
            noExceptionThrown()
    }

    def 'should change order'() {
        when:
            def result = exampleContainerOrder.changeOrder(3)

        then:
            result == fromExampleContainerOrder(3)
            noExceptionThrown()

    }

    def 'should change order at least to 0 when input order is less than 0'() {
        when:
            def result = exampleContainerOrder.changeOrder(-1)

        then:
            result == fromExampleContainerOrder(0)
            noExceptionThrown()

    }

    private static ContainerOrder.ContainerOrderEntry fromExampleContainerOrder(int order) {
        new ContainerOrder.ContainerOrderEntry(order, exampleContainerOrder.getId())
    }
    
    
}
