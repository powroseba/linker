package io.powroseba.page.export


import java.util.stream.Collectors
import java.util.stream.IntStream

final class PagesExportModelFactory {

    private PagesExportModelFactory() {}

    static PagesExportModel with2PagesIncluding4ContainersIncluding10Links() {
        return new PagesExportModel(nPagesWithNContainersNLinks(2, 4, 10))
    }

    static Set<PagesExportModel.Page> nPagesWithNContainersNLinks(int countOfPages, int countOfContainers, int countOfLinks) {
        return IntStream.range(0, countOfPages)
                .mapToObj({ index -> pageWithNContainersNLinks(index, countOfContainers, countOfLinks) })
                .collect(Collectors.toSet())
    }

    static PagesExportModel.Page pageWithNContainersNLinks(int index = 0, int countOfContainers, int countOfLinks) {
        return new PagesExportModel.Page(
                UUID.randomUUID(),
                index,
                "page_" + index,
                nContainersWithNLinks(countOfContainers, countOfLinks)
        )
    }

    static Set<PagesExportModel.Container> nContainersWithNLinks(int countOfContainers, int countOfLinks) {
        return IntStream.range(0, countOfContainers)
                .mapToObj({ index -> containerWithNLinks(index, countOfLinks) })
                .collect(Collectors.toSet())
    }

    static PagesExportModel.Container containerWithNLinks(int index = 0, int countOfLinks) {
        final def containerId = UUID.randomUUID()
        return new PagesExportModel.Container(
                containerId,
                "container_" + index,
                index,
                nLinks(countOfLinks)
        )
    }

    static Set<PagesExportModel.Link> nLinks(int countOfLinks) {
        return IntStream.range(0, countOfLinks)
                .mapToObj({ index -> link(index)})
                .collect(Collectors.toSet())
    }

    static PagesExportModel.Link link(int index = 0) {
        return new PagesExportModel.Link(
                UUID.randomUUID(),
                "link_" + index,
                "webReference_" + index + ".com",
                "image_" + index + ".jpg",
                index
        )
    }
}
