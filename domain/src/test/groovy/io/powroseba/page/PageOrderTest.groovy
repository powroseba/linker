package io.powroseba.page

import io.powroseba.page.structs.PageId
import spock.lang.Specification
import spock.lang.Unroll

class PageOrderTest extends Specification {
    
    def 'should add new page order'() {
        given: 'order with 10 entries'
            final var pageOrder = randomPageOrderWith10Entries()
            final var pageIdToAdd = new PageId(UUID.randomUUID())

        when:
            def result = pageOrder.add(pageIdToAdd)

        then:
            result.stream().count() == pageOrder.stream().count() + 1
            noExceptionThrown()
    }

    @Unroll
    def 'should reorder pages - should move page from #initialIndexOfPage to #destinationIndex'() {
        given:
            final def pageReorder = new PageReorder(new PageId(UUID.randomUUID()), destinationIndex)
            final def order = PageOrderFactory.nEntriesOneFixed(
                    10,
                    new PagesFactory.FixedPage(pageReorder.id, initialIndexOfPage)
            )

        when:
            def result = order.swapOrder(pageReorder)

        then:
            final var resultOrderSize = result.stream().count()
            resultOrderSize == order.stream().count()
            for (int i = 0; i < resultOrderSize; i++) {
                final var oldOrder = findByOrder(order, i)
                final var newOrder = findByOrder(result, i)

                if (initialIndexOfPage > destinationIndex) {
                    if (i <= initialIndexOfPage && i >= destinationIndex) {
                        assert oldOrder.getId() != newOrder.getId()
                    } else if (i == destinationIndex) {
                        assert oldOrder.getId() != pageReorder.id
                        assert newOrder.getId() == pageReorder.id
                    } else {
                        assert oldOrder.getId() == newOrder.getId()
                    }
                } else if(initialIndexOfPage == destinationIndex) {
                    assert oldOrder.getId() == newOrder.getId()
                } else {
                    if (i <= destinationIndex && i >= initialIndexOfPage) {
                        assert oldOrder.getId() != newOrder.getId()
                    } else if (i == destinationIndex) {
                        assert oldOrder.getId() != pageReorder.id
                        assert newOrder.getId() == pageReorder.id
                    } else {
                        assert oldOrder.getId() == newOrder.getId()
                    }
                }
            }
            noExceptionThrown()

        where:
            initialIndexOfPage || destinationIndex
            0                  || 2
            2                  || 4
            2                  || 2
            4                  || 2
            8                  || 3
            8                  || 8
    }

    @Unroll
    def 'should reorder pages order to lower from page on index #initialOrder'() {
        given:
            final def pageId = new PageId(UUID.randomUUID())
            final def order = PageOrderFactory.nEntriesOneFixed(10, new PagesFactory.FixedPage(pageId, initialOrder))

        when:
            def result = order.removeOrder(pageId)

        then:
            final def resultCount = result.stream().count()
            resultCount == order.stream().count() - 1
            assert findByPageId(result, pageId) == null
            for (int i = 0; i < resultCount; i++) {
                if (i >= initialOrder) {
                    assert findByOrder(result, i).getId() == findByOrder(order, i + 1).getId()
                } else {
                    assert findByOrder(order, i) == findByOrder(result, i)
                }
            }
            noExceptionThrown()

        where:
            initialOrder << [0, 4, 6, 9]

    }

    def 'should return order of page when page exist in page order'() {
        given:
            final def order = 2
            final def pageId = new PageId(UUID.randomUUID())
            final var pageOrder = PageOrderFactory.nEntriesOneFixed(10, new PagesFactory.FixedPage(pageId, order))

        when:
            def result = pageOrder.getOrderOfPage(pageId)

        then:
            result == order
            noExceptionThrown()
    }

    private static PageOrder.PageOrderEntry findByOrder(PageOrder pageOrder, int order) {
        pageOrder.stream().filter({ entry -> entry.getOrder() == order }).findFirst().orElse(null)
    }

    private static PageOrder.PageOrderEntry findByPageId(PageOrder pageOrder, PageId pageId) {
        pageOrder.stream().filter({ entry -> entry.getId() == pageId }).findFirst().orElse(null)
    }

    private static PageOrder randomPageOrderWith10Entries() {
        return PageOrderFactory.nEntries(10)
    }
}
