package io.powroseba.page

import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.export.PagesExportModelFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import spock.lang.Specification

import java.util.stream.Collectors

class PageTest extends Specification {

    def 'should create valid export model of page'() {
        given:
            final def inputPageId = new PageId(UUID.randomUUID())
            final def container = new ContainerFactory.FixedContainer(
                    pageId: inputPageId,
                    containerId: new ContainerId(inputPageId, UUID.randomUUID())
            )
            final def page = new Page(
                    inputPageId,
                    new DisplayText("text"),
                    0,
                    ContainersFactory.nContainersSomeFixed(1, container)
            )

        when:
            def exportModel = page.createExportModel()

        then:
            with(exportModel) {
                pageId == page.pageId.value
                displayText == page.displayText
                displayOrder == page.displayOrder
                containers == page.containers().map({ it.createExportModel() }).collect(Collectors.toSet())
            }
            noExceptionThrown()

    }

    def 'should load page from page export model'() {
        given:
            final def exportModelPage = PagesExportModelFactory.pageWithNContainersNLinks(0, 2, 10)

        when:
            Page page = Page.load(exportModelPage)

        then:
            page != null
            with(page) {
                displayOrder == 0
                getDisplayText() == "page_" + 0
                pageId.value == exportModelPage.pageId
                containers().count() == 2
                containers().flatMap({ it.links() }).count() == 20

            }
            noExceptionThrown()
    }
}
