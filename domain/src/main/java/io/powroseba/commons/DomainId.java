package io.powroseba.commons;

import java.util.Objects;

public abstract class DomainId<T> {

    public final T value;

    public DomainId(T value) {
        if (value == null) {
            throw new IllegalArgumentException("Identifier cannot be null!");
        }
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainId<?> domainId = (DomainId<?>) o;
        return Objects.equals(value, domainId.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
