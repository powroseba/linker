package io.powroseba.commons;

public abstract class DomainException extends RuntimeException {

    public DomainException(String message) {
        super(message);
    }
}
