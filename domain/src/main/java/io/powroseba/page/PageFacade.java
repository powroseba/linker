package io.powroseba.page;

import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.export.PagesExporter;
import io.powroseba.page.structs.NewPage;
import io.powroseba.page.structs.PageId;
import io.powroseba.page.structs.PageWithoutColumn;

import java.util.Collection;
import java.util.Optional;

public class PageFacade {

    private final PagesRepository pagesRepository;
    private final PageRepository pageRepository;

    public PageFacade(PagesRepository pagesRepository, PageRepository pageRepository) {
        this.pagesRepository = pagesRepository;
        this.pageRepository = pageRepository;
    }

    public void reorder(PageReorder reorderModel) {
        var pages = pagesRepository.get()
                .orElseThrow(() -> new PageException.PageNotFound(reorderModel.id));
        pages = pages.reorder(reorderModel);
        pagesRepository.save(pages);
    }

    public Collection<PageWithoutColumn> findAllWithoutColumns() {
        return pagesRepository.findAllWithoutColumns();
    }

    public void addPage(NewPage page) {
        var pages = pagesRepository.get().orElse(Pages.empty());
        pages = pages.add(page.pageName);
        pagesRepository.save(pages);
    }

    public Page findById(PageId pageId) {
        return pageRepository.findById(pageId)
                .orElseThrow(() -> new PageException.PageNotFound(pageId));
    }

    public void removePage(PageId pageIdentifier) {
        var pages = pagesRepository.get()
                .orElseThrow(() -> new PageException.PageNotFound(pageIdentifier));
        pages = pages.remove(pageIdentifier);
        pagesRepository.save(pages);
    }

    public <O> Optional<O> export(PagesExporter<O> pagesExporter) {
        return pagesRepository.get()
                .map(pages -> pages.export(pagesExporter));
    }

    public void load(PagesExportModel exportModel) {
        pagesRepository.save(Pages.load(exportModel));
    }
}
