package io.powroseba.page;

import io.powroseba.page.container.Container;
import io.powroseba.page.container.ContainerId;
import io.powroseba.page.container.Containers;
import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.PageId;

import java.util.stream.Collectors;
import java.util.stream.Stream;

class Page {

    final DisplayText displayText;
    final PageId pageId;
    final int displayOrder;
    private final Containers containers;

    Page(PageId pageId, DisplayText displayText, int displayOrder, Containers containers) {
        this.pageId = pageId;
        this.displayText = displayText;
        this.displayOrder = displayOrder;
        this.containers = containers;
    }

    Page(PageId pageId, DisplayText displayText, int displayOrder) {
        this(pageId, displayText, displayOrder, Containers.empty(pageId));
    }

    Page(Page page, int displayOrder) {
        this(page.pageId, page.displayText, displayOrder, page.containers);
    }

    public static Page load(PagesExportModel.Page page) {
        final var pageId = new PageId(page.pageId);
        return new Page(
                pageId,
                new DisplayText(page.displayText),
                page.displayOrder,
                Containers.load(pageId, page.containers)
        );
    }

    public PagesExportModel.Page createExportModel() {
        return new PagesExportModel.Page(
                pageId.value,
                displayOrder,
                displayText.content,
                containers().map(Container::createExportModel).collect(Collectors.toSet())
        );
    }

    Stream<Container> containers() {
        return containers.stream();
    }

    AbstractOrder<ContainerId, ? extends AbstractOrderEntry<?, ?>, ? extends AbstractOrder<?, ?, ?>> getContainersOrder() {
        return containers.getOrder();
    }

    String getDisplayText() {
        return displayText.content;
    }
}
