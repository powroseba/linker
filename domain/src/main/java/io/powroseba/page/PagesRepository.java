package io.powroseba.page;

import io.powroseba.page.structs.PageWithoutColumn;

import java.util.Collection;
import java.util.Optional;

public interface PagesRepository {

    Optional<Pages> get();

    void save(Pages pages);

    Collection<PageWithoutColumn> findAllWithoutColumns();

}
