package io.powroseba.page.structs;

import io.powroseba.page.container.ContainerId;

import java.util.UUID;

public class ModifyContainer {

    public final ContainerId containerId;
    public final DisplayText containerName;

    public ModifyContainer(UUID pageId, UUID containerId, DisplayText containerName) {
        this.containerId = new ContainerId(new PageId(pageId), containerId);
        this.containerName = containerName;
    }

    public ModifyContainer(ContainerId containerId, DisplayText containerName) {
        this.containerId = containerId;
        this.containerName = containerName;
    }
}
