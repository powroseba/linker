package io.powroseba.page.structs;

public final class NewPage {

    public final DisplayText pageName;

    public NewPage(DisplayText pageName) {
        this.pageName = pageName;
    }
}
