package io.powroseba.page.structs;

import java.util.UUID;

public final class NewContainer {

    public final PageId pageId;
    public final DisplayText containerName;

    public NewContainer(UUID pageId, String containerName) {
        this.pageId = new PageId(pageId);
        this.containerName = new DisplayText(containerName);
    }
}
