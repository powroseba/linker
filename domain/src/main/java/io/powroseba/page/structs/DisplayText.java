package io.powroseba.page.structs;

import java.util.Objects;

public final class DisplayText {

    public final String content;

    public DisplayText(String content) {
        if (content == null || content.isBlank() || content.isEmpty()) {
            throw new IllegalArgumentException("Content cannot be empty or blank");
        }
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisplayText that = (DisplayText) o;
        return Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }
}
