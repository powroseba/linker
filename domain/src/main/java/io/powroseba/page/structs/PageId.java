package io.powroseba.page.structs;

import io.powroseba.commons.DomainId;

import java.util.UUID;

public final class PageId extends DomainId<UUID> {

    public PageId(UUID value) {
        super(value);
    }
}
