package io.powroseba.page.structs;

public final class PageWithoutColumn {

    public final PageId pageId;
    public final String displayText;
    public final int displayOrder;

    public PageWithoutColumn(PageId pageId, String displayText, int displayOrder) {
        this.pageId = pageId;
        this.displayText = displayText;
        this.displayOrder = displayOrder;
    }
}
