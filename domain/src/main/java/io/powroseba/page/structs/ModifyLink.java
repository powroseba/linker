package io.powroseba.page.structs;

import io.powroseba.page.link.LinkId;

public class ModifyLink {

    public final LinkId linkId;
    public final DisplayText displayText;
    public final WebReference webReference;
    public final ImageAddress imageAddress;

    public ModifyLink(LinkId linkId, DisplayText displayText, WebReference webReference, ImageAddress imageAddress) {
        this.linkId = linkId;
        this.displayText = displayText;
        this.webReference = webReference;
        this.imageAddress = imageAddress;
    }
}
