package io.powroseba.page.structs;

import java.util.Objects;

public final class WebReference {

    public final String webLink;

    public WebReference(String value) {
        this.webLink = clean(value);
    }

    private String clean(String value) {
        return value
                .replaceFirst("https://", "")
                .replaceFirst("http://", "")
                .replaceFirst("www.", "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WebReference that = (WebReference) o;
        return Objects.equals(webLink, that.webLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(webLink);
    }
}
