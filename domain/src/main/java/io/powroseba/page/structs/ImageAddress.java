package io.powroseba.page.structs;

import java.util.Objects;

public final class ImageAddress {

    public final String address;

    public ImageAddress(String address) {
        if (!validateAddressOfImage(address)) {
            throw new IllegalArgumentException("Address to image is invalid!");
        }
        this.address = address;
    }

    private boolean validateAddressOfImage(String address) {
        return address == null || isValidExtension(address);
    }

    private boolean isValidExtension(String address) {
        return address.contains(".png") || address.contains(".jpg") || address.contains(".jpeg");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageAddress that = (ImageAddress) o;
        return Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
