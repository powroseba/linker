package io.powroseba.page.structs;

import io.powroseba.page.container.ContainerId;

import java.util.UUID;

public class NewLink {

    public final ContainerId containerId;
    public final DisplayText displayText;
    public final WebReference webReference;

    public NewLink(UUID pageId, UUID containerId, String displayText, String webReference) {
        this.containerId = new ContainerId(new PageId(pageId), containerId);
        this.displayText = new DisplayText(displayText);
        this.webReference = new WebReference(webReference);
    }
}
