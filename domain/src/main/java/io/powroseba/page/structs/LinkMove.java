package io.powroseba.page.structs;

import io.powroseba.page.container.ContainerId;
import io.powroseba.page.link.LinkId;

public class LinkMove {

    public final LinkId linkId;
    public final int destinationOrder;
    public final ContainerId sourceContainerId;
    public final ContainerId destinationContainerId;

    public LinkMove(LinkId linkId, int destinationOrder, ContainerId destinationContainerId) {
        this.linkId = linkId;
        this.destinationOrder = destinationOrder;
        this.sourceContainerId = linkId.containerId;
        this.destinationContainerId = destinationContainerId;
    }
}
