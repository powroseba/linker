package io.powroseba.page.export;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public final class PagesExportModel {

    public final Set<Page> pages;

    public PagesExportModel(Set<Page> pages) {
        this.pages = pages;
    }

    public static final class Page {

        public final UUID pageId;
        public final int displayOrder;
        public final String displayText;
        public final Set<Container> containers;

        public Page(UUID pageId, int displayOrder, String displayText, Set<Container> containers) {
            this.pageId = pageId;
            this.displayOrder = displayOrder;
            this.displayText = displayText;
            this.containers = containers;
        }
    }

    public static final class Container {
        public final UUID containerId;
        public final String displayText;
        public final int displayOrder;
        public final Set<Link> links;

        public Container(UUID containerId, String displayText, int displayOrder, Set<Link> links) {
            this.containerId = containerId;
            this.displayText = displayText;
            this.displayOrder = displayOrder;
            this.links = links;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Container container = (Container) o;
            return displayOrder == container.displayOrder
                    && Objects.equals(containerId, container.containerId)
                    && Objects.equals(displayText, container.displayText)
                    && Objects.equals(links, container.links);
        }

        @Override
        public int hashCode() {
            return Objects.hash(containerId, displayText, displayOrder, links);
        }
    }

    public static final class Link {
        public final UUID linkId;
        public final String displayText;
        public final String webReference;
        public final String imageAddress;
        public final int displayOrder;

        public Link(UUID linkId, String displayText, String webReference, String imageAddress, int displayOrder) {
            this.linkId = linkId;
            this.displayText = displayText;
            this.webReference = webReference;
            this.imageAddress = imageAddress;
            this.displayOrder = displayOrder;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Link link = (Link) o;
            return displayOrder == link.displayOrder
                    && Objects.equals(linkId, link.linkId)
                    && Objects.equals(displayText, link.displayText)
                    && Objects.equals(webReference, link.webReference)
                    && Objects.equals(imageAddress, link.imageAddress);
        }

        @Override
        public int hashCode() {
            return Objects.hash(linkId, displayText, webReference, imageAddress, displayOrder);
        }
    }

}
