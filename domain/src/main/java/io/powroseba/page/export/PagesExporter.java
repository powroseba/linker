package io.powroseba.page.export;

public interface PagesExporter<O> {

    O export(PagesExportModel pagesExportModel);
}
