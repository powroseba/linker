package io.powroseba.page;

import io.powroseba.commons.DomainException;
import io.powroseba.page.structs.PageId;

public class PageException {

    public static class PageNotFound extends DomainException {

        public PageNotFound(PageId pageId) {
            super("Page " + pageId.value + " not found");
        }
    }
}
