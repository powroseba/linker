package io.powroseba.page;

import io.powroseba.page.structs.PageId;

import java.util.Optional;

public interface PageRepository {

    Optional<Page> findById(PageId pageId);
}
