package io.powroseba.page.link;

import io.powroseba.page.PageException;
import io.powroseba.page.container.ContainerException;
import io.powroseba.page.container.ContainerId;
import io.powroseba.page.container.ContainersRepository;
import io.powroseba.page.structs.LinkMove;
import io.powroseba.page.structs.ModifyLink;
import io.powroseba.page.structs.NewLink;

public class LinkFacade {

    private final LinksRepository linksRepository;
    private final LinkRepository linkRepository;
    private final ContainersRepository containersRepository;

    public LinkFacade(LinksRepository linksRepository, LinkRepository linkRepository, ContainersRepository containersRepository) {
        this.linksRepository = linksRepository;
        this.linkRepository = linkRepository;
        this.containersRepository = containersRepository;
    }

    public void reorder(LinkReorder reorderModel) {
        final var links = linksRepository.findByContainerId(reorderModel.id.containerId)
                .orElseThrow(() -> new LinkException.LinkNotFound(reorderModel.id));
        linksRepository.save(links.reorder(reorderModel));
    }

    public void move(LinkMove linkMove) {
        final var pageContainers= containersRepository.findByPageId(linkMove.sourceContainerId.pageId)
                .orElseThrow(() -> new PageException.PageNotFound(linkMove.sourceContainerId.pageId));
        containersRepository.save(pageContainers.moveLinkBetweenContainers(linkMove));
    }

    public void removeLinkFromContainer(LinkId linkId) {
        final var links = linksRepository.findByContainerId(linkId.containerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(linkId.containerId));
        linksRepository.save(links.remove(linkId));
    }

    public void addLinkToContainer(NewLink newLink) {
        final var links = linksRepository.findByContainerId(newLink.containerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(newLink.containerId));
        linksRepository.save(links.add(newLink.displayText, newLink.webReference));
    }

    public void modifyLink(ModifyLink modifyLink) {
        final var links = linksRepository.findByContainerId(modifyLink.linkId.containerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(modifyLink.linkId.containerId));
        linksRepository.save(links.modifyLink(modifyLink));
    }

    public ContainerId getLinkContainer(LinkId linkId) {
        return linkRepository.getContainerOfLink(linkId).orElseThrow(() -> new LinkException.LinkNotFound(linkId));
    }
}
