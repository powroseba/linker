package io.powroseba.page.link;

import io.powroseba.commons.DomainException;

public class LinkException {

    public static class LinkNotFound extends DomainException {

        public LinkNotFound(LinkId linkId) {
            super("Link " + linkId.value + " not found");
        }
    }
}
