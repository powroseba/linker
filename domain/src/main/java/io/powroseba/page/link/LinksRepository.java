package io.powroseba.page.link;

import io.powroseba.page.container.ContainerId;

import java.util.Optional;

public interface LinksRepository {
    Optional<Links> findByContainerId(ContainerId pageId);

    void save(Links links);
}
