package io.powroseba.page.link;

import io.powroseba.page.container.ContainerId;
import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.structs.*;

public class Link {

    public final LinkId linkId;
    final DisplayText displayText;
    final WebReference webReference;
    final ImageAddress image;
    final int displayOrder;
    private final ContainerId containerId;

    public Link(Link link, int displayOrder) {
        this(link.containerId, link.linkId, link.displayText, link.webReference, link.image, displayOrder);
    }

    Link(ContainerId containerId, LinkId linkId, DisplayText displayText, WebReference webReference,
         ImageAddress image, int displayOrder) {
        this.containerId = containerId;
        this.linkId = linkId;
        this.displayText = displayText;
        this.webReference = webReference;
        this.image = image;
        this.displayOrder = displayOrder;
    }

    Link(ContainerId containerId, LinkId linkId, DisplayText displayText, WebReference webReference, int displayOrder) {
        this(containerId, linkId, displayText, webReference, null, displayOrder);
    }

    public static Link load(final ContainerId containerId, final PagesExportModel.Link exportModel) {
        return new Link(
                containerId,
                new LinkId(containerId, exportModel.linkId),
                new DisplayText(exportModel.displayText),
                new WebReference(exportModel.webReference),
                exportModel.imageAddress != null ? new ImageAddress(exportModel.imageAddress) : null,
                exportModel.displayOrder
        );
    }

    public Link modify(ModifyLink modifyLink) {
        return new Link(containerId, linkId, modifyLink.displayText, modifyLink.webReference, modifyLink.imageAddress, displayOrder);
    }

    public PagesExportModel.Link createExportModel() {
        return new PagesExportModel.Link(
                linkId.value,
                displayText.content,
                webReference.webLink,
                image.address,
                displayOrder
        );
    }
}
