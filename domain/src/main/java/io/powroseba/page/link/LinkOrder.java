package io.powroseba.page.link;

import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;

import java.util.Set;

class LinkOrder extends AbstractOrder<LinkId, LinkOrder.LinkOrderEntry, LinkOrder> {

    LinkOrder(Set<LinkOrderEntry> entries) {
        super(entries);
    }

    @Override
    protected LinkOrderEntry entry(int order, LinkId id) {
        return new LinkOrderEntry(order, id);
    }

    @Override
    protected LinkOrder order(Set<LinkOrderEntry> entries) {
        return new LinkOrder(entries);
    }

    Integer getOrderOfLink(LinkId linkId) {
        return getOrderOfEntry(linkId);
    }

    static class LinkOrderEntry extends AbstractOrderEntry<LinkId, LinkOrderEntry> {

        LinkOrderEntry(int order, LinkId id) {
            super(order, id);
        }

        @Override
        protected LinkOrderEntry of(int order, LinkId id) {
            return new LinkOrderEntry(order, id);
        }
    }
}
