package io.powroseba.page.link;

import io.powroseba.commons.DomainId;
import io.powroseba.page.container.ContainerId;

import java.util.UUID;

public final class LinkId extends DomainId<UUID> {

    public final ContainerId containerId;

    public LinkId(ContainerId containerId, UUID value) {
        super(value);
        this.containerId = containerId;
    }
}