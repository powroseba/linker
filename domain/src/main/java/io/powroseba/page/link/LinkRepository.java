package io.powroseba.page.link;

import io.powroseba.page.container.ContainerId;

import java.util.Optional;

public interface LinkRepository {

    Optional<ContainerId> getContainerOfLink(LinkId linkId);
}
