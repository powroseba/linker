package io.powroseba.page.link;

import io.powroseba.page.order.ReorderModel;

public final class LinkReorder extends ReorderModel<LinkId> {

    public LinkReorder(LinkId id, int destinationOrder) {
        super(id, destinationOrder);
    }
}
