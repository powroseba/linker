package io.powroseba.page.link;

import io.powroseba.page.container.ContainerId;
import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.ModifyLink;
import io.powroseba.page.structs.WebReference;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Links {

    public final ContainerId containerId;
    private final LinkOrder order;
    private final Set<Link> links;

    public Links(ContainerId containerId, LinkOrder order, Set<Link> links) {
        this.containerId = containerId;
        this.order = order;
        this.links = links;
    }

    Links(ContainerId containerId) {
        this(containerId, new LinkOrder(new HashSet<>()), new HashSet<>());
    }

    public static Links empty(ContainerId containerId) {
        return new Links(containerId);
    }

    public static Links load(final ContainerId containerId, final Set<PagesExportModel.Link> links) {
        final Function<PagesExportModel.Link, LinkOrder.LinkOrderEntry> orderCreator =
                link -> new LinkOrder.LinkOrderEntry(link.displayOrder, new LinkId(containerId, link.linkId));
        return new Links(
                containerId,
                new LinkOrder(
                        links.stream()
                                .map(orderCreator)
                                .collect(Collectors.toSet())
                ),
                links.stream()
                        .map(link -> Link.load(containerId, link))
                        .collect(Collectors.toSet())
        );
    }

    public Links add(DisplayText displayText, WebReference webReference) {
        final var id = new LinkId(this.containerId, UUID.randomUUID());
        final var newOrder = this.order.add(id);
        final var links = new HashSet<>(this.links);
        links.add(new Link(this.containerId, id, displayText, webReference, newOrder.getOrderOfLink(id)));
        return new Links(this.containerId, newOrder, links);
    }

    public Links add(Link link) {
        final var newOrder = this.order.add(link.linkId).swapOrder(new LinkReorder(link.linkId, link.displayOrder));
        final var links = new HashSet<>(this.links);
        links.add(link);
        return new Links(this.containerId, newOrder, links);
    }

    public Links reorder(LinkReorder reorderModel) {
        if (order.getOrderOfLink(reorderModel.id) == null) {
            throw new LinkException.LinkNotFound(reorderModel.id);
        }
        final var newOrder = order.swapOrder(reorderModel);
        final var links = this.links.stream()
                .map(link -> new Link(link, newOrder.getOrderOfLink(link.linkId)))
                .collect(Collectors.toSet());
        return new Links(this.containerId, newOrder, new HashSet<>(links));
    }

    public Links remove(LinkId linkId) {
        if (isLinkDoesNotExist(linkId)) {
            throw new LinkException.LinkNotFound(linkId);
        }
        final var newLinks = new HashSet<>(this.links);
        newLinks.removeIf(link -> link.linkId.equals(linkId));
        final var newOrder = order.removeOrder(linkId);
        return new Links(this.containerId, newOrder, newLinks);
    }

    public Links modifyLink(ModifyLink modifyLink) {
        final var linkToModify = linkWithId(modifyLink.linkId)
                .orElseThrow(() -> new LinkException.LinkNotFound(modifyLink.linkId));
        final var linksWithoutModified = linksWithout(modifyLink.linkId);
        linksWithoutModified.add(linkToModify.modify(modifyLink));
        return new Links(this.containerId, getOrder(), linksWithoutModified);
    }

    public Stream<Link> stream() {
        return links.stream();
    }

    public LinkOrder getOrder() {
        return order;
    }

    private boolean isLinkDoesNotExist(LinkId linkId) {
        return links.stream().noneMatch(link -> link.linkId.equals(linkId));
    }

    private Optional<Link> linkWithId(LinkId linkId) {
        return stream()
                .filter(link -> link.linkId.equals(linkId))
                .findFirst();
    }

    private Set<Link> linksWithout(LinkId linkId) {
        return stream()
                .filter(link -> !link.linkId.equals(linkId))
                .collect(Collectors.toSet());
    }
}
