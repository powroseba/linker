package io.powroseba.page.order;

import io.powroseba.commons.DomainId;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public abstract class AbstractOrder<I extends DomainId<?>, E extends AbstractOrderEntry<I, E>, O extends AbstractOrder<I, E, O>> {

    protected final Set<E> entries;

    protected AbstractOrder(Set<E> entries) {
        this.entries = new HashSet<>(entries);
    }

    protected abstract E entry(int order, I id);
    protected abstract O order(Set<E> entries);

    public O add(I id) {
        final var entries = new HashSet<>(this.entries);
        entries.add(entry(entries.size(), id));
        return order(entries);
    }

    public O swapOrder(ReorderModel<I> reorderModel) {
        final var newOrders = new HashSet<E>();
        final var entryInMove = getEntryById(reorderModel.id);
        if (entryInMove.isEmpty() || entryInMove.get().getOrder().equals(reorderModel.destinationOrder)) {
            return order(Set.copyOf(entries));
        }
        final var shifter = Shifter.matchShifter(entryInMove.get(), reorderModel.destinationOrder);
        for (E order : entries) {
            newOrders.add(shifter.shift(order));
        }
        return order(newOrders);
    }

    public O removeOrder(I id) {
        final var newOrders = new HashSet<E>();
        final var containerToRemove = getEntryById(id);
        if (containerToRemove.isEmpty()) {
            return (O) this;
        }
        for (E entry : entries) {
            if (entry.getOrder() > containerToRemove.get().getOrder()) {
                newOrders.add(entry.decrease());
            }
            if (entry.getOrder() < containerToRemove.get().getOrder()) {
                newOrders.add(entry);
            }
        }
        return order(newOrders);
    }

    public Stream<E> stream() {
        return this.entries.stream();
    }

    protected Optional<E> getEntryById(I id) {
        return entries.stream()
                .filter(order -> order.id.equals(id))
                .findFirst();
    }

    protected Integer getOrderOfEntry(I id) {
        return getEntryById(id).map(AbstractOrderEntry::getOrder).orElse(null);
    }

}
