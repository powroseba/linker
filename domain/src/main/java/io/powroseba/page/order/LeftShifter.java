package io.powroseba.page.order;

import io.powroseba.commons.DomainId;

class LeftShifter<I extends DomainId<?>, E extends AbstractOrderEntry<I, E>> implements Shifter<I, E> {
    private final AbstractOrderEntry<?, ?> entryInMove;
    private final Integer newOrder;

    LeftShifter(AbstractOrderEntry<?, ?> entryInMove, Integer newOrder) {
        this.entryInMove = entryInMove;
        this.newOrder = newOrder;
    }

    @Override
    public E shift(E current) {
        if (current.equals(entryInMove)) {
            return current.changeOrder(newOrder);
        } else if (current.getOrder() >= newOrder && current.getOrder() < entryInMove.getOrder()) {
            return current.increase();
        } else {
            return current;
        }
    }
}
