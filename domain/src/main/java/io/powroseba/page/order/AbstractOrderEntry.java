package io.powroseba.page.order;

import io.powroseba.commons.DomainId;

import java.util.Objects;

public abstract class AbstractOrderEntry<I extends DomainId<?>, E extends AbstractOrderEntry<I, E>> {

    protected final int order;
    protected final I id;

    protected AbstractOrderEntry(int order, I id) {
        this.order = order;
        this.id = id;
    }

    protected abstract E of(int order, I id);

    E increase() {
        return of(this.order + 1, this.id);
    }

    E decrease() {
        return of(this.order <= 0 ? 0 : this.order - 1, this.id);
    }

    E changeOrder(Integer order) {
        return of(Math.max(order, 0), this.id);
    }

    public Integer getOrder() {
        return order;
    }

    public I getId() { return id; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractOrderEntry<I, E> that = (AbstractOrderEntry<I, E>) o;
        return order == that.order && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order, id);
    }
}
