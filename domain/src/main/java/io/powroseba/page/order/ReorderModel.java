package io.powroseba.page.order;

import io.powroseba.commons.DomainId;

public abstract class ReorderModel<I extends DomainId<?>> {

    public final I id;
    public final int destinationOrder;

    protected ReorderModel(I id, int destinationOrder) {
        if (destinationOrder < 0) {
            throw new IllegalArgumentException("Destination order cannot be less than 0");
        }
        this.id = id;
        this.destinationOrder = destinationOrder;
    }
}
