package io.powroseba.page.order;

import io.powroseba.commons.DomainId;

interface Shifter<I extends DomainId<?>, E extends AbstractOrderEntry<I, E>> {
    E shift(E pageOrderEntry);

    static <I extends DomainId<?>, E extends AbstractOrderEntry<I, E>> Shifter<I, E> matchShifter(E entry, Integer newOrder) {
        return entry.getOrder() < newOrder ? new RightShifter<>(entry, newOrder) : new LeftShifter<>(entry, newOrder);
    }
}
