package io.powroseba.page;

import io.powroseba.page.order.ReorderModel;
import io.powroseba.page.structs.PageId;

public final class PageReorder extends ReorderModel<PageId> {

    public PageReorder(PageId id, int destinationOrder) {
        super(id, destinationOrder);
    }
}
