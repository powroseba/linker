package io.powroseba.page.container;

import io.powroseba.commons.DomainId;
import io.powroseba.page.structs.PageId;

import java.util.UUID;

public final class ContainerId extends DomainId<UUID> {

    public final PageId pageId;

    public ContainerId(PageId pageId, UUID value) {
        super(value);
        this.pageId = pageId;
    }
}
