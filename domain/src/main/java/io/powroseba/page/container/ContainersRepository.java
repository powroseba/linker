package io.powroseba.page.container;

import io.powroseba.page.structs.PageId;

import java.util.Optional;

public interface ContainersRepository {
    Optional<Containers> findByPageId(PageId id);

    void save(Containers containers);
}
