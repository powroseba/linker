package io.powroseba.page.container;

import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;

import java.util.Set;

class ContainerOrder extends AbstractOrder<ContainerId, ContainerOrder.ContainerOrderEntry, ContainerOrder> {

    ContainerOrder(Set<ContainerOrderEntry> entries) {
        super(entries);
    }

    @Override
    protected ContainerOrderEntry entry(int order, ContainerId id) {
        return new ContainerOrderEntry(order, id);
    }

    @Override
    protected ContainerOrder order(Set<ContainerOrderEntry> entries) {
        return new ContainerOrder(entries);
    }

    Integer getOrderOfContainer(ContainerId containerId) {
        return getOrderOfEntry(containerId);
    }

    static class ContainerOrderEntry extends AbstractOrderEntry<ContainerId, ContainerOrderEntry> {

        ContainerOrderEntry(int order, ContainerId containerId) {
            super(order, containerId);
        }

        @Override
        protected ContainerOrderEntry of(int order, ContainerId id) {
            return new ContainerOrderEntry(order, id);
        }

    }
}
