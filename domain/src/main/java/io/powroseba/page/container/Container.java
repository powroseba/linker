package io.powroseba.page.container;

import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.link.Link;
import io.powroseba.page.link.LinkId;
import io.powroseba.page.link.LinkReorder;
import io.powroseba.page.link.Links;
import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.PageId;
import io.powroseba.page.structs.WebReference;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Container {

    public final ContainerId containerId;
    final DisplayText displayText;
    final int displayOrder;
    private final Links links;

    Container(ContainerId containerId, DisplayText displayText, int displayOrder, Links links) {
        this.containerId = containerId;
        this.displayText = displayText;
        this.displayOrder = displayOrder;
        this.links = links;
    }

    Container(ContainerId containerId, DisplayText displayText, int displayOrder) {
        this(containerId, displayText, displayOrder, Links.empty(containerId));
    }

    Container(Container container, int displayOrder) {
        this(container.containerId, container.displayText, displayOrder, container.links);
    }

    private Container(Container container, Links links) {
        this(container.containerId, container.displayText, container.displayOrder, links);
    }

    public static Container load(PageId pageId, PagesExportModel.Container container) {
        final var containerId = new ContainerId(pageId, container.containerId);
        return new Container(
                containerId,
                new DisplayText(container.displayText),
                container.displayOrder,
                Links.load(containerId, container.links)
        );
    }

    public Stream<Link> links() {
        return links.stream();
    }

    public AbstractOrder<LinkId, ? extends AbstractOrderEntry<?, ?>, ? extends AbstractOrder<?, ?, ?>> getLinksOrder() {
        return links.getOrder();
    }

    public PagesExportModel.Container createExportModel() {
        return new PagesExportModel.Container(
                containerId.value,
                displayText.content,
                displayOrder,
                links().map(Link::createExportModel).collect(Collectors.toSet())
        );
    }

    Container addLink(DisplayText displayText, WebReference webReference) {
        return new Container(this, links.add(displayText, webReference));
    }

    Container addLink(Link link) {
        return new Container(this, links.add(link));
    }

    Container modifyDisplayText(DisplayText displayText) {
        return new Container(this.containerId, displayText, this.displayOrder, this.links);
    }

    Container removeLink(LinkId linkId) {
        return new Container(this, links.remove(linkId));
    }
}
