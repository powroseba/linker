package io.powroseba.page.container;

import io.powroseba.page.order.ReorderModel;

public final class ContainerReorder extends ReorderModel<ContainerId> {

    public ContainerReorder(ContainerId id, int destinationOrder) {
        super(id, destinationOrder);
    }


}
