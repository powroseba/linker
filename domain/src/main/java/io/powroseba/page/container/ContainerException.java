package io.powroseba.page.container;

import io.powroseba.commons.DomainException;

public class ContainerException {

    public static class ContainerNotFound extends DomainException {

        public ContainerNotFound(ContainerId containerId) {
            super("Container " + containerId.value + " not found");
        }
    }
}
