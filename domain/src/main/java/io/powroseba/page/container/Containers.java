package io.powroseba.page.container;

import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.link.Link;
import io.powroseba.page.link.LinkException;
import io.powroseba.page.link.Links;
import io.powroseba.page.structs.LinkMove;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.ModifyContainer;
import io.powroseba.page.structs.PageId;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Containers {

    public final PageId pageId;
    private final ContainerOrder order;
    private final Set<Container> containers;

    public Containers(PageId pageId, ContainerOrder order, Set<Container> containers) {
        this.pageId = pageId;
        this.order = order;
        this.containers = containers;
    }

    Containers(PageId pageId) {
        this(pageId, new ContainerOrder(new HashSet<>()), new HashSet<>());
    }

    public static Containers empty(PageId pageId) {
        return new Containers(pageId);
    }

    public static Containers load(final PageId pageId, final Set<PagesExportModel.Container> containers) {
        final Function<PagesExportModel.Container, ContainerOrder.ContainerOrderEntry> orderCreator =
                container -> new ContainerOrder.ContainerOrderEntry(container.displayOrder, new ContainerId(pageId, container.containerId));
        return new Containers(
                pageId,
                new ContainerOrder(
                        containers.stream()
                                .map(orderCreator)
                                .collect(Collectors.toSet())
                ),
                containers
                        .stream()
                        .map(container -> Container.load(pageId, container))
                        .collect(Collectors.toSet())
        );
    }

    public Containers add(DisplayText displayText) {
        final var id = new ContainerId(this.pageId, UUID.randomUUID());
        final var newOrder = this.order.add(id);
        final var containers = new HashSet<>(this.containers);
        containers.add(new Container(id, displayText, newOrder.getOrderOfContainer(id)));
        return new Containers(this.pageId, newOrder, containers);
    }

    public Containers reorder(ContainerReorder reorderModel) {
        if (order.getOrderOfContainer(reorderModel.id) == null) {
            throw new ContainerException.ContainerNotFound(reorderModel.id);
        }
        final var newOrder = order.swapOrder(reorderModel);
        final var containers = this.containers.stream()
                .map(container -> new Container(container, newOrder.getOrderOfContainer(container.containerId)))
                .collect(Collectors.toSet());
        return new Containers(this.pageId, newOrder, new HashSet<>(containers));
    }

    public Containers remove(ContainerId containerId) {
        if (isContainerDoesNotExist(containerId)) {
            throw new ContainerException.ContainerNotFound(containerId);
        }
        final var newContainers = new HashSet<>(this.containers);
        newContainers.removeIf(container -> container.containerId.equals(containerId));
        final var newOrder = order.removeOrder(containerId);
        return new Containers(this.pageId, newOrder, newContainers);
    }

    public Containers modifyContainer(ModifyContainer modifyContainer) {
        final var containerToModify = containerWithId(modifyContainer.containerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(modifyContainer.containerId));
        final var containersWithoutModified = containersWithout(modifyContainer.containerId);
        containersWithoutModified.add(containerToModify.modifyDisplayText(modifyContainer.containerName));
        return new Containers(this.pageId, getOrder(), containersWithoutModified);
    }

    public Stream<Container> stream() {
        return containers.stream();
    }

    public ContainerOrder getOrder() {
        return order;
    }

    public Containers moveLinkBetweenContainers(LinkMove linkMove) {
        final var sourceContainer = containerWithId(linkMove.sourceContainerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(linkMove.sourceContainerId));
        final var destinationContainer = containerWithId(linkMove.destinationContainerId)
                .orElseThrow(() -> new ContainerException.ContainerNotFound(linkMove.destinationContainerId));
        final var linkToMove = sourceContainer.links().filter(link -> link.linkId.equals(linkMove.linkId))
                .findFirst().orElseThrow(() -> new LinkException.LinkNotFound(linkMove.linkId));
        final var containersWithoutModifying = containersWithout(
                linkMove.sourceContainerId, linkMove.destinationContainerId
        );
        containersWithoutModifying.add(destinationContainer.addLink(new Link(linkToMove, linkMove.destinationOrder)));
        containersWithoutModifying.add(sourceContainer.removeLink(linkMove.linkId));
        return new Containers(this.pageId, getOrder(), containersWithoutModifying);
    }

    private boolean isContainerDoesNotExist(ContainerId containerId) {
        return containers.stream().noneMatch(container -> container.containerId.equals(containerId));
    }

    private Set<Container> containersWithout(ContainerId... containerIds) {
        return stream()
                .filter(container -> !Set.of(containerIds).contains(container.containerId))
                .collect(Collectors.toSet());
    }

    private Optional<Container> containerWithId(ContainerId containerId) {
        return stream()
                .filter(container -> container.containerId.equals(containerId))
                .findFirst();
    }
}
