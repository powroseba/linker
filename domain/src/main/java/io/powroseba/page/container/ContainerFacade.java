package io.powroseba.page.container;

import io.powroseba.page.PageException;
import io.powroseba.page.structs.ModifyContainer;
import io.powroseba.page.structs.NewContainer;

public class ContainerFacade {

    private final ContainersRepository containersRepository;

    public ContainerFacade(ContainersRepository containersRepository) {
        this.containersRepository = containersRepository;
    }

    public void addContainerToPage(NewContainer container) {
        final var containers = containersRepository.findByPageId(container.pageId)
                .orElseThrow(() -> new PageException.PageNotFound(container.pageId));
        containersRepository.save(containers.add(container.containerName));
    }

    public void reorder(ContainerReorder reorderModel) {
        final var containers = containersRepository.findByPageId(reorderModel.id.pageId)
                .orElseThrow(() -> new PageException.PageNotFound(reorderModel.id.pageId));

        containersRepository.save(containers.reorder(reorderModel));
    }

    public void removeContainerFromPage(ContainerId containerId) {
        final var containers = containersRepository.findByPageId(containerId.pageId)
                .orElseThrow(() -> new PageException.PageNotFound(containerId.pageId));
        containersRepository.save(containers.remove(containerId));
    }

    public void modifyContainer(ModifyContainer containerData) {
        final var containers = containersRepository.findByPageId(containerData.containerId.pageId)
                .orElseThrow(() -> new PageException.PageNotFound(containerData.containerId.pageId));
        containersRepository.save(containers.modifyContainer(containerData));
    }
}
