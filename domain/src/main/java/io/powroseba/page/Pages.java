package io.powroseba.page;

import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.export.PagesExporter;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.PageId;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Pages {

    // probably user identifier
    private final PageOrder order;
    private final Set<Page> pages;

    Pages(PageOrder order, Set<Page> pages) {
        this.order = order;
        this.pages = pages;
    }

    Pages() {
        this(new PageOrder(new HashSet<>()), new HashSet<>());
    }

    static Pages empty() {
        return new Pages();
    }

    public static Pages load(PagesExportModel pagesExportModel) {
        return new Pages(
                new PageOrder(
                        pagesExportModel.pages
                                .stream()
                                .map(page -> new PageOrder.PageOrderEntry(page.displayOrder, new PageId(page.pageId)))
                                .collect(Collectors.toSet())
                ),
                pagesExportModel
                        .pages
                        .stream()
                        .map(Page::load)
                        .collect(Collectors.toSet())
        );
    }

    Pages add(DisplayText displayText) {
        final var newPageId = new PageId(UUID.randomUUID());
        final var newOrder = order.add(newPageId);
        final var newPages = new HashSet<>(this.pages);
        newPages.add(new Page(newPageId, displayText, newOrder.getOrderOfPage(newPageId)));
        return new Pages(newOrder, newPages);
    }

    Pages reorder(PageReorder reorderModel) {
        if (order.getOrderOfPage(reorderModel.id) == null) {
            throw new PageException.PageNotFound(reorderModel.id);
        }
        final var newOrder = order.swapOrder(reorderModel);
        final var pages = this.pages.stream()
                .map(page -> new Page(page, newOrder.getOrderOfPage(page.pageId)))
                .collect(Collectors.toSet());
        return new Pages(newOrder, new HashSet<>(pages));
    }

    Pages remove(PageId pageId) {
        if (isPageDoesNotExist(pageId)) {
            throw new PageException.PageNotFound(pageId);
        }
        final var newPages = new HashSet<>(this.pages);
        newPages.removeIf(page -> page.pageId.equals(pageId));
        final var newOrder = order.removeOrder(pageId);
        return new Pages(newOrder, newPages);
    }

    PageOrder getOrder() {
        return order;
    }

    Stream<Page> stream() {
        return pages.stream();
    }

    <O> O export(PagesExporter<O> pagesExporter) {
        return pagesExporter.export(
                new PagesExportModel(
                        pages.stream()
                                .map(Page::createExportModel)
                                .collect(Collectors.toSet())
                )
        );
    }


    private boolean isPageDoesNotExist(PageId pageId) {
        return pages.stream().noneMatch(page -> page.pageId.equals(pageId));
    }
}