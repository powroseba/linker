package io.powroseba.page;

import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;
import io.powroseba.page.structs.PageId;

import java.util.Set;

class PageOrder extends AbstractOrder<PageId, PageOrder.PageOrderEntry, PageOrder> {

    PageOrder(Set<PageOrderEntry> entries) {
        super(entries);
    }

    @Override
    protected PageOrderEntry entry(int order, PageId id) {
        return new PageOrderEntry(order, id);
    }

    @Override
    protected PageOrder order(Set<PageOrderEntry> entries) {
        return new PageOrder(entries);
    }

    Integer getOrderOfPage(PageId pageId) {
        return getOrderOfEntry(pageId);
    }

    static class PageOrderEntry extends AbstractOrderEntry<PageId, PageOrderEntry> {

        PageOrderEntry(int order, PageId id) {
            super(order, id);
        }

        @Override
        protected PageOrderEntry of(int order, PageId id) {
            return new PageOrderEntry(order, id);
        }
    }
}
