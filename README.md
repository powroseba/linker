# Linker
___

Web application which simple bookmarks organisation functions.

### How to run

1. build a project with maven by command ```mvn clean package```
2. run application with proper yaml configuration file by command 
   <br>
   ```java -jar starter/target/linker-0.0.1.jar server starter/src/main/resources/config/linker.yml```
3. visit localhost:8080 on browser and organise your bookmarks

### Running without yaml file
1. To run application without passing yaml file just use below command
   <br>
   ```java -Ddw.server.rootPath=/api -jar starter/target/linker-0.0.1.jar server```
