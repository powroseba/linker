package io.powroseba.integration.hooks

import com.google.inject.TypeLiteral
import io.powroseba.config.PersistenceModule
import io.powroseba.integration.repository.Cleanable
import io.powroseba.page.PageRepository
import io.powroseba.page.PageRepositoryAdapter
import io.powroseba.page.PagesRepository
import io.powroseba.page.entity.PagesEntity
import io.powroseba.page.storage.PagesStorage
import io.powroseba.page.storage.Storage
import ru.vyarus.dropwizard.guice.GuiceBundle
import ru.vyarus.dropwizard.guice.hook.GuiceyConfigurationHook

final class CleanupRepositoryHook  implements GuiceyConfigurationHook {

    @Override
    void configure(GuiceBundle.Builder builder) {
        builder.modulesOverride(new IntegrationTestPersistenceModule())
    }
    
    static class IntegrationTestPersistenceModule extends PersistenceModule {
        
        @Override
        protected void configure() {
            bind(new TypeLiteral<Storage<PagesEntity>>() {}).to(IntegrationPagesStorageAdapter.class);
            bind(PageRepository.class).to(PageRepositoryAdapter.class)
            bind(PagesRepository.class).to(PageRepositoryAdapter.class)
        }
    }

    static class IntegrationPagesStorageAdapter extends PagesStorage implements Cleanable {

        @Override
        void clean() {
            pagesSource.clear()
        }
    }
}
