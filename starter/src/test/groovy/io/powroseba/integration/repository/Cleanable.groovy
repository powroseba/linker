package io.powroseba.integration.repository

interface Cleanable {

    void clean()
}
