package io.powroseba.integration.page.container

import com.google.inject.Inject
import groovy.transform.PackageScope
import io.powroseba.integration.commons.IntegrationTest
import io.powroseba.page.PagesRepository
import io.powroseba.page.container.ContainersRepository
import spock.lang.Shared

@PackageScope
abstract class ContainerIntegrationTest extends IntegrationTest {

    @Shared @Inject
    protected ContainersRepository containersRepository

    @Shared @Inject
    protected PagesRepository pagesRepository
}
