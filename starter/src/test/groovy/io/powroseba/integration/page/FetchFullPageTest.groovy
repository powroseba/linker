package io.powroseba.integration.page

import io.powroseba.page.PageRepository
import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.link.LinkFactory
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.FullPageDto
import spock.lang.Shared

import javax.inject.Inject

class FetchFullPageTest extends PageIntegrationTest {

    @Shared @Inject
    private PageRepository pageRepository

    def 'should return 404 status when trying to fetch page which does not exist'() {
        given:
            final def pageId = new PageId(UUID.randomUUID())

        when:
            def response = client.targetRest("pages/" + pageId.value).request().buildGet().invoke()

        then:
            response.status == 404
            pageRepository.findById(pageId).isEmpty()
            response.readEntity(String.class) == "Page ${pageId.value} not found"
            noExceptionThrown()
    }

    def 'should return page with containers and list'() {
        given:
            final def pageId = new PageId(UUID.randomUUID())
            addOnePage(pageId)

        when:
            def response = client.targetRest("pages/" + pageId.value).request().buildGet().invoke()

        then:
            response.status == 200
            with(response.readEntity(FullPageDto)) {
                id == pageId.value.toString()
                displayText == "page"
                displayOrder == 0
                !containers.isEmpty()
                with(containers.stream().findFirst().get()) {
                    id != null
                    displayOrder == 0
                    displayText == "container"
                    !links.isEmpty()
                    with(links.stream().findFirst().get()) {
                        linkId != null
                        displayText == "link"
                        webReference == "test.pl"
                        displayOrder == 0
                    }
                }
            }
            pageRepository.findById(pageId).isPresent()
            noExceptionThrown()

    }

    void addOnePage(PageId pageId) {
        final def pages = PagesFactory.nPagesOneFixed(1,
                new PagesFactory.FixedPage(pageId, 0, "page",
                        new ContainerFactory.FixedContainer(displayText : "container", fixedLink:
                                new LinkFactory.FixedLink(webReference: "www.test.pl", displayText: "link")
                        )
                )
        )
        pagesRepository.save(pages)
    }
}
