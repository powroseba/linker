package io.powroseba.integration.page

import com.google.inject.Inject
import groovy.transform.PackageScope
import io.powroseba.integration.commons.IntegrationTest
import io.powroseba.integration.repository.Cleanable
import io.powroseba.page.PagesRepository
import io.powroseba.page.entity.PagesEntity
import io.powroseba.page.storage.Storage
import spock.lang.Shared

@PackageScope
abstract class PageIntegrationTest extends IntegrationTest {

    @Shared @Inject
    protected PagesRepository pagesRepository

    @Shared @Inject
    protected Storage<PagesEntity> pagesStorage

    @Override
    protected cleanDatabase() {
        ((Cleanable) pagesStorage).clean()
    }
}
