package io.powroseba.integration.page.container


import io.powroseba.page.PagesFactory
import io.powroseba.page.container.Container
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.Containers
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.structs.PageId

class DeleteContainerTest extends ContainerIntegrationTest {

    def 'should return not found status due to fact that page does not exist'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Page ${containerId.pageId.value} not found"
            noExceptionThrown()
    }

    def 'should return not found status due to fact that container does not exist in page'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            addEmptyPage(containerId.pageId)

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${containerId.value} not found"
            noExceptionThrown()
    }

    def 'should remove container from page'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            addPageWithContainer(containerId)

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .delete()

        then:
            response.status == 200
            containersRepository.findByPageId(containerId.pageId)
                    .flatMap({ getContainer(it, containerId) })
                    .isEmpty()
            noExceptionThrown()

    }

    private void addPageWithContainer(ContainerId containerId) {
        cleanDatabase()
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private void addEmptyPage(PageId pageId) {
        cleanDatabase()
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(pageId, 0)))
    }

    private static Optional<Container> getContainer(Containers it, containerId) {
        it.stream().filter({ it.containerId == containerId }).findFirst()
    }
}
