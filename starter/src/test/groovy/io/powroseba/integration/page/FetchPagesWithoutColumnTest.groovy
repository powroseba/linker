package io.powroseba.integration.page

import io.powroseba.page.PagesFactory
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.PageWithoutColumnDto

import javax.ws.rs.core.GenericType

class FetchPagesWithoutColumnTest extends PageIntegrationTest {

    def 'should return no pages'() {
        when:
            def response = client.targetRest("pages").request().buildGet().invoke()

        then:
            response.status == 200
            response.readEntity(new GenericType<List<PageWithoutColumnDto>>(){}).size() == 0
            pagesRepository.findAllWithoutColumns().size() == 0
            noExceptionThrown()
    }

    def 'should return list with one page which was already created'() {
        given:
            addOnePage()

        when:
            def response = client.targetRest("pages").request().buildGet().invoke()

        then:
            response.status == 200
            response.readEntity(new GenericType<List<PageWithoutColumnDto>>(){}).size() == 1
            pagesRepository.findAllWithoutColumns().size() == 1
            noExceptionThrown()
    }

    void addOnePage() {
        pagesRepository.save(
                PagesFactory.nPagesOneFixed(
                        1, new PagesFactory.FixedPage(new PageId(UUID.randomUUID()), 0)
                )
        )
    }
}
