package io.powroseba.integration.page

import io.powroseba.page.PageDataFetcher
import io.powroseba.page.PagesFactory
import io.powroseba.rest.pages.dto.CreatePageDto

import javax.ws.rs.client.Entity

class AddPageTest extends PageIntegrationTest {

    private PageDataFetcher pageDataFetcher = new PageDataFetcher(pagesRepository)

    def 'should add new page on first place'() {
        given:
            final def pageName = "newPage"
            final def pageAmountBeforeCreation = pagesRepository.findAllWithoutColumns().size()
            final def pageOrderSizeBeforeCreation = pageDataFetcher.getPagesOrderSize()

        when:
            def response = client.targetRest("pages")
                    .request()
                    .post(Entity.json(new CreatePageDto(pageName)))

        then:
            final def pages = pagesRepository.findAllWithoutColumns()
            response.status == 201
            pages.size() - 1 == pageAmountBeforeCreation
            pageDataFetcher.getPagesOrderSize() - 1 == pageOrderSizeBeforeCreation
            with(pages.stream().findFirst().get()) {
                displayOrder == 0
                displayText == pageName
                pageDataFetcher.getOrderOfPage(pageId) == 0
            }
            noExceptionThrown()
    }

    def 'should add new page on the last place when some pages already exist'() {
        given:
            add10Pages()
            final def pageName = "newPage"
            final def pageAmountBeforeCreation = pagesRepository.findAllWithoutColumns().size()
            final def pageOrderSizeBeforeCreation = pageDataFetcher.getPagesOrderSize()

        when:
            def response = client.targetRest("pages")
                    .request()
                    .post(Entity.json(new CreatePageDto(pageName)))

        then:
            final def pages = pagesRepository.findAllWithoutColumns()
            response.status == 201
            pages.size() - 1 == pageAmountBeforeCreation
            pageDataFetcher.getPagesOrderSize() - 1 == pageOrderSizeBeforeCreation
            with(pages.stream().filter({
                entry -> (entry.displayText == pageName)
            }).findFirst().get()) {
                displayOrder == 10
                displayText == pageName
                pageDataFetcher.getOrderOfPage(pageId) == 10
            }
            noExceptionThrown()
    }

    private void add10Pages() {
        cleanDatabase()
        pagesRepository.save(PagesFactory.nPages(10))
    }

}
