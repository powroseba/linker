package io.powroseba.integration.page.link

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.link.LinkDataFetcher
import io.powroseba.page.link.LinksFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.CreateLinkDto

import javax.ws.rs.client.Entity

class AddLinkTest extends LinkIntegrationTest {

    private static final ContainerId containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())

    private LinkDataFetcher linkDataFetcher = new LinkDataFetcher(linksRepository)

    def 'should not add link when page does not exist'() {
        when:
            def response = client.targetRest(apiUrl(containerId))
                    .request()
                    .post(Entity.json(new CreateLinkDto("link", "www.address.pl")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${containerId.value} not found"
            noExceptionThrown()
    }

    def 'should add new link to container on first place'() {
        given:
            addEmptyContainer()
            final def linkData = new CreateLinkDto("displayText", "www.web.address")
            final def linksAmountBeforeCreation = linkDataFetcher.getAmountOfLinksInContainer(containerId)
            final def linksOrderSizeBeforeCreation = linkDataFetcher.getLinksOrderSize(containerId)

        when:
            def response = client.targetRest(apiUrl(containerId))
                    .request()
                    .post(Entity.json(linkData))

        then:
            final def links = linksRepository.findByContainerId(containerId)
            links.isPresent()
            response.status == 201
            links.get().stream().count() - 1 == linksAmountBeforeCreation
            linkDataFetcher.getLinksOrderSize(containerId) - 1 == linksOrderSizeBeforeCreation
            with(links.get().stream().findFirst().get()) {
                displayOrder == 0
                displayText == new DisplayText(linkData.displayText)
                linkDataFetcher.getOrderOfLink(linkId) == 0
            }
            noExceptionThrown()
    }

    def 'should add new link to container on the last place when some links already exist'() {
        given:
            addPageWith10Containers()
            final def linkData = new CreateLinkDto("displayText", "www.web.address")
            final def linksAmountBeforeCreation = linkDataFetcher.getAmountOfLinksInContainer(containerId)
            final def linksOrderSizeBeforeCreation = linkDataFetcher.getLinksOrderSize(containerId)

        when:
            def response = client.targetRest(apiUrl(containerId))
                    .request()
                    .post(Entity.json(linkData))

        then:
            final def links = linksRepository.findByContainerId(containerId)
            links.isPresent()
            response.status == 201
            links.get().stream().count() - 1 == linksAmountBeforeCreation
            linkDataFetcher.getLinksOrderSize(containerId) - 1 == linksOrderSizeBeforeCreation
            with(links.get().stream().filter({
                entry -> (entry.displayText == new DisplayText(linkData.displayText))
            }).findFirst().get()) {
                displayOrder == 10
                displayText == new DisplayText(linkData.displayText)
                linkDataFetcher.getOrderOfLink(linkId) == 10
            }
            noExceptionThrown()
    }

    private void addPageWith10Containers() {
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId,
                links: LinksFactory.nLinks(10)
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private void addEmptyContainer() {
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private static GString apiUrl(ContainerId containerId) {
        return "pages/${containerId.pageId.value}/containers/${containerId.value}/links"
    }
}
