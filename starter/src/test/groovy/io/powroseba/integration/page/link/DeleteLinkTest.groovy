package io.powroseba.integration.page.link

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.link.*
import io.powroseba.page.structs.PageId

class DeleteLinkTest extends LinkIntegrationTest {

    private static final LinkId linkId = new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())

    def 'should return not found status due to fact that container does not exist in page'() {
        given:
            addEmptyPage(linkId.containerId.pageId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${linkId.containerId.value} not found"
            noExceptionThrown()
    }

    def 'should return not found status due to fact that link does not exist in container'() {
        given:
            addPageWithContainer(linkId.containerId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Link ${linkId.value} not found"
            noExceptionThrown()
    }

    def 'should return not found status due to fact that link does not exist'() {
        given:
            addPageWithContainerAndLink(new LinkId(linkId.containerId, UUID.randomUUID()))

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Link ${linkId.value} not found"
            noExceptionThrown()
    }

    def 'should remove link from container'() {
        given:
            addPageWithContainerAndLink(linkId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .delete()

        then:
            response.status == 200
            linksRepository.findByContainerId(linkId.containerId)
                    .flatMap({ getLink(it, linkId) })
                    .isEmpty()
            noExceptionThrown()

    }

    private void addEmptyPage(PageId pageId) {
        cleanDatabase()
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(pageId, 0)))
    }

    private void addPageWithContainer(ContainerId containerId) {
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private void addPageWithContainerAndLink(LinkId linkId) {
        final def links = LinksFactory.nLinksOneFixed(10, new LinkFactory.FixedLink(containerId: linkId.containerId, linkId: linkId))
        final def containers = ContainersFactory.nContainersSomeFixed(1,
                new ContainerFactory.FixedContainer(
                        pageId: linkId.containerId.pageId,
                        containerId: linkId.containerId,
                        links: links
                )
        )
        pagesRepository.save(PagesFactory.nPagesOneFixed(10,
                new PagesFactory.FixedPage(linkId.containerId.pageId, 0, containers)
        )
        )
    }

    private static Optional<Link> getLink(Links it, LinkId linkId) {
        return it.stream().filter({ it.linkId == linkId }).findFirst()
    }

    private static GString apiUrl(LinkId linkId) {
        return "pages/${linkId.containerId.pageId.value}/containers/${linkId.containerId.value}/links/${linkId.value}"
    }
}
