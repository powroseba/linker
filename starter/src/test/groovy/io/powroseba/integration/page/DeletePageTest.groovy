package io.powroseba.integration.page

import io.powroseba.page.PageRepository
import io.powroseba.page.PagesFactory
import io.powroseba.page.structs.PageId
import spock.lang.Shared

import javax.inject.Inject

class DeletePageTest extends PageIntegrationTest {

    @Shared @Inject
    private PageRepository pageRepository

    def 'should return not found status due to fact that page does not exist'() {
        given:
            final def pageId = UUID.randomUUID()

        when:
            def response = client.targetRest("pages/" + pageId)
                    .request()
                    .delete()

        then:
            response.status == 404
            response.readEntity(String.class) == "Page ${pageId} not found"
            noExceptionThrown()
    }

    def 'should remove one page'() {
        given:
            final def pageId = UUID.randomUUID()
            addOnePage(pageId)

        when:
            def response = client.targetRest("pages/" + pageId)
                    .request()
                    .delete()

        then:
            response.status == 200
            pageRepository.findById(new PageId(pageId)).isEmpty()
            noExceptionThrown()

    }

    void addOnePage(UUID pageId) {
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(new PageId(pageId), 0)))
    }
}
