package io.powroseba.integration.page.container

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.*
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.EditContainerDto

import javax.ws.rs.client.Entity

class EditContainerNameTest extends ContainerIntegrationTest {

    def 'should return not found status due to fact that page does not exist'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .put(Entity.json(new EditContainerDto("displayText")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Page ${containerId.pageId.value} not found"
            noExceptionThrown()
    }

    def 'should return not found status due to fact that container does not exist in page'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            addEmptyPage(containerId.pageId)

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .put(Entity.json(new EditContainerDto("displayText")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${containerId.value} not found"
            noExceptionThrown()
    }

    def 'should edit container display text'() {
        given:
            final var containerId = new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID())
            final var newContainerText = "UpdatedText"
            addPageWithContainer(containerId, "ContainerText")
            final var containerBeforeModification = getModifiedContainer(containerId)

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}")
                    .request()
                    .put(Entity.json(new EditContainerDto(newContainerText)))

        then:
            response.status == 200
            containerBeforeModification.isPresent()
            with (getModifiedContainer(containerId)) {
                isPresent()
                get().displayText == new DisplayText(newContainerText)
                get().displayText != containerBeforeModification.get().displayText
            }
            noExceptionThrown()

    }

    private void addPageWithContainer(ContainerId containerId, String containerText) {
        cleanDatabase()
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId,
                displayText: containerText
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private void addEmptyPage(PageId pageId) {
        cleanDatabase()
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(pageId, 0)))
    }

    private Optional<Container> getModifiedContainer(ContainerId containerId) {
        return containersRepository.findByPageId(containerId.pageId)
                .flatMap({ getContainer(it, containerId) })
    }

    private static Optional<Container> getContainer(Containers it, containerId) {
        it.stream().filter({ it.containerId == containerId }).findFirst()
    }
}
