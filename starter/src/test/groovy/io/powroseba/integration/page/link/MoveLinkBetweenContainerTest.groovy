package io.powroseba.integration.page.link

import com.google.inject.Inject
import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersRepository
import io.powroseba.page.link.LinkDataFetcher
import io.powroseba.page.link.LinkFactory
import io.powroseba.page.link.LinkId
import io.powroseba.page.order.AbstractOrderEntry
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.MoveLinkDto
import spock.lang.Shared

import javax.ws.rs.client.Entity

import static io.powroseba.page.container.ContainersFactory.nContainersSomeFixed
import static io.powroseba.page.link.LinksFactory.nLinks
import static io.powroseba.page.link.LinksFactory.nLinksOneFixed

class MoveLinkBetweenContainerTest extends LinkIntegrationTest {

    private static final PageId randomPageId = new PageId(UUID.randomUUID())

    @Shared
    @Inject
    protected ContainersRepository containersRepository

    private LinkDataFetcher linkDataFetcher = new LinkDataFetcher(linksRepository)

    def 'should not move link if link does not exist in source container'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkWhichDoesNotExistIdentifier = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(
                    sourceContainerId, destinationContainerId, new LinkId(sourceContainerId, UUID.randomUUID())
            )

        when:
            def response = client.targetRest(apiUrl(linkWhichDoesNotExistIdentifier))
                    .request()
                    .buildPut(Entity.json(new MoveLinkDto(destinationContainerId.value, destinationOrder)))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()
    }

    def 'should not move link if page does not exist'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkIdWithPageIdWhichDoesNotExist = new LinkId(
                    new ContainerId(new PageId(UUID.randomUUID()), sourceContainerId.value), UUID.randomUUID()
            )
            final def destinationOrder = 4
            addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(
                    sourceContainerId, destinationContainerId, new LinkId(sourceContainerId, UUID.randomUUID())
            )

        when:
            def response = client.targetRest(apiUrl(linkIdWithPageIdWhichDoesNotExist))
                    .request()
                    .buildPut(Entity.json(new MoveLinkDto(destinationContainerId.value, destinationOrder)))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()
    }

    def 'should not move link if source container does not exist'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkIdWithContainerIdWhichDoesNotExist = new LinkId(
                    new ContainerId(randomPageId, UUID.randomUUID()), UUID.randomUUID()
            )
            final def destinationOrder = 4
            addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(
                    sourceContainerId, destinationContainerId, new LinkId(sourceContainerId, UUID.randomUUID())
            )

        when:
            def response = client.targetRest(apiUrl(linkIdWithContainerIdWhichDoesNotExist))
                    .request()
                    .buildPut(Entity.json(new MoveLinkDto(destinationContainerId.value, destinationOrder)))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()
    }

    def 'should not move link if destination container does not exist'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 4
            addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(
                    sourceContainerId, destinationContainerId, new LinkId(sourceContainerId, UUID.randomUUID())
            )

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .buildPut(Entity.json(new MoveLinkDto(UUID.randomUUID(), destinationOrder)))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()
    }

    def 'should move link from one container to another on indicated display order'() {
        given:
            final def sourceContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def destinationContainerId = new ContainerId(randomPageId, UUID.randomUUID())
            final def linkId = new LinkId(sourceContainerId, UUID.randomUUID())
            final def destinationOrder = 0
            addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(
                    sourceContainerId, destinationContainerId, linkId
            )
            final def previousSourceContainerLinkOrder = linkDataFetcher.getSortedLinksOrder(sourceContainerId)
            final def previousDestinationContainerLinkOrder = linkDataFetcher.getSortedLinksOrder(destinationContainerId)
            final def previousSourceContainerLinksIdentifiers = linkDataFetcher.containerLinksIdentifiers(sourceContainerId)
            final def previousDestinationContainerLinksIdentifiers = linkDataFetcher.containerLinksIdentifiers(destinationContainerId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .buildPut(Entity.json(new MoveLinkDto(destinationContainerId.value, destinationOrder)))
                    .invoke()

        then:
            response.status == 200
            final def currentSourceContainerLinkOrder = linkDataFetcher.getSortedLinksOrder(sourceContainerId)
            final def currentDestinationContainerLinkOrder = linkDataFetcher.getSortedLinksOrder(destinationContainerId)
            final def currentSourceContainerLinksIdentifiers = linkDataFetcher.containerLinksIdentifiers(sourceContainerId)
            final def currentDestinationContainerLinksIdentifiers = linkDataFetcher.containerLinksIdentifiers(destinationContainerId)
            checkAmountOfLinksInSourceAndDestinationBeforeAndAfter(
                    previousSourceContainerLinkOrder,
                    currentSourceContainerLinkOrder,
                    previousDestinationContainerLinkOrder,
                    currentDestinationContainerLinkOrder,
                    previousSourceContainerLinksIdentifiers,
                    currentSourceContainerLinksIdentifiers,
                    previousDestinationContainerLinksIdentifiers,
                    currentDestinationContainerLinksIdentifiers
            )
            checkLinkExistenceInSourceAndDestinationContainerBeforeAndAfter(
                    linkId,
                    previousSourceContainerLinksIdentifiers,
                    currentSourceContainerLinksIdentifiers,
                    previousDestinationContainerLinksIdentifiers,
                    currentDestinationContainerLinksIdentifiers
            )
            checkOrderOfLinksInSourceContainer(previousSourceContainerLinkOrder, currentSourceContainerLinkOrder)
            checkOrderOfLinksInDestinationContainer(
                    linkId,
                    previousDestinationContainerLinkOrder,
                    currentDestinationContainerLinkOrder,
                    destinationOrder
            )
            noExceptionThrown()
    }

    private static GString apiUrl(LinkId linkId) {
        "pages/${linkId.containerId.pageId.value}/containers/${linkId.containerId.value}/links/${linkId.value}/position"
    }

    private static void checkLinkExistenceInSourceAndDestinationContainerBeforeAndAfter(linkId,
                                                                                        Set<UUID> previousSourceContainerLinksIdentifiers,
                                                                                        Set<UUID> currentSourceContainerLinksIdentifiers,
                                                                                        Set<UUID> previousDestinationContainerLinksIdentifiers,
                                                                                        Set<UUID> currentDestinationContainerLinksIdentifiers) {
        assert previousSourceContainerLinksIdentifiers.stream().anyMatch({ id -> id == linkId.value })
        assert currentSourceContainerLinksIdentifiers.stream().noneMatch({ id -> id == linkId.value })
        assert previousDestinationContainerLinksIdentifiers.stream().noneMatch({ id -> id == linkId.value })
        assert currentDestinationContainerLinksIdentifiers.stream().anyMatch({ id -> id == linkId.value })
    }

    private static void checkAmountOfLinksInSourceAndDestinationBeforeAndAfter(List<AbstractOrderEntry<LinkId, ?>> previousSourceContainerLinkOrder,
                                                                               List<AbstractOrderEntry<LinkId, ?>> currentSourceContainerLinkOrder,
                                                                               List<AbstractOrderEntry<LinkId, ?>> previousDestinationContainerLinkOrder,
                                                                               List<AbstractOrderEntry<LinkId, ?>> currentDestinationContainerLinkOrder,
                                                                               Set<UUID> previousSourceContainerLinksIdentifiers,
                                                                               Set<UUID> currentSourceContainerLinksIdentifiers,
                                                                               Set<UUID> previousDestinationContainerLinksIdentifiers,
                                                                               Set<UUID> currentDestinationContainerLinksIdentifiers) {
        assert previousSourceContainerLinkOrder.size() - 1 == currentSourceContainerLinkOrder.size()
        assert previousDestinationContainerLinkOrder.size() + 1 == currentDestinationContainerLinkOrder.size()
        assert previousSourceContainerLinksIdentifiers.size() - 1 == currentSourceContainerLinksIdentifiers.size()
        assert previousDestinationContainerLinksIdentifiers.size() + 1 == currentDestinationContainerLinksIdentifiers.size()
    }

    private static void checkOrderOfLinksInSourceContainer(List<AbstractOrderEntry<LinkId, ?>> previousSourceContainerLinkOrder,
                                                           List<AbstractOrderEntry<LinkId, ?>> currentSourceContainerLinkOrder) {
        for (int i = 0; i < previousSourceContainerLinkOrder.size() - 1; i++) {
            final def currentEntry = currentSourceContainerLinkOrder.get(i)
            final def previousEntry = previousSourceContainerLinkOrder.get(i + 1)
            assert previousEntry.getId() == currentEntry.getId()
            assert previousEntry.getOrder() == currentEntry.getOrder() + 1
        }
    }

    private static void checkOrderOfLinksInDestinationContainer(LinkId linkId,
                                                                List<AbstractOrderEntry<LinkId, ?>> previousDestinationContainerLinkOrder,
                                                                List<AbstractOrderEntry<LinkId, ?>> currentDestinationContainerLinkOrder,
                                                                int destinationOrder) {
        for (int i = 0; i < previousDestinationContainerLinkOrder.size() + 1; i++) {
            final def currentEntry = currentDestinationContainerLinkOrder.get(i)
            if (i < destinationOrder) {
                final def previousEntry = previousDestinationContainerLinkOrder.get(i)
                assert previousEntry.getId() == currentEntry.getId()
                assert previousEntry.getOrder() == currentEntry.getOrder()
            } else if (i == destinationOrder) {
                currentEntry.getId() == linkId
                currentEntry.getOrder() == destinationOrder
            } else {
                final def previousEntry = previousDestinationContainerLinkOrder.get(i - 1)
                assert previousEntry.getId() == currentEntry.getId()
                assert previousEntry.getOrder() == currentEntry.getOrder() - 1
            }
        }
    }

    private void addTwoContainersWith10LinksOneFixedInFirstContainerOnFirstPlace(ContainerId firstContainerId,
                                                                                 ContainerId secondContainerId,
                                                                                 LinkId linkId) {
        final def containers = nContainersSomeFixed(2,
                new ContainerFactory.FixedContainer(
                        firstContainerId,
                        0,
                        nLinksOneFixed(10,
                                new LinkFactory.FixedLink(containerId: firstContainerId, linkId: linkId, order: 0)
                        )
                ),
                new ContainerFactory.FixedContainer(secondContainerId, 1, nLinks(10)),
        )
        final def pages = PagesFactory.nPagesOneFixed(
                1,
                new PagesFactory.FixedPage(containers.pageId, 0, containers)
        )
        pagesRepository.save(pages)
    }
}
