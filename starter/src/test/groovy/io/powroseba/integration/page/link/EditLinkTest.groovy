package io.powroseba.integration.page.link

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.link.Link
import io.powroseba.page.link.LinkFactory
import io.powroseba.page.link.LinkId
import io.powroseba.page.link.Links
import io.powroseba.page.link.LinksFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import io.powroseba.page.structs.WebReference
import io.powroseba.rest.pages.dto.EditLinkDto

import javax.ws.rs.client.Entity

class EditLinkTest extends LinkIntegrationTest {

    def 'should return not found status due to fact that page does not exist'() {
        given:
            final def linkId = new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .put(Entity.json(new EditLinkDto("displayText", "www.address.web", "www.image.jpg")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${linkId.containerId.value} not found"
            noExceptionThrown()
    }


    def 'should return not found status due to fact that container does not exist in page'() {
        given:
            final def linkId = new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())
            addEmptyPage(linkId.containerId.pageId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .put(Entity.json(new EditLinkDto("displayText", "www.address.web", "www.newImage.jpg")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Container ${linkId.containerId.value} not found"
            noExceptionThrown()
    }

    def 'should return not found status due to fact that link does not exist in container'() {
        given:
            final def linkId = new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())
            addPageWithEmptyContainer(linkId.containerId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .put(Entity.json(new EditLinkDto("displayText", "www.address.web", "www.newImage.jpg")))

        then:
            response.status == 404
            response.readEntity(String.class) == "Link ${linkId.value} not found"
            noExceptionThrown()
    }

    def 'should edit link data'() {
        given:
            final def linkId = new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID())
            final var newLinkText = "newDisplayText"
            final var newWebReference = "www.newWeb.reference"
            final var newImageAddress = "www.newImage.jpg"
            addPageWithContainer(linkId, "ContainerText", "www.web.reference")
            final var linkBeforeModification = getModifiedLink(linkId)

        when:
            def response = client.targetRest(apiUrl(linkId))
                    .request()
                    .put(Entity.json(new EditLinkDto(newLinkText, newWebReference, newImageAddress)))

        then:
            response.status == 200
            linkBeforeModification.isPresent()
            with (getModifiedLink(linkId)) {
                isPresent()
                get().displayText == new DisplayText(newLinkText)
                get().displayText != linkBeforeModification.get().displayText
                get().webReference == new WebReference(newWebReference)
                get().webReference != linkBeforeModification.get().webReference
                get().image != linkBeforeModification.get().image
            }
            noExceptionThrown()

    }

    private void addEmptyPage(PageId pageId) {
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(pageId, 0)))
    }

    private void addPageWithEmptyContainer(ContainerId containerId) {
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: containerId.pageId,
                containerId: containerId
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(containerId.pageId, 0, containers)))
    }

    private Optional<Link> getModifiedLink(LinkId linkId) {
        return linksRepository.findByContainerId(linkId.containerId)
                .flatMap({ getLink(it, linkId) })
    }

    private static Optional<Link> getLink(Links it, LinkId linkId) {
        it.stream().filter({ it.linkId == linkId }).findFirst()
    }

    private void addPageWithContainer(LinkId linkId, String displayText, String webReference) {
        final def links = LinksFactory.nLinksOneFixed(1, new LinkFactory.FixedLink(
                linkId.containerId, linkId, 0, displayText, webReference
        ))
        final def containers = ContainersFactory.nContainersSomeFixed(1, new ContainerFactory.FixedContainer(
                pageId: linkId.containerId.pageId,
                containerId: linkId.containerId,
                links: links
        ))
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(linkId.containerId.pageId, 0, containers)))
    }

    private static GString apiUrl(LinkId linkId) {
        return "pages/${linkId.containerId.pageId.value}/containers/${linkId.containerId.value}/links/${linkId.value}"
    }
}
