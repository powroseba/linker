package io.powroseba.integration.page.link

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerFactory
import io.powroseba.page.container.ContainerId
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.link.LinkDataFetcher
import io.powroseba.page.link.LinkFactory
import io.powroseba.page.link.LinkId
import io.powroseba.page.link.LinksFactory
import io.powroseba.page.structs.PageId

import javax.ws.rs.client.Entity

class ReorderLinkTest extends LinkIntegrationTest {

    private static final LinkId linkToMove = new LinkId(
            new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID()
    )

    private LinkDataFetcher linkDataFetcher = new LinkDataFetcher(linksRepository)

    def 'should not reorder links from container which does not exist'() {
        given:
            addContainerWith4Links()
            final def destinationIndex = 1

        when:
            def response = client.targetRest(apiUrl(linkId, destinationIndex))
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()

        where:
            linkId << [
                    new LinkId(new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()), UUID.randomUUID()),
                    new LinkId(new ContainerId(linkToMove.containerId.pageId, UUID.randomUUID()), UUID.randomUUID()),
                    new LinkId(linkToMove.containerId, UUID.randomUUID())
            ]
    }

    def 'should move last link to first place and all others links should have increased order'() {
        given:
            addContainerWith4Links()
            final def destinationIndex = 0
            final def previousOrder = linkDataFetcher.getSortedLinksOrder(linkToMove.containerId)

        when:
            def response = client.targetRest(apiUrl(linkToMove, destinationIndex))
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 200
            final def currentOrder = linkDataFetcher.getSortedLinksOrder(linkToMove.containerId)
            for (int i = 0; i < previousOrder.size(); i++) {
                final def currentEntry = currentOrder.get(i)
                if (i == destinationIndex) {
                    final def previousEntry = previousOrder.get(i)
                    assert previousEntry != currentEntry &&
                            currentEntry.getId() == linkToMove &&
                            previousEntry.getId() != linkToMove
                }
                if (i > 0) {
                    final def previousEntry = previousOrder.get(i - 1)
                    assert previousEntry.getId() == currentEntry.getId()
                    assert previousEntry.getOrder() + 1 == currentEntry.getOrder()
                }
            }
            noExceptionThrown()
    }

    private static GString apiUrl(LinkId linkId, destinationIndex) {
        "pages/${linkId.containerId.pageId.value}/containers/${linkId.containerId.value}/links/${linkId.value}/order/${destinationIndex}"
    }

    void addContainerWith4Links() {
        final def links = LinksFactory.nLinksOneFixed(
                4, new LinkFactory.FixedLink(linkToMove.containerId, linkToMove, 3)
        )
        final def containers = ContainersFactory.nContainersSomeFixed(
                1, new ContainerFactory.FixedContainer(linkToMove.containerId, 0, links)
        )
        final def pages = PagesFactory.nPagesOneFixed(
                1,
                new PagesFactory.FixedPage(linkToMove.containerId.pageId, 0, containers)
        )
        pagesRepository.save(pages)
    }
}
