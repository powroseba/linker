package io.powroseba.integration.page.container

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.*
import io.powroseba.page.structs.PageId

import javax.ws.rs.client.Entity

class ReorderContainerTest extends ContainerIntegrationTest {

    private static final ContainerId containerToMove = new ContainerId(
            new PageId(UUID.randomUUID()), UUID.randomUUID()
    )

    private ContainerDataFetcher containerDataFetcher = new ContainerDataFetcher(containersRepository)

    def 'should not reorder container from page which does not exist'() {
        given:
            addPageWith4Containers()
            final def destinationIndex = 1

        when:
            def response = client.targetRest("pages/${containerId.pageId.value}/containers/${containerId.value}/order/${destinationIndex}")
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 404
            noExceptionThrown()

        where:
            containerId << [
                    new ContainerId(new PageId(UUID.randomUUID()), UUID.randomUUID()),
                    new ContainerId(containerToMove.pageId, UUID.randomUUID())
            ]
    }

    def 'should move last container to first place and all others containers should have increased order'() {
        given:
            addPageWith4Containers()
            final def destinationIndex = 0
            final def previousOrder = containerDataFetcher.getSortedContainersOrder(containerToMove.pageId)

        when:
            def response = client.targetRest("pages/${containerToMove.pageId.value}/containers/${containerToMove.value}/order/${destinationIndex}")
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 200
            final def currentOrder = containerDataFetcher.getSortedContainersOrder(containerToMove.pageId)
            for (int i = 0; i < previousOrder.size(); i++) {
                final def currentEntry = currentOrder.get(i)
                if (i == destinationIndex) {
                    final def previousEntry = previousOrder.get(i)
                    assert previousEntry != currentEntry &&
                            currentEntry.getId() == containerToMove &&
                            previousEntry.getId() != containerToMove
                }
                if (i > 0) {
                    final def previousEntry = previousOrder.get(i - 1)
                    assert previousEntry.getId() == currentEntry.getId()
                    assert previousEntry.getOrder() + 1 == currentEntry.getOrder()
                }
            }
            noExceptionThrown()
    }

    void addPageWith4Containers() {
        final def containers = ContainersFactory.nContainersSomeFixed(
                10, new ContainerFactory.FixedContainer(containerId : containerToMove, order : 9)
        )
        final def pages = PagesFactory.nPagesOneFixed(
                1,
                new PagesFactory.FixedPage(containerToMove.pageId, 0, containers)
        )
        pagesRepository.save(pages)
    }
}
