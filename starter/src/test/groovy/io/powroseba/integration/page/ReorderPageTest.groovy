package io.powroseba.integration.page

import io.powroseba.page.PageDataFetcher
import io.powroseba.page.PagesFactory
import io.powroseba.page.structs.PageId

import javax.ws.rs.client.Entity

class ReorderPageTest extends PageIntegrationTest {

    private static final PageId pageIdToMove = new PageId(UUID.randomUUID())

    private PageDataFetcher pageDataFetcher = new PageDataFetcher(pagesRepository)

    def 'should not reorder page which does not exist'() {
        given:
            final def pageId = UUID.randomUUID()
            final def newIndex = 1

        when:
            def response = client.targetRest("pages/" + pageId + "/order/" + newIndex)
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 404
            response.readEntity(String.class) == "Page ${pageId} not found"
            noExceptionThrown()
    }

    def 'should move last page to first place and all others pages should have increased order'() {
        given:
            add10Pages()
            final def newPageIndex = 0
            final def previousOrder = pageDataFetcher.getSortedPageOrder()

        when:
            def response = client.targetRest("pages/" + pageIdToMove.value + "/order/" + newPageIndex)
                    .request()
                    .buildPut(Entity.text(""))
                    .invoke()

        then:
            response.status == 200
            final def currentOrder = pageDataFetcher.getSortedPageOrder()
            for (int i = 0; i < previousOrder.size(); i++) {
                final def currentEntry = currentOrder.get(i)
                if (i == newPageIndex) {
                    final def previousEntry = previousOrder.get(i)
                    assert previousEntry != currentEntry &&
                            currentEntry.getId() == pageIdToMove &&
                            previousEntry.getId() != pageIdToMove
                }
                if (i > 0) {
                    final def previousEntry = previousOrder.get(i - 1)
                    assert previousEntry.getId() == currentEntry.getId()
                    assert previousEntry.getOrder() + 1 == currentEntry.getOrder()
                }
            }
            noExceptionThrown()
    }

    void add10Pages() {
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(pageIdToMove, 9)))
    }
}
