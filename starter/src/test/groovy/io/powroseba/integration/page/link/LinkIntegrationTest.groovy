package io.powroseba.integration.page.link

import com.google.inject.Inject
import groovy.transform.PackageScope
import io.powroseba.integration.commons.IntegrationTest
import io.powroseba.page.PagesRepository
import io.powroseba.page.link.LinksRepository
import spock.lang.Shared

@PackageScope
abstract class LinkIntegrationTest extends IntegrationTest {
    @Shared @Inject
    protected LinksRepository linksRepository

    @Shared @Inject
    protected PagesRepository pagesRepository
}
