package io.powroseba.integration.page.container

import io.powroseba.page.PagesFactory
import io.powroseba.page.container.ContainerDataFetcher
import io.powroseba.page.container.ContainersFactory
import io.powroseba.page.structs.DisplayText
import io.powroseba.page.structs.PageId
import io.powroseba.rest.pages.dto.CreateContainerDto

import javax.ws.rs.client.Entity

class AddContainerTest extends ContainerIntegrationTest {

    private static final PageId pageId = new PageId(UUID.randomUUID())

    private ContainerDataFetcher containerDataFetcher = new ContainerDataFetcher(containersRepository)

    def 'should not add container when page does not exist'() {
        given:
            final def pageId = UUID.randomUUID()
            final def containerName = "newContainer"

        when:
            def response = client.targetRest("pages/${pageId}/containers")
                    .request()
                    .post(Entity.json(new CreateContainerDto(containerName)))

        then:
            response.status == 404
            response.readEntity(String.class) == "Page ${pageId} not found"
            noExceptionThrown()
    }

    def 'should add new container to page on first place'() {
        given:
            addEmptyPage()
            final def containerName = "newContainer"
            final def containerAmountBeforeCreation = containerDataFetcher.getAmountOfContainersInPage(pageId)
            final def containerOrderSizeBeforeCreation = containerDataFetcher.getContainersOrderSize(pageId)

        when:
            def response = client.targetRest("pages/${pageId.value}/containers")
                    .request()
                    .post(Entity.json(new CreateContainerDto(containerName)))

        then:
            final def containers = containersRepository.findByPageId(pageId)
            containers.isPresent()
            response.status == 201
            containers.get().stream().count() - 1 == containerAmountBeforeCreation
            containerDataFetcher.getContainersOrderSize(pageId) - 1 == containerOrderSizeBeforeCreation
            with(containers.get().stream().findFirst().get()) {
                displayOrder == 0
                displayText == new DisplayText(containerName)
                containerDataFetcher.getOrderOfContainer(containerId) == 0
            }
            noExceptionThrown()
    }

    def 'should add new container to page on the last place when some containers already exist'() {
        given:
            addPageWith10Containers()
            final def containerName = "newContainer"
            final def containerAmountBeforeCreation = containerDataFetcher.getAmountOfContainersInPage(pageId)
            final def containerOrderSizeBeforeCreation = containerDataFetcher.getContainersOrderSize(pageId)

        when:
            def response = client.targetRest("pages/${pageId.value}/containers")
                    .request()
                    .post(Entity.json(new CreateContainerDto(containerName)))

        then:
            final def containers = containersRepository.findByPageId(pageId)
            containers.isPresent()
            response.status == 201
            containers.get().stream().count() - 1 == containerAmountBeforeCreation
            containerDataFetcher.getContainersOrderSize(pageId) - 1 == containerOrderSizeBeforeCreation
            with(containers.get().stream().filter({
                entry -> (entry.displayText == new DisplayText(containerName))
            }).findFirst().get()) {
                displayOrder == 10
                displayText == new DisplayText(containerName)
                containerDataFetcher.getOrderOfContainer(containerId) == 10
            }
            noExceptionThrown()
    }

    private void addPageWith10Containers() {
        cleanDatabase()
        final def containers = ContainersFactory.nContainers(10)
        pagesRepository.save(PagesFactory.nPagesOneFixed(10, new PagesFactory.FixedPage(pageId, 0, containers)))
    }

    private void addEmptyPage() {
        cleanDatabase()
        pagesRepository.save(PagesFactory.nPagesOneFixed(1, new PagesFactory.FixedPage(pageId, 0)))
    }

}
