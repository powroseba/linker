package io.powroseba.integration.commons

import com.google.inject.Inject
import io.powroseba.LinkerStarter
import io.powroseba.integration.hooks.CleanupRepositoryHook
import io.powroseba.integration.repository.Cleanable
import io.powroseba.page.entity.PagesEntity
import io.powroseba.page.storage.Storage
import ru.vyarus.dropwizard.guice.test.ClientSupport
import ru.vyarus.dropwizard.guice.test.spock.InjectClient
import ru.vyarus.dropwizard.guice.test.spock.UseDropwizardApp
import spock.lang.Shared
import spock.lang.Specification

@UseDropwizardApp(value = LinkerStarter, randomPorts = true, config = "src/test/resources/test-config.yml", hooks = [CleanupRepositoryHook])
abstract class IntegrationTest extends Specification {

    @Shared @InjectClient
    protected ClientSupport client

    @Shared @Inject
    protected Storage<PagesEntity> pageStorage

    def setupSpec() {
        cleanDatabase()
    }

    def cleanupSpec() {
        cleanDatabase()
    }

    protected cleanDatabase() {
        ((Cleanable) pageStorage).clean()
    }
}
