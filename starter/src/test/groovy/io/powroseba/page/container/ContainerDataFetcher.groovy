package io.powroseba.page.container

import io.powroseba.page.order.AbstractOrderEntry
import io.powroseba.page.structs.PageId

import java.util.stream.Collectors

class ContainerDataFetcher {

    private final ContainersRepository containersRepository

    ContainerDataFetcher(ContainersRepository containersRepository) {
        this.containersRepository = containersRepository
    }

    Integer getOrderOfContainer(ContainerId containerId) {
        return getContainersOrder(containerId.pageId).getOrderOfContainer(containerId)
    }

    Integer getContainersOrderSize(PageId pageId) {
        return getSortedContainersOrder(pageId)
                .stream()
                .count()
    }

    List<AbstractOrderEntry<ContainerId, ?>> getSortedContainersOrder(PageId pageId) {
        containersRepository.findByPageId(pageId)
                .map({ containers -> containers.order })
                .orElse(new ContainerOrder(new HashSet<ContainerOrder.ContainerOrderEntry>()))
                .stream()
                .sorted({ o1, o2 -> Integer.compare(o1.getOrder(), o2.getOrder()) })
                .collect(Collectors.toList())
    }

    Integer getAmountOfContainersInPage(PageId pageId) {
        return containersRepository.findByPageId(pageId)
                .map({it.stream().count() })
                .orElse(0)
    }

    private ContainerOrder getContainersOrder(PageId pageId) {
        containersRepository.findByPageId(pageId)
                .map({ containers -> containers.order })
                .orElse(new ContainerOrder(new HashSet<ContainerOrder.ContainerOrderEntry>()))
    }
}
