package io.powroseba.page.link

import io.powroseba.page.container.ContainerId
import io.powroseba.page.order.AbstractOrderEntry

import java.util.stream.Collectors

class LinkDataFetcher {

    private final LinksRepository linksRepository

    LinkDataFetcher(LinksRepository linksRepository) {
        this.linksRepository = linksRepository
    }

    List<AbstractOrderEntry<LinkId, ?>> getSortedLinksOrder(ContainerId containerId) {
        linksRepository.findByContainerId(containerId)
                .map({ links -> links.order })
                .orElse(new LinkOrder(new HashSet<LinkOrder.LinkOrderEntry>()))
                .stream()
                .sorted({ o1, o2 -> Integer.compare(o1.order, o2.order)})
                .collect(Collectors.toList())
    }

    Set<UUID> containerLinksIdentifiers(ContainerId containerId) {
        linksRepository.findByContainerId(containerId)
                .stream()
                .flatMap({ links -> links.stream() })
                .map({ link -> link.linkId })
                .map({ linkId -> linkId.value })
                .collect(Collectors.toSet())
    }

    Integer getAmountOfLinksInContainer(ContainerId containerId) {
        return linksRepository.findByContainerId(containerId)
                .map({ it.stream().count() })
                .orElse(0)
    }

    Integer getLinksOrderSize(ContainerId containerId) {
        return getSortedLinksOrder(containerId)
                .stream()
                .count()
    }

    Integer getOrderOfLink(LinkId linkId) {
        return getLinksOrder(linkId.containerId).getOrderOfLink(linkId)
    }

    private LinkOrder getLinksOrder(ContainerId containerId) {
        return linksRepository.findByContainerId(containerId)
                .map({ links -> links.order })
                .orElse(new LinkOrder(new HashSet<LinkOrder.LinkOrderEntry>()))
    }
}
