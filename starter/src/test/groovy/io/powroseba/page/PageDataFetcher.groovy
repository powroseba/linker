package io.powroseba.page

import io.powroseba.page.order.AbstractOrderEntry
import io.powroseba.page.structs.PageId

import java.util.stream.Collectors

final class PageDataFetcher {

    private final PagesRepository pagesRepository

    PageDataFetcher(PagesRepository pagesRepository) {
        this.pagesRepository = pagesRepository
    }

    Integer getPagesOrderSize() {
        return getPagesOrder()
                .stream()
                .count()
    }

    Integer getOrderOfPage(PageId pageId) {
        return getPagesOrder().getOrderOfPage(pageId)
    }

    List<AbstractOrderEntry<PageId, ?>> getSortedPageOrder() {
        pagesRepository.get()
                .map({ pages -> pages.order })
                .orElse(new PageOrder(new HashSet<PageOrder.PageOrderEntry>()))
                .stream()
                .sorted({ o1, o2 -> Integer.compare(o1.getOrder(), o2.getOrder()) })
                .collect(Collectors.toList())
    }

    private PageOrder getPagesOrder() {
        pagesRepository.get()
                .map({ pages -> pages.order })
                .orElse(new PageOrder(new HashSet<PageOrder.PageOrderEntry>()))
    }
}
