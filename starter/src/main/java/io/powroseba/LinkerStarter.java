package io.powroseba;

import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.powroseba.config.AppConfiguration;
import io.powroseba.config.ConfigModule;
import io.powroseba.config.PersistenceModule;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.guicey.spa.SpaBundle;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class LinkerStarter extends Application<AppConfiguration> {

    public static void main(String[] args) throws Exception {
        new LinkerStarter().run(args);

        printInfo();
    }

    @Override
    public void initialize(Bootstrap<AppConfiguration> bootstrap) {
        bootstrap.addBundle(
                GuiceBundle.builder()
                        .enableAutoConfig(getClass().getPackage().getName())
                        .modules(new ConfigModule(), new PersistenceModule())
                        .build()
        );
        bootstrap.addBundle(SpaBundle.app("app", "/web/assets/", "/").indexPage("index.html").build());
        bootstrap.addBundle(new MultiPartBundle());
    }

    @Override
    public void run(AppConfiguration appConfiguration, Environment environment) throws Exception {
        enableCors(environment);
    }

    private void enableCors(Environment environment) {
        final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    }

    private static void printInfo() {
        System.out.println("========== Linker ==========");
        System.out.println("client : http://localhost:8080");
        System.out.println("============================");
    }
}
