package io.powroseba.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Setter;

@Setter
public class AppConfiguration extends Configuration {

    @JsonProperty("profile")
    private String profile;

    @JsonIgnore
    public boolean isDevelopment() {
        return "development".equals(profile);
    }


}
