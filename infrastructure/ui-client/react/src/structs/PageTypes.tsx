interface ApiOrderable {
  displayOrder : number;
}

interface LinkData {
  displayText : string,
  webAddress : string
  imageAddress : string;
}

interface Link {
  id : string;
  data : LinkData;
  order : number;
}

interface Column {
  id : string;
  name : string;
  order : number;
  rows : Link[];
}

interface Scene {
    id : string;
    columns : Column[];
    order: number;
}

interface SceneGroup {
    scenes : Scene[]
}

interface Page {
  id: string;
  displayText: string;
  order : number;
}

interface ApiLink extends ApiOrderable {
  linkId : string;
  displayText : string;
  webReference : string;
  imageAddress : string;
}

interface ApiContainer extends ApiOrderable {
  containerId : string;
  displayText : string;
  links : ApiLink[]
}

interface ApiPage extends ApiOrderable {
  id : string;
  displayText : string;
  containers : ApiContainer[]
}

interface ApiModifyLink {
  displayText : string,
  webAddress : string,
  imageAddress : string | null
}

export type {
  LinkData, Link, Column, Scene, SceneGroup, ApiPage, ApiContainer, ApiLink, Page, ApiOrderable, ApiModifyLink
}