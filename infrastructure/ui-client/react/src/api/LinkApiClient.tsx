import { ApiModifyLink } from "../structs/PageTypes";
import { ApiClient } from "./ApiClient";

const pagesClient = new ApiClient('/pages');

function reorderLinks(pageId: string, containerId: string, linkId: string, order: number | null): Promise<Response> {
    return pagesClient.put(`/${pageId}/containers/${containerId}/links/${linkId}/order/${order}`);
}

function addLink(pageId: string, containerId: string): Promise<Response> {
    return pagesClient.post(`/${pageId}/containers/${containerId}/links/`,
        {
            displayText: 'NewPage',
            webAddress: ''
        }
    );
}

function modifyLink(pageId : string,
                    containerId : string,
                    linkId : string,
                    modifyData : ApiModifyLink): Promise<Response> {
    return pagesClient.put(`/${pageId}/containers/${containerId}/links/${linkId}`,
        {
            displayText: modifyData.displayText,
            webAddress: modifyData.webAddress,
            imageAddress: modifyData.imageAddress
        }
    );
}

function removeLink(pageId : string, containerId : string, linkId : string) : Promise<Response> {
    return pagesClient.delete(`/${pageId}/containers/${containerId}/links/${linkId}`);
}

export {
    reorderLinks,
    addLink,
    modifyLink,
    removeLink
};
