
export class ApiClient {
    static apiUrl: string = process.env.REACT_APP_SERVER_API_URL + '/api';
    path: string;

    constructor(path?: string) {
        this.path = path != null ? ApiClient.apiUrl + path : ApiClient.apiUrl;
    }

    post(path?: string, body?: object): Promise<Response> {
        return fetch(this.evaluateContextPath(path), {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: body != null ? JSON.stringify(body) : null
        });
    }

    put(path?: string, body?: object): Promise<Response> {
        return fetch(this.evaluateContextPath(path), {
            method: 'PUT',
            headers: { "Content-Type": "application/json" },
            body: body != null ? JSON.stringify(body) : null
        });
    }

    get(path?: string): Promise<Response> {
        return fetch(this.evaluateContextPath(path), {
            method: 'GET',
            headers: { "Content-Type": "application/json" }
        });
    }

    delete(path?: string): Promise<Response> {
        return fetch(this.evaluateContextPath(path), {
            method: 'DELETE',
            headers: { "Content-Type": "application/json" }
        });
    }

    download(filename: string, path?: string): void {
        fetch(this.evaluateContextPath(path), {
            method: 'GET',
            headers: { "Content-Type": "application/json" }
        })
        .then(res => res.blob())
        .then((blob) => URL.createObjectURL(blob))
        .then((href) => {
          Object.assign(document.createElement('a'), {
            href,
            download: filename
          }).click();
        });
    }

    upload(file: File, path?: string): Promise<Response> {
        const formData = new FormData();
        formData.append("file", file);
        return fetch(this.evaluateContextPath(path), {
            method: 'POST',
            body: formData
        });
    }

    evaluateContextPath(path: string | undefined): string {
        return path != null ? this.path + path : this.path;
    }
}
