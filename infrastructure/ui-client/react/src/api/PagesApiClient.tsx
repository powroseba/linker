
import { ApiPage, Page } from "../structs/PageTypes";
import { ApiClient } from "./ApiClient";

const pagesClient = new ApiClient('/pages');

function fetchAllPagesWithoutColumns(): Promise<Page[]> {
    return pagesClient.get()
    .then(response => response.json())
    .then((data: ApiPage[]) => data.sort((a : ApiPage, b : ApiPage) => a.displayOrder < b.displayOrder ? -1 : 1))
    .then((data: ApiPage[]) => data.map((page : ApiPage) => {
      return {
        id: page.id,
        displayText: page.displayText,
        order: page.displayOrder
      };
    }));
}

function addPage(pageName : String): Promise<Response> {
  return pagesClient.post('', { displayText: pageName });
}

function reorderPages(pageId : string, newIndex : number | null) : Promise<Response> {
  return pagesClient.put(`/${ pageId }/order/${newIndex}`);
}

function removePage(pageId : string) : Promise<Response> {
  return pagesClient.delete(`/${pageId}`);
}

function fetchPageData(pageId : string): Promise<Response> {
  return pagesClient.get(`/${pageId}`);
}

function exportPages(): void {
  return pagesClient.download("pages.json", "/export");
}

function loadPages(file: File) : Promise<Response> {
  return pagesClient.upload(file, "/import");
}

export {
  fetchAllPagesWithoutColumns,
  addPage,
  reorderPages,
  removePage,
  fetchPageData,
  exportPages,
  loadPages
};
