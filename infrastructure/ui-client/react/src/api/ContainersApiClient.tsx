import { ApiClient } from "./ApiClient";

const pagesClient = new ApiClient('/pages');

function reorderContainers(pageId: string, containerId : string, destinationOrder: number | null): Promise<Response> {
    return pagesClient.put(`/${pageId}/containers/${containerId}/order/${destinationOrder}`);
}

function addContianer(pageId : string) : Promise<Response> {
    return pagesClient.post(`/${pageId}/containers`, { displayText: 'NewPage' });
}

function removeContainer(pageId : string, containerId : string) : Promise<Response> {
    return pagesClient.delete(`/${pageId}/containers/${containerId}`);
}

function modifyContainer(pageId : string, containerId : string, displayText : string): Promise<Response> {
    return pagesClient.put(`/${pageId}/containers/${containerId}`, { displayText: displayText });
}

export {
    reorderContainers,
    addContianer,
    removeContainer,
    modifyContainer
};
