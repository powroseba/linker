import { configureStore } from '@reduxjs/toolkit';
import pageReducer from './page/PageWithoutColumnSlice';
import { useDispatch } from 'react-redux';
import ModifiedLinkSlice from './link/ModifiedLinkSlice';

export const store = configureStore({
    reducer: {
        pages: pageReducer,
        modifiedLink: ModifiedLinkSlice
    }
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
