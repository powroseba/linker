import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../StoreFactory";

export interface ModifiedLinkData {
    id: string;
}

export interface ModifiedLinkState {
    id: string | null;
}

const initialModifiedLink: ModifiedLinkState = {
    id: null
};

export const modifiedLinkSlice = createSlice({
    name: 'modifiedLink',
    initialState: initialModifiedLink,
    reducers: {
        setModifiedLink: (state: ModifiedLinkState, action: PayloadAction<ModifiedLinkData>) => {
            return {
                ...state,
                id: action.payload.id
            };
        },
        resetModification: (state) => {
            return {
                ...state,
                id: null
            };
        }
    }
});


export const modifiedLinkSelector = (state: RootState) => state.modifiedLink;

export const { setModifiedLink, resetModification } = modifiedLinkSlice.actions;

export default modifiedLinkSlice.reducer;
