import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchAllPagesWithoutColumns } from '../../api/PagesApiClient';
import { Page } from "../../structs/PageTypes";
import { RootState } from "../StoreFactory";

export interface FetchPageState {
    loading : boolean;
    pages : Page[];
    error : string;
}

const fetchPagesState: FetchPageState = {
    loading: false,
    pages: [],
    error: ''
};

export const fetchPages = createAsyncThunk(
    'pageWithoutColumnProvider/fetchPages',
    async () => {
        return fetchAllPagesWithoutColumns();
    }
);

export const pageSlice = createSlice({
    name: 'pageWithoutColumnProvider',
    initialState: fetchPagesState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(fetchPages.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(fetchPages.fulfilled, (state, { payload }) => {
            state.loading = false;
            state.pages = payload;
            state.error = '';
        });
        builder.addCase(fetchPages.rejected, (state) => {
            state.loading = false;
            state.pages = [];
            state.error = 'Error in fetching pages';
        });
    }
});

export const pagesSelector = (state : RootState) => state.pages;

export default pageSlice.reducer;
