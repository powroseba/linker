import { useState } from "react";
import { Draggable } from "react-smooth-dnd";
import { Button, ButtonGroup, Card, Confirm } from "semantic-ui-react";
import { modifyContainer, removeContainer, reorderContainers } from "../api/ContainersApiClient";
import EditableTextComponent from "./EditableTextComponent";
import LinkContainerComponent, { LinkContainerProperties } from "./LinkContainerComponent";
import './PageComponent.css';
import { MAX_COLUMN_PER_SCENE } from "./PageConfig";

interface DraggableLinkContainerProperties extends LinkContainerProperties {
    columnCount: number;
}

function DraggableLinkContainerComponent(props: DraggableLinkContainerProperties) {
    const [confirmState, setConfirmState] = useState({ open: false });

    const removeColumn = () => {
        removeContainer(props.pageId, props.column.id)
            .then(() => {
                props.reloadPageData();
                setConfirmState({ open: false });
            })
            .catch(() => {
                props.reloadPageData();
                setConfirmState({ open: false });
            });
    };

    const handleConfirm = () => {
        removeColumn();
    };

    const handleCancel = () => {
        setConfirmState({ open: false });
    };

    const modifyContainerText = (name: string) => {
        modifyContainer(props.pageId, props.column.id, name)
            .then(() => {
                props.reloadPageData();
            })
            .catch(error => {
                console.log(error);
            });
    };

    const switchContainerToAnotherScene = (destinationOrder: number) => {
        reorderContainers(props.pageId, props.column.id, destinationOrder)
            .then(() => {
                props.reloadPageData();
            })
            .catch(error => {
                console.log(error);
            });
    };

    return (
        <Draggable key={props.column.id} className={"draggable-link-container column-drag-handle"}>
            <Card className={"w100-important"} key={props.column.id}>
                { props.column.order >= MAX_COLUMN_PER_SCENE ?
                    <Button basic icon="angle up" size="tiny" className={"no-draggable-area right pt-0 pb-2 no-box-shadow"}
                            onClick={() =>
                                switchContainerToAnotherScene(props.column.order - MAX_COLUMN_PER_SCENE)
                            }
                    /> : ''
                }
                <Card.Content style={{ borderTop: 0 }} className={"mb-5 mt-5 pb-0 pt-0"}>
                    <ButtonGroup floated="right" className={"no-draggable-area"}>
                        <Button icon="trash alternate" negative size="tiny"
                            onClick={() => setConfirmState({ open: true })} />
                        <Confirm open={confirmState.open} onCancel={handleCancel} onConfirm={handleConfirm}
                            cancelButton="Cancel"
                            confirmButton="Delete"
                            content="Are you sure you want to delete this container?"
                            size="tiny" />
                    </ButtonGroup>
                    <Card.Header style={{ marginTop: '3px' }} className={"font-1em"}>
                        <EditableTextComponent
                            initialText={props.column.name}
                            successCallback={modifyContainerText}
                            className={"no-draggable-area editable-name"}
                        />
                    </Card.Header>
                </Card.Content>
                <Card.Content extra>
                    <LinkContainerComponent
                        column={props.column}
                        sceneId={props.sceneId}
                        onCardDrop={props.onCardDrop}
                        getCardPayload={props.getCardPayload}
                        pageId={props.pageId}
                        reloadPageData={props.reloadPageData}
                    />
                </Card.Content>
                { props.column.order < props.columnCount - MAX_COLUMN_PER_SCENE ?
                    <Button basic icon="angle down" size="tiny" className={"no-draggable-area right pt-2 pb-0 no-box-shadow"}
                            onClick={() =>
                                switchContainerToAnotherScene(props.column.order + MAX_COLUMN_PER_SCENE)
                            }
                    /> : ''
                }
            </Card>
        </Draggable>
    );
}

export default DraggableLinkContainerComponent;
