import { fetchPageData } from "../../api/PagesApiClient";
import { ApiContainer, ApiLink, ApiOrderable, ApiPage, Column, Link, SceneGroup } from "../../structs/PageTypes";
import { MAX_COLUMN_PER_SCENE } from "../PageConfig";

function getPageData(pageId: string): Promise<SceneGroup> {
    return fetchPageData(pageId)
        .then(response => response.json())
        .then(data => map(data))
        .catch(error => {
            console.log(error);
            return { scenes: [] };
        });
}

function map(data: ApiPage): SceneGroup {
    const sceneGroup: SceneGroup = { scenes: [] };
    data.containers = sortByOrder(data.containers);
    let sceneOrder = 0;
    for (let index: number = 1; index <= data.containers.length; index++) {
        const groupIndex = Math.ceil(index / MAX_COLUMN_PER_SCENE) - 1;
        if (sceneGroup.scenes[groupIndex] == null) {
            sceneGroup.scenes.push({ id: `Scene_${index}`, columns: [], order: sceneOrder++ });
        }
        sceneGroup.scenes[groupIndex].columns.push(toColumn(data.containers[index - 1]));
    }

    return sceneGroup;
}

function toColumn(container: ApiContainer): Column {
    return {
        id: container.containerId,
        name: container.displayText,
        order: container.displayOrder,
        rows: sortByOrder(container.links).map(link => toLink(link))
    };
}

function toLink(link: ApiLink): Link {
    return {
        id: link.linkId,
        data: {
            displayText: link.displayText,
            webAddress: link.webReference,
            imageAddress: link.imageAddress
        },
        order: link.displayOrder
    };
}

function sortByOrder<T extends ApiOrderable>(orderable : T[]) : T[] {
    return orderable.sort((a : T, b : T) => a.displayOrder < b.displayOrder ? -1 : 1);
}

export {
    getPageData
};
