import { useState, useEffect } from "react";
import { match } from "react-router-dom";
import { Container, DropResult } from "react-smooth-dnd";
import DndUtils from '../commons/DndUtils';
import DraggableLinkContainerComponent from "./DraggableLinkContainerComponent";
import './PageComponent.css';
import { Column, Link, Scene, SceneGroup } from '../structs/PageTypes';
import { getPageData } from './service/PageService';
import { addContianer, reorderContainers } from "../api/ContainersApiClient";
import { MAX_COLUMN_PER_SCENE } from "./PageConfig";
import { Button, ButtonGroup } from "semantic-ui-react";
import { reorderLinks } from "../api/LinkApiClient";

interface RouteParams {
  pageId: string
}

interface PageProperties {
  match: match<RouteParams>
}

function PageComponent(props: PageProperties) {
  const [sceneGroup, setSceneGroup] = useState<SceneGroup>({ scenes: [] });

  useEffect(() => {
    fetchPageData();
  }, []);

  function fetchPageData() {
    getPageData(props.match.params.pageId)
      .then(sceneGroup => setSceneGroup(sceneGroup));
  }

  const getScene = (sceneGroup: SceneGroup, sceneId: string): Scene => {
    return sceneGroup.scenes.filter((scene: Scene) => scene.id === sceneId)[0];
  };

  const getCardPayload = (sceneId: string, columnId: string, index: number): Link => {
    return getScene(sceneGroup, sceneId)
      .columns.filter((column: any) => column.id === columnId)[0]
      .rows[index];
  };

  const getColumnOfSceneByIndex = (sceneId: string, index: number | null): Column | null => {
    return index != null ? getScene(sceneGroup, sceneId).columns[index] : null;
  };

  const onColumnDrop = (sceneId: string, dropResult: DropResult) => {
    if (dropResult.addedIndex !== dropResult.removedIndex) {
      const movedColumn = getColumnOfSceneByIndex(sceneId, dropResult.removedIndex);
      if (movedColumn != null) {
        const sceneGroupCopy = Object.assign({}, sceneGroup);
        const sceneToModify = getScene(sceneGroupCopy, sceneId);
        sceneToModify.columns = DndUtils.applyDrag(sceneToModify.columns, dropResult);
        setSceneGroup(sceneGroupCopy);
        const destinationOrder = (sceneToModify.order * MAX_COLUMN_PER_SCENE) + (dropResult.addedIndex ?? 0);
        reorderContainers(props.match.params.pageId, movedColumn.id, destinationOrder)
          .then(() => fetchPageData())
          .catch(() => fetchPageData());
      }
    }
  };

  const onCardDrop = (sceneId: string, columnId: string, dropResult: DropResult) => {
    if (dropResult.removedIndex !== null || dropResult.addedIndex !== null) {
      const sceneGroupCopy = Object.assign({}, sceneGroup);
      const sceneToModify = getScene(sceneGroupCopy, sceneId);
      const column = sceneToModify.columns.filter((p: any) => p.id === columnId)[0];
      const columnIndex = sceneToModify.columns.indexOf(column);

      const newColumn = Object.assign({}, column);
      newColumn.rows = DndUtils.applyDrag(newColumn.rows, dropResult);
      sceneToModify.columns.splice(columnIndex, 1, newColumn);

      setSceneGroup(sceneGroupCopy);

      if (dropResult.addedIndex !== null) {
        reorderLinks(props.match.params.pageId, columnId, dropResult.payload.id, dropResult.addedIndex)
          .then(() => fetchPageData())
          .catch(() => fetchPageData());
      }
    }
  };

  const addColumn = () => {
    addContianer(props.match.params.pageId)
      .then(() => fetchPageData())
      .catch(() => fetchPageData());
  };

  return (
    <div style={{ marginBottom: '60px' }}>
      <ButtonGroup floated="right">
        <Button
            style={{ position: 'fixed', bottom: '5px', right: '5px', zIndex: 2, borderRadius: '50px' }}
            icon="add"
            positive
            size="huge"
            onClick={() => addColumn()}
        />
      </ButtonGroup>
      {sceneGroup.scenes.map((scene: Scene) => (
        <div key={scene.id}>
          <Container
            shouldAcceptDrop={draggedObject => draggedObject.groupName === scene.id && scene.columns.length <= 4}
            groupName={scene.id}
            orientation="horizontal"
            onDrop={dropResult => onColumnDrop(scene.id, dropResult)}
            nonDragAreaSelector=".no-draggable-area"
            dragClass={"column-drag-class"}
            dropPlaceholder={{
              animationDuration: 150,
              showOnTop: true,
              className: 'column-drop-preview'
            }}
            style={{ width: '100%' }}
          >
            {scene.columns.map((column: any) => {
              return (
                <DraggableLinkContainerComponent
                  key={column.id}
                  column={column}
                  sceneId={scene.id}
                  onCardDrop={onCardDrop}
                  getCardPayload={getCardPayload}
                  pageId={props.match.params.pageId}
                  reloadPageData={fetchPageData}
                  columnCount={
                    sceneGroup.scenes
                        .map((scene: Scene) => scene.columns.length)
                        .reduce((left, right) => left + right)
                }
                />
              );
            })}
          </Container>
        </div>
      ))}
    </div>
  );
}

export default PageComponent;

