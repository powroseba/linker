import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Draggable } from "react-smooth-dnd";
import { Button, Form, Icon, List, Image, Confirm } from "semantic-ui-react";
import { modifyLink, removeLink } from "../api/LinkApiClient";
import {
  modifiedLinkSelector,
  ModifiedLinkState,
  resetModification,
  setModifiedLink
} from "../store/link/ModifiedLinkSlice";
import { Link } from '../structs/PageTypes';
import './PageComponent.css';
import icon from '../assets/images/icon.png';

interface LinkProperties {
  data: Link,
  pageId: string,
  columnId: string,
  reloadPageData: () => void
}

function LinkComponent(props: LinkProperties) {
  const modifiedLink: ModifiedLinkState = useSelector(modifiedLinkSelector);
  const dispatch = useDispatch();
  const [linkDisplayText, setLinkDisplayText] = useState(props.data.data.displayText || "");
  const [displayText, setDisplayText] = useState(props.data.data.displayText || "");
  const [webAddress, setWebAddress] = useState(props.data.data.webAddress || "");
  const [imageAddress, setImageAddress] = useState(props.data.data.imageAddress || null);
  const [confirmState, setConfirmState] = useState({ open: false });
  const openLinkInNewTab = () => {
    if (webAddress.length > 0) {
      window.open('http://' + webAddress, '_blank')?.focus();
    }
  };

  const turnOnModification = () => {
    dispatch(setModifiedLink({ id: props.data.id }));
  };

  const turnOffModification = () => {
    dispatch(resetModification());
  };

  const nullWhenBlank = (value: string | null): string | null => {
    return (!value || /^\s*$/.test(value)) ? null : value.trimEnd().trimStart();
  };

  const isModifyValueChangedAndValid = () => {
    return (props.data.data.displayText !== displayText && displayText.trimStart().trimEnd()) ||
           (props.data.data.webAddress !== webAddress && webAddress.trimStart().trimEnd()) ||
           (props.data.data.imageAddress !== imageAddress && imageAddress?.trimStart().trimEnd());
  };

  const modifyLinkData = () => {
    if (isModifyValueChangedAndValid()) {
      modifyLink(
        props.pageId, props.columnId, props.data.id,
        {
          displayText: displayText.trimEnd().trimStart(),
          webAddress: webAddress.trimStart().trimEnd(),
          imageAddress: nullWhenBlank(imageAddress)
        })
        .then(() => {
          props.reloadPageData();
          turnOffModification();
          setLinkDisplayText(displayText);
        })
        .catch(error => {
          console.log(error);
          turnOffModification();
          setLinkDisplayText(displayText);
        });
    }
  };

  const deleteLink = () => {
    removeLink(props.pageId, props.columnId, props.data.id)
      .then(() => {
        props.reloadPageData();
        turnOffModification();
        setConfirmState({ open: false });
      })
      .catch(error => {
        console.log(error);
        turnOffModification();
        setConfirmState({ open: false });
      });
  };

  const handleConfirm = () => {
    deleteLink();
  };

  const handleCancel = () => {
    setConfirmState({ open: false });
  };

  const changeLinkDisplayText = (type: 'out' | 'in') => {
    if (type === 'in') {
      if (webAddress.length > 0) {
        setLinkDisplayText("http://" + webAddress);
      } else {
        setLinkDisplayText("address not specified");
      }
    }
    if (type === 'out') {
      setLinkDisplayText(displayText);
    }
  };

  const handleSubmit = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      modifyLinkData();
    }
    if (event.key === "Escape") {
      turnOffModification();
    }
  };

  const nonEditableContent = (
    <List.Item className={"row-content row-drag-handle"}>
      <List.Content floated="left">
        {nullWhenBlank(imageAddress) != null ?
          <Image avatar size="mini" src={imageAddress} /> :
          <Image avatar size="mini" src={icon} />
        }
      </List.Content>
      <List.Content floated="right">
        <Icon name="edit" className={"icon"} size="small" color="grey" onClick={() => turnOnModification()} />
      </List.Content>
      <List.Content onMouseOver={() => changeLinkDisplayText('in')} onMouseOut={() => changeLinkDisplayText('out')}>
        <List.Header onClick={() => openLinkInNewTab()} className={"font-0_75em"}>
          {linkDisplayText}
        </List.Header>
      </List.Content>
    </List.Item>
  );
  const editableContent = (
    <List.Item className={"row-editable"}>
      <List.Content floated="right">
        <Icon name="cancel" className={"button-icon"} size="small" color="red" onClick={() => turnOffModification()} />
      </List.Content>
      <List.Header>
        <Form.Group widths="equal">
          <Form.Input
            className={"font-0_75em"}
            fluid
            label="Image address"
            autoFocus
            value={imageAddress || ""}
            onKeyDown={(event: any) => handleSubmit(event)}
            onChange={(event) => setImageAddress(event.target.value)}
          />
          <Form.Input
            className={"font-0_75em"}
            fluid
            label="Display text"
            autoFocus
            value={displayText || ""}
            onKeyDown={(event: any) => handleSubmit(event)}
            onChange={(event) => setDisplayText(event.target.value)}
          />
          <Form.Input
            className={"font-0_75em"}
            fluid
            label="Web address"
            value={webAddress || ""}
            onKeyDown={(event: any) => handleSubmit(event)}
            onChange={(event) => setWebAddress(event.target.value)}
          />
          <div style={{ padding: '10px 0', marginBottom: '25px' }}>
            <Button floated="left" positive size="tiny" content="Submit" type="submit" onClick={() => modifyLinkData()} />
            <Button floated="right" negative size="tiny" icon="trash alternate"
              onClick={() => setConfirmState({ open: true })} />
            <Confirm open={confirmState.open} onCancel={handleCancel} onConfirm={handleConfirm}
                     cancelButton="Cancel"
                     confirmButton="Delete"
                     content="Are you sure you want to delete this link?"
                     size="tiny" />
          </div>
        </Form.Group>
      </List.Header>
    </List.Item>
  );

  return (
    <Draggable key={props.data.id}>
      {modifiedLink.id != null && modifiedLink.id === props.data.id ? editableContent : nonEditableContent}
    </Draggable>
  );
}

export default LinkComponent;
