import { useState } from "react";
import { Input } from "semantic-ui-react";

interface EditableTextProperties {
    initialText?: string;
    successCallback: (name: string) => void;
    className: string;
}

function EditableTextComponent(props: EditableTextProperties) {
    const [editable, setEditable] = useState(false);
    const [text, setText] = useState(props.initialText || "");

    const handleClick = () => {
        if (text !== props.initialText && !!text.trimStart().trimEnd()) {
            props.successCallback(text);
        } else {
            setText(props.initialText || "");
            setEditable(false);
        }
    };

    const handleEnterAndEscape = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            if (!text.trimStart().trimEnd()) {
                setText(props.initialText || "");
                setEditable(false);
            } else {
                props.successCallback(text);
                event.preventDefault();
                event.stopPropagation();
                setEditable(false);
            }
        }
        if (event.key === "Escape") {
            setText(props.initialText || "");
            setEditable(false);
        }
    };

    return (
        <span className={props.className}>
            {editable ?
                (
                    <span>
                        <Input
                            autoFocus
                            style={{ height: '30px' }}
                            action={{ icon: "edit", onClick: handleClick }}
                            value={text}
                            onKeyDown={(event: any) => handleEnterAndEscape(event)}
                            onChange={(event) => setText(event.target.value)}
                        />
                    </span>
                ) :
                (<span onDoubleClick={() => setEditable(true)}>{text}</span>)
            }
        </span>
    );
}

export default EditableTextComponent;
