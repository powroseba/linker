import { Container, DropResult } from "react-smooth-dnd";
import LinkComponent from "./LinkComponent";
import './PageComponent.css';

import { Column, Link } from '../structs/PageTypes';
import { Button, ButtonGroup, List } from "semantic-ui-react";
import { addLink } from "../api/LinkApiClient";

export interface LinkContainerProperties {
    pageId: string;
    column: Column;
    sceneId: string;
    onCardDrop: (sceneId: string, columnId: string, dropResult: DropResult) => void;
    getCardPayload: (sceneId: string, columnId: string, cardIndex: number) => Link;
    reloadPageData: () => void;
}

function LinkContainerComponent(props: LinkContainerProperties) {
    const appendLink = () => {
        addLink(props.pageId, props.column.id)
            .then(() => props.reloadPageData())
            .catch(() => props.reloadPageData());
    };

    return (
        <div>
            <List verticalAlign="middle">
                <Container
                    groupName="row"
                    onDrop={e => props.onCardDrop(props.sceneId, props.column.id, e)}
                    getChildPayload={index =>
                        props.getCardPayload(props.sceneId, props.column.id, index)
                    }
                    dragHandleSelector=".row-drag-handle"
                    dragClass="row-ghost"
                    dropClass="row-ghost-drop"
                    dropPlaceholder={{
                        animationDuration: 150,
                        showOnTop: true,
                        className: 'drop-preview'
                    }}
                >
                    {props.column.rows.map((link: Link) => {
                        return (
                            <LinkComponent
                                key={link.id}
                                data={link}
                                columnId={props.column.id}
                                pageId={props.pageId}
                                reloadPageData={props.reloadPageData}
                            />
                        );
                    })}
                </Container>
            </List>
            <ButtonGroup floated="right">
                <Button icon="add" color={'blue'} size="mini" onClick={() => appendLink()} />
            </ButtonGroup>
        </div>
    );
}

export default LinkContainerComponent;
