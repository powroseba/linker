import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import NavbarComponent from './navbar/NavbarComponent';
import PageComponent from './links/PageComponent';
import DashboardComponent from './dashboard/DashboardComponent';
import NotesComponent from './notes/NotesComponent';
import { Provider } from 'react-redux';
import { store } from './store/StoreFactory';

function AppComponent() {
  return (
    <div>
      <Provider store={store}>
        <Router>
          <div>
            <NavbarComponent />
            <div>
              <Switch>
                <Route exact path="/" component={DashboardComponent} />
                <Route path="/pages/:pageId" component={PageComponent} />
                <Route path="/notes" component={NotesComponent} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    </div>);
}

export default AppComponent;
