import { useState, useEffect, useRef, ChangeEvent } from 'react';
import { Dropdown, Menu } from 'semantic-ui-react';
import { Page } from "../structs/PageTypes";

import { useSelector, useDispatch } from 'react-redux';
import { fetchPages, FetchPageState } from '../store/page/PageWithoutColumnSlice';
import { pagesSelector } from '../store/page/PageWithoutColumnSlice';
import icon from '../assets/images/icon.png';
import { exportPages, loadPages } from '../api/PagesApiClient';

function NavbarComponent(props: any) {
  const [active, setActive] = useState<String>('');
  const dispatch = useDispatch();
  const inputFile = useRef<HTMLInputElement>(null);

  const pagesState: FetchPageState = useSelector(pagesSelector);

  const getAllPages = () => {
    dispatch(fetchPages());
  };

  const handleLoadFile = (event : ChangeEvent<HTMLInputElement>) => {
    const file : File | undefined = event?.target?.files?.[0];
    if (file != null) {
      loadPages(file)
        .then(res => {
          if (res.status === 200) {
            window.location.replace(window.location.origin);
          }
        });
    }
  };

  useEffect(() => {
    setActive(window.location.pathname);
    getAllPages();
  }, []);

  return (
    <Menu fixed="top" widths={12} style={{ position: "sticky", marginBottom: "10px" }}>
      <Dropdown item icon="wrench" style={{ width: "50px" }}>
        <Dropdown.Menu>
          <input type="file" id="file" ref={inputFile} onChange={handleLoadFile} style={{ display: "none" }} />
          <Dropdown.Item onClick={() => inputFile?.current?.click()}>Load</Dropdown.Item>
          <Dropdown.Item onClick={exportPages}>Export</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <Menu.Item active={active === '/'} href="/">
        <img src={icon} />
      </Menu.Item>

      {pagesState.pages.map((page: Page) => (
        <Menu.Item
          name={String(page.id)}
          active={active.includes('pages/' + page.id)}
          href={"/pages/" + page.id}
          key={page.id}
        >
          {page.displayText}
        </Menu.Item>
      ))}

      {/* <Menu.Item
        name="notes"
        active={active.includes("notes")}
        href="/notes"
      >
        Notes
      </Menu.Item> */}
    </Menu>
  );
}

export default NavbarComponent;
