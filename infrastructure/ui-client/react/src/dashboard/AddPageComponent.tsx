import { useState } from 'react';
import { Form, Grid, Message } from 'semantic-ui-react';
import { addPage } from '../api/PagesApiClient';

export interface AddPageProperties {
    successFunction: () => void
}

function AddPageComponent(props: AddPageProperties) {
    const [pageName, setPageName] = useState<String>('');
    const [state, setState] = useState<'loading' | 'success' | 'error'>();

    const submitAddPage = () => {
        if (pageName !== undefined && !!pageName.trimStart().trimEnd()) {
            setState('loading');
            addPage(pageName)
                .then(response => {
                    if (response.status === 201) {
                        setPageName('');
                        setState('success');
                        props.successFunction();
                    }
                })
                .catch(() => setState('error'));
        }
    };

    return (
        <Grid centered>
            <Grid.Row columns={1}>
                <Form onSubmit={submitAddPage}
                      loading={state === 'loading'}
                      error={state === 'error'}
                      success={state === 'success'}
                >
                    <Message
                        success
                        header="Page created"
                    />
                    <Message
                        error
                        header="Page creation failed"
                    />
                    <Form.Group>
                        <Form.Input
                            placeholder="Page name"
                            name="pageName"
                            value={pageName}
                            onChange={e => setPageName(e.target.value)}
                        />
                        <Form.Button content="Submit" />
                    </Form.Group>
                </Form>
            </Grid.Row>
        </Grid>
    );
}

export default AddPageComponent;
