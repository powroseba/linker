import { useEffect, useState } from 'react';
import { Container, Draggable, DropResult } from 'react-smooth-dnd';
import { Button, Card, Confirm } from 'semantic-ui-react';
import DndUtils from '../commons/DndUtils';
import { fetchAllPagesWithoutColumns, reorderPages, removePage } from '../api/PagesApiClient';
import { Page } from "../structs/PageTypes";

import { useAppDispatch } from '../store/StoreFactory';
import { fetchPages } from '../store/page/PageWithoutColumnSlice';
import AddPageComponent from './AddPageComponent';


function DashboardComponent() {
  const [pages, setPages] = useState<Page[]>([]);
  const [confirmState, setConfirmState] = useState({ open: false, pageId: "" });

  const dispatch = useAppDispatch();

  const getAllPages = () => {
    fetchAllPagesWithoutColumns().then(pages => setPages(pages));
  };

  useEffect(() => {
    getAllPages();
    dispatch(fetchPages());
  }, []);

  const updatePageChanges = () => {
    dispatch(fetchPages());
  };

  const onDrop = (dropResult: DropResult) => {
    const { removedIndex, addedIndex, payload } = dropResult;
    if (removedIndex !== undefined || addedIndex !== undefined) {
      setPages(DndUtils.applyDrag(pages, dropResult));
      reorderPages(payload.id, addedIndex)
        .then(() => updatePageChanges())
        .catch(() => updatePageChanges());
    }
  };

  const getPayload = (index: number): Page => {
    return pages[index];
  };

  const deletePage = (pageId: string) => {
    removePage(pageId)
      .then((response: any) => {
        if (response.status === 200) {
          updatePageChanges();
          getAllPages();
        }
        setConfirmState({ open: false, pageId: "" });
      }).catch(error => {
        updatePageChanges();
        getAllPages();
        setConfirmState({ open: false, pageId: "" });
      });
  };

  const handleConfirm = () => {
    if (confirmState.pageId !== "") {
      deletePage(confirmState.pageId);
    }
  };

  const handleCancel = () => {
    setConfirmState({ open: false, pageId: "" });
  };

  return (
    <div>
      <AddPageComponent
        successFunction={() => {
          updatePageChanges();
          getAllPages();
        }}
      />
      <Card.Group centered>
        <Container dragHandleSelector=".drag-handle"
          lockAxis="y"
          onDrop={onDrop}
          getChildPayload={index => getPayload(index)}
        >
          {pages.map((page: Page) => (
            <Draggable key={page.id}>
              <div className="drag-handle" key={page.id}>
                <Card raised key={page.id} style={{ margin: '10px' }}>
                  <Card.Content>
                    <Button floated="right" basic color="red" icon="trash"
                            onClick={() => setConfirmState({ open: true, pageId: page.id })} />
                    <Confirm open={confirmState.open} onCancel={handleCancel} onConfirm={handleConfirm}
                            cancelButton="Cancel"
                            confirmButton="Delete"
                            content="Are you sure you want to delete this page?"
                            size="tiny" />
                    <Card.Header>{page.displayText}</Card.Header>
                    <Card.Meta href={'/pages/' + page.id}>Display page</Card.Meta>
                  </Card.Content>
                </Card>
              </div>
            </Draggable>
          ))}
        </Container>
      </Card.Group>
    </div>
  );
}

export default DashboardComponent;
