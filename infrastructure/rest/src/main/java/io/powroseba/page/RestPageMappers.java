package io.powroseba.page;

import io.powroseba.page.container.Container;
import io.powroseba.page.container.RestContainerMapper;
import io.powroseba.page.structs.PageWithoutColumn;
import io.powroseba.rest.pages.dto.ContainerDto;
import io.powroseba.rest.pages.dto.FullPageDto;
import io.powroseba.rest.pages.dto.PageWithoutColumnDto;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RestPageMappers {

    public static FullPageDto fullPage(Page page) {
        return new FullPageDto(
                page.pageId.value.toString(),
                page.getDisplayText(),
                page.displayOrder,
                mapContainers(page.containers())
        );
    }

    private static Set<ContainerDto> mapContainers(Stream<Container> containers) {
        return containers
                .map(RestContainerMapper.container())
                .collect(Collectors.toSet());
    }

    public static Function<PageWithoutColumn, PageWithoutColumnDto> pageWithoutColumn() {
        return page -> new PageWithoutColumnDto(page.pageId.value.toString(), page.displayText, page.displayOrder);
    }

}
