package io.powroseba.page.link;

import io.powroseba.rest.pages.dto.LinkDto;

import java.util.function.Function;

public class RestLinkMapper {

    public static Function<Link, LinkDto> link() {
        return link -> new LinkDto(
                link.linkId.value,
                link.displayText.content,
                link.webReference.webLink,
                link.image.address,
                link.displayOrder
        );
    }
}
