package io.powroseba.page.container;

import io.powroseba.page.link.RestLinkMapper;
import io.powroseba.rest.pages.dto.ContainerDto;

import java.util.function.Function;
import java.util.stream.Collectors;

public class RestContainerMapper {

    public static Function<Container, ContainerDto> container() {
        return container -> new ContainerDto(
                container.containerId.value,
                container.displayText.content,
                container.displayOrder,
                container.links().map(RestLinkMapper.link()).collect(Collectors.toSet())
        );
    }
}
