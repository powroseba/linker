package io.powroseba.rest.pages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import io.powroseba.page.*;
import io.powroseba.page.container.*;
import io.powroseba.page.link.*;
import io.powroseba.page.structs.*;
import io.powroseba.rest.pages.dto.*;
import io.powroseba.rest.pages.exporter.JsonPagesExporter;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Path("/pages")
@Produces(MediaType.APPLICATION_JSON)
public class PageResource {

    private final PageFacade pageFacade;
    private final ContainerFacade containerFacade;
    private final LinkFacade linkFacade;

    @Inject
    public PageResource(PagesRepository pagesRepository, PageRepository pageRepository,
                        ContainersRepository containersRepository, LinkRepository linkRepository,
                        LinksRepository linksRepository) {
        this.containerFacade = new ContainerFacade(containersRepository);
        this.pageFacade = new PageFacade(pagesRepository, pageRepository);
        this.linkFacade = new LinkFacade(linksRepository, linkRepository, containersRepository);
    }

    @GET
    public Response getPages() {
        log.debug("Fetching pages");
        final var response = pageFacade.findAllWithoutColumns()
                .stream()
                .map(RestPageMappers.pageWithoutColumn())
                .collect(Collectors.toSet());
        return Response.ok(response).build();
    }

    @POST
    public Response addPage(CreatePageDto dto) {
        log.debug("Page created with name {}", dto.displayText);
        pageFacade.addPage(new NewPage(new DisplayText(dto.displayText)));
        log.debug("New page created");
        return Response.status(Response.Status.CREATED).build();
    }

    @GET
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response export() {
        log.debug("Exporting pages to json file");
        return pageFacade.export(new JsonPagesExporter())
                .map(this::exportFile)
                .orElseGet(() -> Response.status(HttpStatus.NOT_FOUND_404).entity("pages not found").build());
    }

    @POST
    @Path("/import")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response receive(@FormDataParam("file") final InputStream inputStream) throws IOException {
        log.debug("Loading page from json");
        final var jsonPagesExportModel = new ObjectMapper().readValue(inputStream, JsonPagesExportModel.class);
        pageFacade.load(jsonPagesExportModel.toPages());
        log.debug("Pages loaded");
        return Response.ok().build();
    }

    @GET
    @Path("/{id}")
    public Response getPage(@PathParam("id") UUID pageIdentifier) {
        try {
            log.debug("Fetching page with id {}", pageIdentifier);
            final var fullPageDto = RestPageMappers.fullPage(pageFacade.findById(new PageId(pageIdentifier)));
            return Response.ok(fullPageDto).build();
        } catch (PageException.PageNotFound ex) {
            log.error("Page not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{pageId}/order/{destinationOrder}")
    public Response reorderPages(@PathParam("pageId") UUID pageIdentifier,
                                 @PathParam("destinationOrder") int destinationOrder) {
        try {
            log.debug("Changing order of page [ id : {} ] to destinationOrder : {}", pageIdentifier, destinationOrder);
            pageFacade.reorder(new PageReorder(new PageId(pageIdentifier), destinationOrder));
            log.debug("Pages order changed");
            return Response.ok().build();
        } catch (PageException.PageNotFound ex) {
            log.error("Page not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{pageId}")
    public Response deletePage(@PathParam("pageId") UUID pageIdentifier) {
        try {
            log.debug("Removing page with id {}", pageIdentifier);
            pageFacade.removePage(new PageId(pageIdentifier));
            log.debug("Page removed");
            return Response.ok().build();
        } catch (PageException.PageNotFound ex) {
            log.error("Page not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("{pageId}/containers/{containerId}/order/{destinationOrder}")
    public Response reorderContainers(@PathParam("pageId") UUID pageIdentifier,
                                      @PathParam("containerId") UUID containerId,
                                      @PathParam("destinationOrder") int destinationOrder) {
        try {
            log.debug("Changing order of container [ id : {} ] in page [ id : {} ] to destinationOrder : {}",
                    pageIdentifier, containerId, destinationOrder
            );
            final var pageId = new PageId(pageIdentifier);
            final var reorderModel = new ContainerReorder(new ContainerId(pageId, containerId), destinationOrder);
            log.debug("Container order changed");
            containerFacade.reorder(reorderModel);
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound ex) {
            log.error("Page or container not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("{pageId}/containers")
    public Response addContainerToPage(@PathParam("pageId") UUID pageIdentifier, CreatePageDto dto) {
        try {
            log.debug("Container created in page with id {} and with name {}", pageIdentifier, dto.displayText);
            containerFacade.addContainerToPage(new NewContainer(pageIdentifier, dto.displayText));
            log.debug("New container created");
            return Response.status(Response.Status.CREATED).build();
        } catch (PageException.PageNotFound ex) {
            log.error("Page not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{pageId}/containers/{containerId}")
    public Response modifyContainerInPage(@PathParam("pageId") UUID pageIdentifier,
                                          @PathParam("containerId") UUID containerId,
                                          EditContainerDto dto) {
        try {
            log.debug("Modifying container name to {} with id {} from page with id {}",
                    dto.displayText, containerId, pageIdentifier);
            containerFacade.modifyContainer(
                    new ModifyContainer(pageIdentifier, containerId, new DisplayText(dto.displayText))
            );
            log.debug("Container modified");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound ex) {
            log.error("Page or container not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{pageId}/containers/{containerId}")
    public Response deleteContainerInPage(@PathParam("pageId") UUID pageIdentifier,
                                          @PathParam("containerId") UUID containerId) {
        try {
            log.debug("Removing container with id {} from page with id {}", containerId, pageIdentifier);
            containerFacade.removeContainerFromPage(new ContainerId(new PageId(pageIdentifier), containerId));
            log.debug("Container removed");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound ex) {
            log.error("Page or container not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @POST
    @Path("{pageId}/containers/{containerId}/links")
    public Response addLinkToContainer(@PathParam("pageId") UUID pageIdentifier,
                                       @PathParam("containerId") UUID containerIdentifier,
                                       CreateLinkDto dto) {
        try {
            log.debug("Link created in page with id {} in container with id {} and with name {}",
                    pageIdentifier, containerIdentifier, dto.displayText);
            linkFacade.addLinkToContainer(new NewLink(pageIdentifier, containerIdentifier, dto.displayText, dto.webAddress));
            log.debug("New link created");
            return Response.status(Response.Status.CREATED).build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound | LinkException.LinkNotFound ex) {
            log.error("Page or container or link not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("{pageId}/containers/{containerId}/links/{linkId}/order/{destinationOrder}")
    public Response reorderLinks(@PathParam("pageId") UUID pageIdentifier,
                                 @PathParam("containerId") UUID containerIdentifier,
                                 @PathParam("linkId") UUID linkIdentifier,
                                 @PathParam("destinationOrder") int destinationOrder) {
        final var linkId = new LinkId(new ContainerId(new PageId(pageIdentifier), containerIdentifier), linkIdentifier);
        try {
            log.debug("Changing order of link [ id : {} ] in container [ id : {} ] and in page [ id : {} ] to destinationOrder : {}",
                    linkIdentifier, containerIdentifier, pageIdentifier, destinationOrder
            );
            final var reorderModel = new LinkReorder(linkId, destinationOrder);
            linkFacade.reorder(reorderModel);
            log.debug("Link order changed");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound ex) {
            log.debug("Page or container not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        } catch (LinkException.LinkNotFound e) {
            log.error("Link not found, trying to move link to other container ...");
            return tryToMoveLinkToAnotherContainer(
                    pageIdentifier, linkId, new MoveLinkDto(containerIdentifier, destinationOrder)
            );
        }
    }

    @PUT
    @Path("{pageId}/containers/{containerId}/links/{linkId}/position")
    public Response moveLinkBetweenContainers(@PathParam("pageId") UUID pageIdentifier,
                                              @PathParam("containerId") UUID containerIdentifier,
                                              @PathParam("linkId") UUID linkIdentifier,
                                              MoveLinkDto moveLinkDto) {
        try {
            log.debug("Moving link [ id : {} ] in container [ id : {} ] and in page [ id : {} ] to " +
                            "container [ id : {} ] in page [ id : {} ] to destinationOrder : {}",
                    linkIdentifier, containerIdentifier, pageIdentifier,
                    moveLinkDto.destinationContainerId, pageIdentifier, moveLinkDto.destinationOrder
            );
            final var pageId = new PageId(pageIdentifier);
            final var linkId = new LinkId(new ContainerId(pageId, containerIdentifier), linkIdentifier);
            final var move = new LinkMove(
                    linkId, moveLinkDto.destinationOrder, new ContainerId(pageId, moveLinkDto.destinationContainerId)
            );
            linkFacade.move(move);
            log.debug("Link moved to other container");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound | LinkException.LinkNotFound ex) {
            log.error("Page or container or link not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("/{pageId}/containers/{containerId}/links/{linkId}")
    public Response deleteLinkInContainer(@PathParam("pageId") UUID pageIdentifier,
                                          @PathParam("containerId") UUID containerId,
                                          @PathParam("linkId") UUID linkIdentifier) {
        try {
            log.debug("Removing link with id {} from container with id {} in page id {}", linkIdentifier, containerId, pageIdentifier);
            linkFacade.removeLinkFromContainer(new LinkId(new ContainerId(new PageId(pageIdentifier), containerId), linkIdentifier));
            log.debug("Link removed");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound | LinkException.LinkNotFound ex) {
            log.error("Page or container or link not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{pageId}/containers/{containerId}/links/{linkId}")
    public Response modifyLinkInContainer(@PathParam("pageId") UUID pageIdentifier,
                                          @PathParam("containerId") UUID containerId,
                                          @PathParam("linkId") UUID linkIdentifier,
                                          EditLinkDto dto) {
        try {
            log.debug("Modifying link name to {} and web reference to {} with id {} from container with id {} and in page with id {}",
                    dto.displayText, dto.webAddress, linkIdentifier, containerId, pageIdentifier);
            final var linkId = new LinkId(new ContainerId(new PageId(pageIdentifier), containerId), linkIdentifier);
            final var modifyData = new ModifyLink(
                    linkId,
                    new DisplayText(dto.displayText),
                    new WebReference(dto.webAddress),
                    new ImageAddress(dto.imageAddress)
            );
            linkFacade.modifyLink(modifyData);
            log.debug("Link changed");
            return Response.ok().build();
        } catch (PageException.PageNotFound | ContainerException.ContainerNotFound | LinkException.LinkNotFound ex) {
            log.error("Page or container or link not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    private Response exportFile(byte[] inputData) {
        try {
            log.debug("Pages exported");
            return Response.ok(inputData).header("Content-Disposition", "attachment; filename=pages.json").build();
        } catch (Exception ex) {
            log.error("Error in exporting the pages into json");
            return Response.status(HttpStatus.INTERNAL_SERVER_ERROR_500).entity(ex.getMessage()).build();
        }
    }

    private Response tryToMoveLinkToAnotherContainer(UUID pageIdentifier, LinkId linkId, MoveLinkDto dto) {
        try {
            final var sourceContainerId = linkFacade.getLinkContainer(linkId);
            return moveLinkBetweenContainers(pageIdentifier, sourceContainerId.value, linkId.value, dto);
        } catch (LinkException.LinkNotFound ex) {
            log.error("Link not found", ex);
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }
}
