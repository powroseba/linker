package io.powroseba.rest.pages.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageWithoutColumnDto {

    public String id;
    public String displayText;
    public int displayOrder;
}
