package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EditContainerDto {

    public final String displayText;

    @JsonCreator
    public EditContainerDto(@JsonProperty("displayText") String displayText) {
        this.displayText = displayText;
    }
}
