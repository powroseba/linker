package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class LinkDto {

    public final UUID linkId;
    public final String displayText;
    public final String webReference;
    public final String imageAddress;
    public final int displayOrder;

    @JsonCreator
    public LinkDto(
            @JsonProperty("linkId") UUID linkId,
            @JsonProperty("displayText") String displayText,
            @JsonProperty("webReference") String webReference,
            @JsonProperty("imageAddress") String imageAddress,
            @JsonProperty("displayOrder") int displayOrder) {
        this.linkId = linkId;
        this.displayText = displayText;
        this.webReference = webReference;
        this.imageAddress = imageAddress;
        this.displayOrder = displayOrder;
    }
}
