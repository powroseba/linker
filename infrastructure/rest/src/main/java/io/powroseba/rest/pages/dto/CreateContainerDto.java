package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateContainerDto {

    public final String displayText;

    @JsonCreator
    public CreateContainerDto(@JsonProperty("displayText") String displayText) {
        this.displayText = displayText;
    }
}
