package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class MoveLinkDto {

    public final UUID destinationContainerId;
    public final int destinationOrder;

    @JsonCreator
    public MoveLinkDto(@JsonProperty("destinationContainerId") UUID destinationContainerId,
                       @JsonProperty("destinationOrder") int destinationOrder) {
        this.destinationContainerId = destinationContainerId;
        this.destinationOrder = destinationOrder;
    }
}
