package io.powroseba.rest.pages.exporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.powroseba.page.export.PagesExportModel;
import io.powroseba.page.export.PagesExporter;

import java.nio.charset.StandardCharsets;

public class JsonPagesExporter implements PagesExporter<byte[]> {
    @Override
    public byte[] export(PagesExportModel pagesExportModel) {
        try {
            return new ObjectMapper().writeValueAsString(pagesExportModel).getBytes(StandardCharsets.UTF_8);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Export pages to json error!", e);
        }
    }
}
