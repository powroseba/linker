package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class FullPageDto {

    public final String id;
    public final String displayText;
    public final int displayOrder;
    public final Set<ContainerDto> containers;

    @JsonCreator
    public FullPageDto(@JsonProperty("id") String id,
                       @JsonProperty("displayText") String displayText,
                       @JsonProperty("displayOrder") int displayOrder,
                       @JsonProperty("containers") Set<ContainerDto> containers) {
        this.id = id;
        this.displayText = displayText;
        this.displayOrder = displayOrder;
        this.containers = containers;
    }
}
