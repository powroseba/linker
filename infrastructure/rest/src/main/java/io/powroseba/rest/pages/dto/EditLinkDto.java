package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EditLinkDto {

    public final String displayText;
    public final String webAddress;
    public final String imageAddress;

    @JsonCreator
    public EditLinkDto(@JsonProperty("displayText") String displayText,
                       @JsonProperty("webAddress") String webAddress,
                       @JsonProperty("imageAddress") String imageAddress) {
        this.displayText = displayText;
        this.webAddress = webAddress;
        this.imageAddress = imageAddress;
    }
}
