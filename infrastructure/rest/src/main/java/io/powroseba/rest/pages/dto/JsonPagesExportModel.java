package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.powroseba.page.export.PagesExportModel;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class JsonPagesExportModel {

    public final Set<JsonPage> pages;

    public PagesExportModel toPages() {
        return new PagesExportModel(
                pages.stream().map(JsonPage::toPage).collect(Collectors.toSet())
        );
    }

    @JsonCreator
    public JsonPagesExportModel(@JsonProperty("pages") Set<JsonPage> pages) {
        this.pages = pages;
    }

    public static final class JsonPage {
        public final UUID pageId;
        public final int displayOrder;
        public final String displayText;
        public final Set<JsonContainer> containers;

        @JsonCreator
        public JsonPage(@JsonProperty("pageId") UUID pageId,
                        @JsonProperty("displayOrder") int displayOrder,
                        @JsonProperty("displayText") String displayText,
                        @JsonProperty("containers") Set<JsonContainer> containers) {
            this.pageId = pageId;
            this.displayOrder = displayOrder;
            this.displayText = displayText;
            this.containers = containers;
        }

        private PagesExportModel.Page toPage() {
            return new PagesExportModel.Page(
                    pageId,
                    displayOrder,
                    displayText,
                    containers.stream().map(JsonContainer::toContainer).collect(Collectors.toSet())
            );
        }
    }

    public static final class JsonContainer {
        public final UUID containerId;
        public final String displayText;
        public final int displayOrder;
        public final Set<JsonLink> links;

        @JsonCreator
        public JsonContainer(@JsonProperty("containerId") UUID containerId,
                             @JsonProperty("displayText") String displayText,
                             @JsonProperty("displayOrder") int displayOrder,
                             @JsonProperty("links") Set<JsonLink> links) {
            this.containerId = containerId;
            this.displayText = displayText;
            this.displayOrder = displayOrder;
            this.links = links;
        }

        private PagesExportModel.Container toContainer() {
            return new PagesExportModel.Container(
                    containerId,
                    displayText,
                    displayOrder,
                    links.stream().map(JsonLink::toLink).collect(Collectors.toSet())
            );
        }
    }

    public static final class JsonLink {
        public final UUID linkId;
        public final String displayText;
        public final String webReference;
        public final String imageAddress;
        public final int displayOrder;

        @JsonCreator
        public JsonLink(@JsonProperty("linkId") UUID linkId,
                        @JsonProperty("displayText") String displayText,
                        @JsonProperty("webReference") String webReference,
                        @JsonProperty("imageAddress") String imageAddress,
                        @JsonProperty("displayOrder") int displayOrder) {
            this.linkId = linkId;
            this.displayText = displayText;
            this.webReference = webReference;
            this.imageAddress = imageAddress;
            this.displayOrder = displayOrder;
        }

        private PagesExportModel.Link toLink() {
            return new PagesExportModel.Link(
                    linkId,
                    displayText,
                    webReference,
                    imageAddress,
                    displayOrder
            );
        }
    }

}
