package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateLinkDto {

    public final String displayText;
    public final String webAddress;

    @JsonCreator
    public CreateLinkDto(@JsonProperty("displayText") String displayText,
                         @JsonProperty("webAddress") String webAddress) {
        this.displayText = displayText;
        this.webAddress = webAddress;
    }
}
