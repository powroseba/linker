package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;
import java.util.UUID;

public class ContainerDto {

    public final UUID containerId;
    public final String displayText;
    public final int displayOrder;
    public final Set<LinkDto> links;

    @JsonCreator
    public ContainerDto(@JsonProperty("containerId") UUID containerId,
                        @JsonProperty("displayText") String displayText,
                        @JsonProperty("displayOrder") int displayOrder,
                        @JsonProperty("links") Set<LinkDto> links) {
        this.containerId = containerId;
        this.displayText = displayText;
        this.displayOrder = displayOrder;
        this.links = links;
    }
}
