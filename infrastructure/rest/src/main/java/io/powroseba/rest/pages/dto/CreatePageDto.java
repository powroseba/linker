package io.powroseba.rest.pages.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePageDto {

    public final String displayText;

    @JsonCreator
    public CreatePageDto(@JsonProperty("displayText") String displayText) {
        this.displayText = displayText;
    }
}
