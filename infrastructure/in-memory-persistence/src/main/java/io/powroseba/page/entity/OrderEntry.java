package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderEntry {
    public final String id;
    public final int order;
}
