package io.powroseba.page.link;

import com.google.inject.Inject;
import io.powroseba.page.container.*;
import io.powroseba.page.entity.ContainerEntity;
import io.powroseba.page.entity.PageEntity;
import io.powroseba.page.entity.PagesEntity;
import io.powroseba.page.storage.Storage;
import io.powroseba.page.structs.PageId;

import javax.inject.Singleton;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class LinksRepositoryAdapter implements LinksRepository, LinkRepository {

    private final Storage<PagesEntity> pagesStorage;
    private final ContainersRepository containersRepository;

    @Inject
    public LinksRepositoryAdapter(Storage<PagesEntity> pagesStorage, ContainersRepository containersRepository) {
        this.pagesStorage = pagesStorage;
        this.containersRepository = containersRepository;
    }

    @Override
    public Optional<Links> findByContainerId(ContainerId containerId) {
        return pagesStorage.get()
                .flatMap(pages -> extractPageContainers(containerId.pageId, pages))
                .map(page -> page.containers)
                .flatMap(containers -> extractContainer(containerId, containers))
                .map(container -> LinkMapper.map(containerId, container.linkOrder, container.links));
    }

    @Override
    public void save(Links links) {
        containersRepository.findByPageId(links.containerId.pageId)
                .map(containers -> replaceContainersLinks(containers, links))
                .ifPresent(containersRepository::save);
    }

    @Override
    public Optional<ContainerId> getContainerOfLink(LinkId linkId) {
        return containersRepository.findByPageId(linkId.containerId.pageId)
                .flatMap(containers -> getContainerOfLink(containers, linkId))
                .map(container -> container.containerId);
    }

    private Optional<Container> getContainerOfLink(Containers containers, LinkId linkId) {
        return ContainerFetcher.getContainerOfLink(containers, linkId);
    }

    private Containers replaceContainersLinks(Containers containers, Links links) {
        final var containersCollection = containersWithoutModifyingContainer(containers, links);
        extractContainer(containers, links)
                .map(entity -> replaceLinksInContainer(entity, links))
                .ifPresent(containersCollection::add);
        return new Containers(containers.pageId, containers.getOrder(), containersCollection);
    }

    private Optional<PageEntity> extractPageContainers(PageId pageId, PagesEntity pages) {
        return pages.pages.stream().filter(entity -> comparePageId(pageId, entity)).findFirst();
    }

    private Set<Container> containersWithoutModifyingContainer(Containers containers, Links links) {
        return containers.stream().filter(container -> !compareContainerId(container, links.containerId)).collect(Collectors.toSet());
    }

    private Optional<Container> extractContainer(Containers containers, Links links) {
        return containers.stream().filter(container -> container.containerId.equals(links.containerId)).findFirst();
    }

    private Optional<ContainerEntity> extractContainer(ContainerId containerId, Set<ContainerEntity> containers) {
        return containers.stream().filter(entity -> compareContainerId(containerId, entity)).findFirst();
    }

    private Container replaceLinksInContainer(Container container, Links links) {
        return ContainersMapper.createContainer(container, links);
    }

    private boolean comparePageId(PageId pageId, PageEntity entities) {
        return entities.pageId.equals(pageId.value.toString());
    }

    private boolean compareContainerId(ContainerId containerId, ContainerEntity entity) {
        return containerId.value.toString().equals(entity.containerId);
    }

    private boolean compareContainerId(Container container, ContainerId containerId) {
        return containerId.equals(container.containerId);
    }
}
