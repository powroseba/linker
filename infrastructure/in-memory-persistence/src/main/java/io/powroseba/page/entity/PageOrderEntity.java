package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class PageOrderEntity {

    public final Set<OrderEntry> entries;
}
