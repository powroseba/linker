package io.powroseba.page.link;

import io.powroseba.page.container.ContainerId;
import io.powroseba.page.entity.LinkEntity;
import io.powroseba.page.entity.LinkOrderEntity;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.ImageAddress;
import io.powroseba.page.structs.WebReference;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public final class LinkMapper {

    private LinkMapper() {}

    public static Links map(ContainerId containerId, LinkOrderEntity linkOrder, Set<LinkEntity> linkEntities) {
        final var order = mapOrder(containerId, linkOrder);
        final var links = linkEntities
                .stream()
                .map(entity -> link(containerId, order, entity))
                .collect(Collectors.toSet());
        return new Links(containerId, order, links);
    }

    private static Link link(ContainerId containerId, LinkOrder order, LinkEntity entity) {
        return new Link(
                containerId,
                toLinkId(containerId, entity.linkId),
                new DisplayText(entity.displayText),
                new WebReference(entity.webAddress),
                new ImageAddress(entity.imageAddress),
                order.getOrderOfLink(toLinkId(containerId, entity.linkId))
            );
    }

    public static LinkEntity toEntity(Link link) {
        return new LinkEntity(link.linkId.value.toString(), link.displayText.content, link.webReference.webLink, link.image != null ? link.image.address: null);
    }

    private static LinkOrder mapOrder(ContainerId containerId, LinkOrderEntity orderEntity) {
        return new LinkOrder(
                orderEntity.entries.stream()
                        .map(link -> new LinkOrder.LinkOrderEntry(link.order, toLinkId(containerId, link.id)))
                        .collect(Collectors.toSet())
        );
    }

    public static LinkId toLinkId(ContainerId containerId, String id) {
        return new LinkId(containerId, UUID.fromString(id));
    }
}
