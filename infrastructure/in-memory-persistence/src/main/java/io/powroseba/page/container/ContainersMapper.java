package io.powroseba.page.container;

import io.powroseba.page.entity.ContainerEntity;
import io.powroseba.page.entity.ContainerOrderEntity;
import io.powroseba.page.entity.LinkOrderEntity;
import io.powroseba.page.entity.OrderEntry;
import io.powroseba.page.link.LinkMapper;
import io.powroseba.page.link.Links;
import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.PageId;
import org.checkerframework.checker.units.qual.C;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public final class ContainersMapper {

    private ContainersMapper() {}

    public static Containers map(String pageId, Set<ContainerEntity> containerEntities, ContainerOrderEntity orderEntity) {
        final var order = mapOrder(pageId, orderEntity);
        final var containers = containerEntities
                .stream()
                .map(entity -> container(pageId, order, entity))
                .collect(Collectors.toSet());
        return new Containers(new PageId(UUID.fromString(pageId)), order, containers);
    }

    private static Container container(String pageId, ContainerOrder order, ContainerEntity entity) {
        return new Container(
                toContainerId(pageId, entity.containerId),
                new DisplayText(entity.displayText),
                order.getOrderOfContainer(toContainerId(pageId, entity.containerId)),
                LinkMapper.map(toContainerId(pageId, entity.containerId), entity.linkOrder, entity.links)
        );
    }

    public static ContainerEntity toEntity(Container container) {
        return new ContainerEntity(
                container.containerId.value.toString(),
                container.displayText.content,
                container.links().map(LinkMapper::toEntity).collect(Collectors.toSet()),
                new LinkOrderEntity(
                        container.getLinksOrder().stream()
                                .map(entry -> new OrderEntry(entry.getId().value.toString(), entry.getOrder()))
                                .collect(Collectors.toSet())
                )
        );
    }

    public static ContainerOrderEntity toEntity(AbstractOrder<ContainerId, ?, ?> order) {
        return new ContainerOrderEntity(
                order.stream()
                        .map(entry -> new OrderEntry(entry.getId().value.toString(), entry.getOrder()))
                        .collect(Collectors.toSet())
        );
    }

    private static ContainerOrder mapOrder(String pageId, ContainerOrderEntity orderEntity) {
        return new ContainerOrder(
                orderEntity.entries.stream()
                .map(container -> new ContainerOrder.ContainerOrderEntry(container.order, toContainerId(pageId, container.id)))
                .collect(Collectors.toSet())
        );
    }

    public static ContainerId toContainerId(String pageId, String id) {
        return new ContainerId(new PageId(UUID.fromString(pageId)), UUID.fromString(id));
    }

    public static Container createContainer(Container container, Links links) {
        return new Container(container.containerId, container.displayText, container.displayOrder, links);
    }
}
