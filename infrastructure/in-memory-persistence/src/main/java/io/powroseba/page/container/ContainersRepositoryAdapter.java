package io.powroseba.page.container;

import io.powroseba.page.entity.PageEntity;
import io.powroseba.page.entity.PagesEntity;
import io.powroseba.page.storage.Storage;
import io.powroseba.page.structs.PageId;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class ContainersRepositoryAdapter implements ContainersRepository {

    private final Storage<PagesEntity> pagesStorage;

    @Inject
    public ContainersRepositoryAdapter(Storage<PagesEntity> pagesStorage) {
        this.pagesStorage = pagesStorage;
    }

    @Override
    public Optional<Containers> findByPageId(PageId pageId) {
        return pagesStorage.get()
                .flatMap(pages -> extractPage(pageId, pages))
                .map(page -> ContainersMapper.map(page.pageId, page.containers, page.containerOrder));
    }

    @Override
    public void save(Containers containers) {
        pagesStorage.get()
                .map(pages -> replacePageContainers(containers.pageId, pages, containers))
                .ifPresent(pagesStorage::save);
    }

    private PagesEntity replacePageContainers(PageId pageId, PagesEntity pages, Containers containers) {
        final var pagesCollection = pagesWithoutModifyingPage(pageId, pages);
        extractPage(pageId, pages)
                .map(entity -> replaceContainerInPage(entity, containers))
                .ifPresent(pagesCollection::add);
        return new PagesEntity(pagesCollection, pages.pageOrderEntity);
    }

    private Set<PageEntity> pagesWithoutModifyingPage(PageId pageId, PagesEntity pages) {
        return pages.pages.stream().filter(entity -> !comparePageId(pageId, entity)).collect(Collectors.toSet());
    }

    private PageEntity replaceContainerInPage(PageEntity page, Containers containers) {
        return new PageEntity(
                page.pageId,
                page.displayText,
                containers.stream().map(ContainersMapper::toEntity).collect(Collectors.toSet()),
                ContainersMapper.toEntity(containers.getOrder())
        );
    }

    private Optional<PageEntity> extractPage(PageId pageId, PagesEntity pages) {
        return pages.pages.stream().filter(entities -> comparePageId(pageId, entities)).findFirst();
    }

    private boolean comparePageId(PageId pageId, PageEntity entities) {
        return entities.pageId.equals(pageId.value.toString());
    }

}
