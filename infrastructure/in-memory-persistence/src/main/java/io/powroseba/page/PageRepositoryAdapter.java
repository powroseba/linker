package io.powroseba.page;

import com.google.inject.Singleton;
import io.powroseba.page.entity.PagesEntity;
import io.powroseba.page.storage.Storage;
import io.powroseba.page.structs.PageId;
import io.powroseba.page.structs.PageWithoutColumn;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.powroseba.page.PageMapper.toPageId;

// FIXME if it will be final repository refactor all mappings
@Singleton
public class PageRepositoryAdapter implements PagesRepository, PageRepository {

    private final Storage<PagesEntity> storage;

    @Inject
    public PageRepositoryAdapter(Storage<PagesEntity> storage) {
        this.storage = storage;
    }

    @Override
    public Optional<Pages> get() {
        return storage.get().map(PageMapper::mapPages);
    }

    @Override
    public void save(Pages pages) {
        storage.save(PageMapper.toEntity(pages));
    }

    @Override
    public Collection<PageWithoutColumn> findAllWithoutColumns() {
        var order = pageOrder();
        return storage.get().map(page -> page.pages).orElse(Collections.emptySet()).stream()
                .map(page -> new PageWithoutColumn(toPageId(page.pageId), page.displayText, order.getOrderOfPage(toPageId(page.pageId))))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Page> findById(PageId pageId) {
        return storage.get().flatMap(pages -> mapSingleMap(pageId, pages));
    }

    private Optional<Page> mapSingleMap(PageId pageId, PagesEntity pages) {
        return pages.pages
                .stream()
                .filter(entity -> toPageId(entity.pageId).equals(pageId))
                .findFirst()
                .map(entity ->
                        PageMapper.map(
                                entity,
                                pages.pageOrderEntity.entries
                                        .stream()
                                        .filter(entry -> toPageId(entity.pageId).equals(pageId))
                                        .map(entry -> entry.order)
                                        .findFirst()
                                        .orElse(0)
                        )
                );
    }

    public PageOrder pageOrder() {
        return new PageOrder(
                storage.get()
                        .map(page -> page.pageOrderEntity)
                        .map(order -> order.entries)
                        .orElse(Collections.emptySet())
                        .stream()
                        .map(entry -> new PageOrder.PageOrderEntry(entry.order, toPageId(entry.id)))
                        .collect(Collectors.toSet())
        );
    }

}
