package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class PageEntity {
    public final String pageId;
    public final String displayText;
    public final Set<ContainerEntity> containers;
    public final ContainerOrderEntity containerOrder;
}
