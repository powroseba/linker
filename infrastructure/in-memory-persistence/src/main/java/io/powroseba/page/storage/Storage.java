package io.powroseba.page.storage;

import java.util.Optional;

public interface Storage<T> {

    Optional<T> get();

    void save(T pages);
}
