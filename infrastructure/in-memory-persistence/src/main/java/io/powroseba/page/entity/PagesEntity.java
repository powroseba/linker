package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class PagesEntity {

    public final Set<PageEntity> pages;
    public final PageOrderEntity pageOrderEntity;
}
