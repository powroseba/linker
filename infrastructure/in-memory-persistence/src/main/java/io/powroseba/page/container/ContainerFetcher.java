package io.powroseba.page.container;

import io.powroseba.page.link.LinkId;

import java.util.Optional;

public final class ContainerFetcher {

    public static Optional<Container> getContainerOfLink(Containers containers, LinkId linkId) {
        return containers.stream().filter(container -> isContainLink(container, linkId)).findFirst();
    }

    private static boolean isContainLink(Container container, LinkId linkId) {
        return container.links().anyMatch(link -> link.linkId.equals(linkId));
    }
}
