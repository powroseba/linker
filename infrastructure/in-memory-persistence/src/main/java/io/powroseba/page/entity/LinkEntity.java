package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LinkEntity {
    public final String linkId;
    public final String displayText;
    public final String webAddress;
    public final String imageAddress;
}
