package io.powroseba.page.storage;

import io.powroseba.page.entity.PagesEntity;
import io.powroseba.page.structs.PageId;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class PagesStorage implements Storage<PagesEntity> {

    private static final PageId staticPageId = new PageId(UUID.randomUUID());

    protected static final Map<PageId, PagesEntity> pagesSource = new ConcurrentHashMap<>();

    public PagesStorage() {}

    public Optional<PagesEntity> get() {
        return Optional.ofNullable(pagesSource.get(staticPageId));
    }

    public void save(PagesEntity pages) {
        pagesSource.clear();
        pagesSource.put(staticPageId, pages);
    }
}
