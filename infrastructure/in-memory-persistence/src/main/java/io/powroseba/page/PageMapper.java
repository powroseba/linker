package io.powroseba.page;

import io.powroseba.page.container.ContainerId;
import io.powroseba.page.container.ContainersMapper;
import io.powroseba.page.entity.*;
import io.powroseba.page.order.AbstractOrder;
import io.powroseba.page.order.AbstractOrderEntry;
import io.powroseba.page.structs.DisplayText;
import io.powroseba.page.structs.PageId;

import java.util.UUID;
import java.util.stream.Collectors;

public final class PageMapper {

    private PageMapper() {}

    static Pages mapPages(PagesEntity pagesEntity) {
        final var order = mapOrders(pagesEntity);
        final var pages = pagesEntity.pages.stream()
                .map(entity -> new Page(
                        toPageId(entity.pageId),
                        new DisplayText(entity.displayText),
                        order.getOrderOfPage(toPageId(entity.pageId)),
                        ContainersMapper.map(entity.pageId, entity.containers, entity.containerOrder)
                    )
                )
                .collect(Collectors.toSet());
        return new Pages(order, pages);
    }

    static PagesEntity toEntity(Pages pages) {
        return new PagesEntity(
                pages.stream()
                        .map(page ->
                            new PageEntity(
                                    page.pageId.value.toString(), page.displayText.content,
                                    page.containers().map(ContainersMapper::toEntity).collect(Collectors.toSet()),
                                    ContainersMapper.toEntity(page.getContainersOrder())
                                )
                        )
                        .collect(Collectors.toSet()),
                new PageOrderEntity(
                        pages.getOrder().stream()
                                .map(entry -> new OrderEntry(entry.getId().value.toString(), entry.getOrder()))
                                .collect(Collectors.toSet())
                )
        );
    }

    public static Page map(PageEntity pageEntity, int order) {
        return new Page(
                toPageId(pageEntity.pageId),
                new DisplayText(pageEntity.displayText),
                order,
                ContainersMapper.map(pageEntity.pageId, pageEntity.containers, pageEntity.containerOrder)
        );
    }

    private static PageOrder mapOrders(PagesEntity pagesEntity) {
        return new PageOrder(
                pagesEntity.pageOrderEntity
                        .entries
                        .stream()
                        .map(order -> new PageOrder.PageOrderEntry(order.order, toPageId(order.id)))
                        .collect(Collectors.toSet())
        );
    }

    public static PageId toPageId(String id) {
        return new PageId(UUID.fromString(id));
    }
}
