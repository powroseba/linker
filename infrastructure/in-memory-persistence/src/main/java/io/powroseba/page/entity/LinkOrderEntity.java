package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class LinkOrderEntity {
    public final Set<OrderEntry> entries;
}
