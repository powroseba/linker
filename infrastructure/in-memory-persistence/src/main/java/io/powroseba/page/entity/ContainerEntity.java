package io.powroseba.page.entity;

import lombok.RequiredArgsConstructor;

import java.util.Set;

@RequiredArgsConstructor
public class ContainerEntity {
    public final String containerId;
    public final String displayText;
    public final Set<LinkEntity> links;
    public final LinkOrderEntity linkOrder;
}
