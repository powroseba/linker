package io.powroseba.config;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.powroseba.page.PageRepository;
import io.powroseba.page.PageRepositoryAdapter;
import io.powroseba.page.PagesRepository;
import io.powroseba.page.container.ContainersRepository;
import io.powroseba.page.container.ContainersRepositoryAdapter;
import io.powroseba.page.entity.PagesEntity;
import io.powroseba.page.link.LinkRepository;
import io.powroseba.page.link.LinksRepository;
import io.powroseba.page.link.LinksRepositoryAdapter;
import io.powroseba.page.storage.PagesStorage;
import io.powroseba.page.storage.Storage;

public class PersistenceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(PageRepository.class).to(PageRepositoryAdapter.class);
        bind(PagesRepository.class).to(PageRepositoryAdapter.class);
        bind(ContainersRepository.class).to(ContainersRepositoryAdapter.class);
        bind(LinksRepository.class).to(LinksRepositoryAdapter.class);
        bind(LinkRepository.class).to(LinksRepositoryAdapter.class);
        bind(new TypeLiteral<Storage<PagesEntity>>() {}).to(PagesStorage.class);
    }

}
